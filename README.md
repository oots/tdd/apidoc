# OOTS API documentation

Website url: https://oots.pages.code.europa.eu/tdd/apidoc/

Documentation website built with [Docusaurus](https://docusaurus.io/).

## Table of Contents

1. [Getting started](#Getting-started)
2. [Linting](#Linting)
3. [Auditing](#Auditing)
4. [Add API definitions](#Add-API-definitions)
5. [Licensing](#Licensing)

## Getting started

Clone the repository and move to the project directory:

```sh
git clone https://github.com/yhuard/ootsdoc.git
cd ootsdoc
```

For building, you can choose to build with Docker or to build from the sources directly.

### Build with Docker Compose

Build with Docker Compose:

```sh
docker-compose up --build
```

The OOTS API documentation will be accesible at http://localhost:3000/docs

### Build from source

You need:

- Node.js >= 20.10.0
- Yarn >= 1.22.0

Clone the repository and move to the project directory.

Install the libraries and dependencies:

```sh
yarn install
```

Start the OOTS API documentation:

```sh
yarn start
```

The API will be accesible at http://localhost:3000/docs/

## Linting

If you haven't installed the dev dependencies already, make sure to run:

```sh
yarn install
```

To lint all the files, run the following command:

```sh
yarn lint
```

## Auditing

In order to check the dependencies for known vulnerabilities, run:

```sh
yarn run audit
```

## Licensing

Copyright (c) 2019 European Commission  
Licensed under the EUPL, Version 1.2 or - as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
You may not use this work except in compliance with the Licence.
You may obtain a copy of the Licence at:

- <https://joinup.ec.europa.eu/page/eupl-text-11-12>

Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the Licence for the specific language governing permissions and limitations under the Licence.
