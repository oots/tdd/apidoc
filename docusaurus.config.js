/* eslint-disable @typescript-eslint/no-unsafe-member-access, @typescript-eslint/no-unsafe-assignment, @typescript-eslint/no-var-requires */
// @ts-check
require("dotenv").config();
const {
  themes: { dracula, github },
} = require("prism-react-renderer/dist/index");

/** @type {import('@docusaurus/types').Config} */
const config = {
  title: "OOTS developers hub",
  tagline:
    "A one-stop shop for OOTS application providers to discover our specs, then build and test their solutions.",
  url: process.env.URL ?? "http://localhost:3000",
  baseUrl: process.env.BASE_URL ?? "/tdd/apidoc/",
  onBrokenLinks: "throw",
  onBrokenMarkdownLinks: "warn",
  favicon: "img/favicon.ico",
  organizationName: "oots",
  projectName: "oots",
  trailingSlash: false,
  presets: [
    [
      "@docusaurus/preset-classic",
      {
        docs: {
          path: "api",
          breadcrumbs: true,
          routeBasePath: "/",
          include: ["**/*.md", "**/*.mdx"],
          exclude: ["**/_*.{js,jsx,ts,tsx,md,mdx}", "**/_*/**"],
          sidebarPath: undefined,
          sidebarCollapsible: false,
          sidebarCollapsed: false,
        },
        blog: false,
        theme: {
          customCss: [require.resolve("./src/css/custom.css")],
        },
      },
    ],
  ],
  plugins: [
    [
      "@docusaurus/plugin-client-redirects",
      {
        redirects: [
          {
            from: "/dsd",
            to: "/",
          },
        ],
      },
    ],
  ],
  themes: [
    [
      // https://github.com/easyops-cn/docusaurus-search-local
      require.resolve("@easyops-cn/docusaurus-search-local"),
      {
        // `hashed` is recommended as long-term-cache of index file is possible.
        hashed: true,
        language: ["en"],
        indexDocs: true,
        indexBlog: false,
        indexPages: true,
      },
    ],
  ],
  themeConfig: {
    /** @type {import('@docusaurus/preset-classic').ThemeConfig} */
    colorMode: {
      defaultMode: "light",
      disableSwitch: true,
    },
    docs: {
      sidebar: {
        hideable: false,
        autoCollapseCategories: true,
      },
    },
    navbar: {
      hideOnScroll: false,
      logo: {
        alt: "EBSI Logo",
        src: "img/logo-once-only-hub.svg",
        href: "https://ec.europa.eu/digital-building-blocks/sites/display/EBSI/Home",
      },
      items: [
        {
          to: "/",
          activeBaseRegex: "/api",
          label: "APIs",
          position: "left",
          items: [
            {
              to: "/evidence-broker",
              html: `<div class="menu-item-wrapper"><div class="menu-item-title">Evidence Broker</div><div class="menu-item-caption">Authoritative system that maps specific data sets</div></div>`,
            },
            {
              to: "/data-services-directory",
              html: `<div class="menu-item-wrapper"><div class="menu-item-title">Data Service Directory</div><div class="menu-item-caption">Catalogue of evidence types that can be provided upon request</div></div>`,
            },
            {
              to: "/semantic-repository",
              html: `<div class="menu-item-wrapper"><div class="menu-item-title">Semantic Repository</div><div class="menu-item-caption">A public repository containing all OOTS semantic assets</div></div>`,
            },
          ],
        },
      ],
    },
    footer: {
      // We use a custom footer
    },
    prism: {
      theme: github,
      darkTheme: dracula,
    },
  },
};

module.exports = config;
