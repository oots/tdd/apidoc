﻿---
sidebar_position: 1
---

# Redirect to Asset Distribution Access URL
>**Version 1.1.0** | **Mandatory** |

## Request

:::info
**Purpose of the Redirect to Asset Distribution Access URL method:**
The main purpose of this method is to force a redirection to the distribution access URL of the asset. If the asset has more than one distribution, the SR will choose the most relevant one.
Distributions of an asset are physical embodiments of the asset and can have multiple formats and be stored in different locations. A classical sample of this can be a codelist that is published in csv, genericode xml, etc. Distributions have two main URLs: the access URL and the download URL. The first, which is used in the redirect method, drives to the distribution main page (normally a code repository like code.europa.eu or an external repository), whilst the second is the URL that allows downloading the asset file.
:::

### Parameter Details
As well as with the Get Asset Metadata method, the redirection method is a GET method that requires the asset URI to trigger it.
It will be required to inform the `redirection` parameter in order to force the redirection (otherwise the Get Asset Metadata method will be executed by default)  

| Parameter    | Requirement | Description                                                                    |
| ------------ | ----------- |--------------------------------------------------------------------------------|
| **redirect** | MANDATORY   | `yes` is the only accepted value. If an asset access URL is found it will force the redirection|

## Response

### Positive response

If the URI is found and it corresponds to an asset in PUBLISHED status (only public assets are displayed in the Semantic Repository), and it also meets the condition of having at least one distribution with access URL, the method will redirect to this URL. In the case of having more than one accessURL the system will choose the most relevant one based on the following criteria:

1. It will prioritise structured rather than unstructured formats
2. It will prioritise EC repositories rather than external repositories
3. It will prioritise distributions that have both access and download URLs

#### Example

The table below contains samples of each of some assets that can redirect to access URLs:

| Asset Type                       | Asset                                    | URI                                                                                                  |
|----------------------------------|------------------------------------------|------------------------------------------------------------------------------------------------------|
| **Codelist**                     | EBErrorCodes                             | https://sr.oots.tech.ec.europa.eu/codelists/eberrorcodes?redirect=yes                                             |          
| **Example**                      | 4.1 Step 1 Evidence Request Header - Tax | https://sr.oots.tech.ec.europa.eu/examples/10006e73-0ab3-4850-964b-93d1c985d110?redirect=yes                       |
| **Schematron**                   | DSD-ERR-S                                | https://sr.oots.tech.ec.europa.eu/schematrons/dsd-err-s?redirect=yes                                              |
| **Technical Specification**      | TDD                                      | https://sr.oots.tech.ec.europa.eu/specifications/tdds?redirect=yes                                               |
| **Semantic Data Specification**  | EMREX ELMO Evidence                      | https://sr.acc.oots.tech.ec.europa.eu/datamodels/elmo-schemas-v1?redirect=yes                                     |
| **Schema**                       | XML                                      | https://sr.acc.oots.tech.ec.europa.eu/schemas/xml?redirect=yes                                                    |                              |                                           |


### Error response

When something unexpected occurs during the execution of the Redirect to Asset AccessURL method, an error response is returned in XML format with the following fields:

- **code**: The internal error code of the Semantic Repository (with the following format SR:ERR:XXXX)
- **title**: This is the general description of the error code
- **description**: A string describing additional details of the error that might be needed to understand the context in which it occurred
- **timestamp**: the exact time in which the error was thrown by the system.

#### Example

An example of an Error Response of the Semantic Repository can be found in the following XML snippet:

<details>
<summary>Example of an Error Response</summary>

```xml
<errorDto>
    <code>SR:ERR:0004</code>
    <title>Semantic Repository data error</title>
    <description>Semantic Repository data are not present in database. Asset with identifier '/codelists/errorseveritys' is not found</description>
    <timestamp>2023-12-01T12:28:39</timestamp>
</errorDto>
```

</details>

#### Error Response Codes

The following table provides the list of possible error codes for this method. The `Description` may contain further information for specific errors:

**Error Response Codes**

| HTTP Status  | Code        | Title                                    | Detail                                                                                |
| ------------ |-------------|------------------------------------------| --------------------------------------------------------------------------------------|
| 500          | SR:ERR:0001 | General error                            | An error occurred during execution (runtime error)
| 406          | SR:ERR:0002 | Bad request error                        | Error for general bad requests due to wrong unaccepted values on input parameters
| 404          | SR:ERR:0004 | Cannot find asset                        | The URI pattern is correctly written but the asset UUID cannot be found
| 404          | SR:ERR:0005 | Incorrect URI format                     | The URI pattern (asset type) is not recognised by the system                 
