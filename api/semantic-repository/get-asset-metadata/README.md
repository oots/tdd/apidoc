﻿---
sidebar_position: 0
---

# Get Asset Metadata 
>**Version 1.1.0** | **Mandatory** | 

## Request

:::info
**Purpose of the Get Asset Metadata method:**
The main purpose of this method is to provide a machine-readable metadata file of an OOTS semantic asset contained in the Semantic Repository.
This method can be accessed issuing a GET request on the URI of the asset. If it is needed a specific version of the asset it can be also indicated in the request by adding it at the end of the URI. Metadata format can be indicated as an optional parameters (default format is XML if nothing is indicated, and it is also the recommended one as it is supported by XSD for validation)
More information about the asset data model and the serialisation format is provided in **section 3.3.5** of the TDDs (SR Data Model)
:::

### Parameter Details
Use the GET method on the asset URI to invoke the GetAssetMedatadata method. If it is needed a specific version of the asset it can be added at the end of the URI with "/version" (otherwise it will fetch the latest published version). The asset metadata is available in multiple formats. To specify the requested format, you can set the "format" query parameter to one of the supported values "xml" and "json". If nothing is indicated, the default format will be "xml". In the future more formats can be potentially added, as well as transformations to human-readable formats. 

| Parameter  | Requirement | Description                                                                    |
| ---------- | ----------- |--------------------------------------------------------------------------------|
| **format** | OPTIONAL    | `xml` or `json` are accepted. The default value if nothing is specified is xml.|

*Available formats*: 

>**XML format:** https://sr.oots.tech.ec.europa.eu/codelists/agentclassification?format=xml or simply: https://sr.oots.tech.ec.europa.eu/codelists/agentclassification

>**JSON format:** https://sr.oots.tech.ec.europa.eu/codelists/agentclassification?format=json 

## Response

### Positive response

If the asset URI is found and it corresponds to an asset in PUBLISHED status (only public assets are displayed in the Semantic Repository), the method will return a file containing the asset metadata in the specified format. A serialised version of the asset metadata based on the SR Data Model is explained in **section 3.3.5** of the [TDDs](https://ec.europa.eu/digital-building-blocks/wikis/pages/viewpage.action?pageId=703038096). XML metadata files can be validated against an existing set of [XSDs](https://code.europa.eu/oots/tdd/tdd_chapters/-/tree/master/OOTS-SR/OOTS-SR-data-model/srdm-xsd). 

#### Examples

The table below contains URIs samples of each available asset type:

| Asset Type                       | Asset                                    | URI                                                                                                  |
|----------------------------------|------------------------------------------|------------------------------------------------------------------------------------------------------|
| **Evidence**                     | Birth Certificate                        | https://sr.oots.tech.ec.europa.eu/evidencetypeclassifications/HR/22a8b134-57be-482b-a196-b0c620ced2c3|
| **Procedure**                    | Requesting a Birth Certificate           | https://sr.oots.tech.ec.europa.eu/codelist/procedures/r1                                             |
| **Requirement**                  | Proof of nationality                     | https://sr.oots.tech.ec.europa.eu/requirements/b1077fa7-e9d5-4c67-8957-b2b56fcc8f32                  |
| **Codelist**                     | EBErrorCodes                             | https://sr.oots.tech.ec.europa.eu/codelists/eberrorcodes                                             |          
| **Example**                      | 4.1 Step 1 Evidence Request Header - Tax | https://sr.oots.tech.ec.europa.eu/examples/10006e73-0ab3-4850-964b-93d1c985d110                       |
| **Schematron**                   | DSD-ERR-S                                | https://sr.oots.tech.ec.europa.eu/schematrons/dsd-err-s                                              |
| **Technical Specification**      | TDD                                      | https://sr.oots.tech.ec.europa.eu/specifications/tdds                                                |
| **Semantic Data Specification**  | EMREX ELMO Evidence                      | https://sr.acc.oots.tech.ec.europa.eu/datamodels/elmo-schemas-v1                                     |
| **Schema**                       | XML                                      | https://sr.acc.oots.tech.ec.europa.eu/schemas/xml                                                    |                              |                                           |

A sample XML metadata output is shown in continuation for URI https://sr.acc.oots.tech.ec.europa.eu/examples/0660f5f8-72e5-4bc6-aa8f-0961fe316cdc

<details>
<summary>Output of XML of metadata for proof of birth evidence response example</summary>

```xml
<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<sr:Asset xmlns:sr="https://sr.oots.tech.ec.europa.eu/model/srdm#">
	<sr:identifier>https://sr.acc.oots.tech.ec.europa.eu/examples/0660f5f8-72e5-4bc6-aa8f-0961fe316cdc</sr:identifier>
	<sr:versionedIdentifier>https://sr.acc.oots.tech.ec.europa.eu/examples/0660f5f8-72e5-4bc6-aa8f-0961fe316cdc/x.x.x</sr:versionedIdentifier>
	<sr:title xml:lang="en">2.2 Step 2 Evidence Response - Birth - full example</sr:title>
	<sr:type>EXAMPLE</sr:type>
	<sr:associatedTransaction>
		<sr:Transaction>
			<sr:identifier>https://sr.oots.tech.ec.europa.eu/codelist/transaction/EDM-RESP</sr:identifier>
			<sr:title xml:lang="en">EDM-RESP</sr:title>
			<sr:description xml:lang="en">Evidence Response</sr:description>
			<sr:type>QUERY</sr:type>
			<sr:subject>PAYLOAD</sr:subject>
			<sr:component>EDM</sr:component>
		</sr:Transaction>
	</sr:associatedTransaction>
	<sr:publisher>
		<sr:Agent>
			<sr:name xml:lang="en">Directorate-General for Digital Services</sr:name>
		</sr:Agent>
	</sr:publisher>
	<sr:status>PUBLISHED</sr:status>
	<sr:language>ENG</sr:language>
	<sr:issued>2023-12-01</sr:issued>
	<sr:modified>2024-09-08</sr:modified>
	<sr:landingPage>https://code.europa.eu/oots/tdd/tdd_chapters/-/blob/1.1.0/OOTS-EDM/xml/Request-Response Samples/4.5.4 - OOTS-EDM XML Examples of the Evidence Exchange/2 Example for requesting a Birth Certificate - natural person/2.2 Step 2 Evidence Response - Birth - full example.xml</sr:landingPage>
	<sr:distribution>
		<sr:AssetDistribution>
			<sr:downloadURL>https://code.europa.eu/oots/tdd/tdd_chapters/-/raw/1.1.0/OOTS-EDM/xml/Request-Response Samples/4.5.4 - OOTS-EDM XML Examples of the Evidence Exchange/2 Example for requesting a Birth Certificate - natural person/2.2 Step 2 Evidence Response - Birth - full example.xml</sr:downloadURL>
			<sr:mediaType>XML</sr:mediaType>
			<sr:title xml:lang="en">XML distribution of 2.2 Step 2 Evidence Response - Birth - full example example</sr:title>
		</sr:AssetDistribution>
	</sr:distribution>
	<sr:conformsTo>
		<sr:VersionedAssetRef>
			<sr:identifier>https://sr.acc.oots.tech.ec.europa.eu/schematrons/EDM-RESP-C</sr:identifier>
			<sr:version>1.1.0</sr:version>
		</sr:VersionedAssetRef>
		<sr:VersionedAssetRef>
			<sr:identifier>https://sr.acc.oots.tech.ec.europa.eu/schematrons/EDM-RESP-S</sr:identifier>
			<sr:version>1.1.0</sr:version>
		</sr:VersionedAssetRef>
	</sr:conformsTo>
	<sr:isPartOf>
		<sr:VersionedAssetRef>
			<sr:identifier>https://sr.acc.oots.tech.ec.europa.eu/specifications/tdds</sr:identifier>
			<sr:version>1.0.0</sr:version>
		</sr:VersionedAssetRef>
		<sr:VersionedAssetRef>
			<sr:identifier>https://sr.acc.oots.tech.ec.europa.eu/specifications/tdds</sr:identifier>
			<sr:version>1.0.1</sr:version>
		</sr:VersionedAssetRef>
		<sr:VersionedAssetRef>
			<sr:identifier>https://sr.acc.oots.tech.ec.europa.eu/specifications/tdds</sr:identifier>
			<sr:version>1.0.2</sr:version>
		</sr:VersionedAssetRef>
		<sr:VersionedAssetRef>
			<sr:identifier>https://sr.acc.oots.tech.ec.europa.eu/specifications/tdds</sr:identifier>
			<sr:version>1.0.3</sr:version>
		</sr:VersionedAssetRef>
		<sr:VersionedAssetRef>
			<sr:identifier>https://sr.acc.oots.tech.ec.europa.eu/specifications/tdds</sr:identifier>
			<sr:version>1.0.4</sr:version>
		</sr:VersionedAssetRef>
		<sr:VersionedAssetRef>
			<sr:identifier>https://sr.acc.oots.tech.ec.europa.eu/specifications/tdds</sr:identifier>
			<sr:version>1.0.5</sr:version>
		</sr:VersionedAssetRef>
		<sr:VersionedAssetRef>
			<sr:identifier>https://sr.acc.oots.tech.ec.europa.eu/specifications/tdds</sr:identifier>
			<sr:version>1.1.0</sr:version>
		</sr:VersionedAssetRef>
	</sr:isPartOf>
	<sr:inCatalog>
		<sr:AssetRepository>
			<sr:title xml:lang="en">OOTS TDDs assets repository</sr:title>
			<sr:description xml:lang="en">The catalog of the OOTS Technical Design Documents assets</sr:description>
			<sr:landingPage>https://code.europa.eu/oots/tdd/tdd_chapters/-/tree/master/OOTS-EDM</sr:landingPage>
			<sr:publisher>
				<sr:Agent>
					<sr:name xml:lang="en">Directorate-General for Digital Services</sr:name>
				</sr:Agent>
			</sr:publisher>
			<sr:inCatalog>
				<sr:AssetRepository>
					<sr:title xml:lang="en">OOTS Semantic Repository</sr:title>
					<sr:description xml:lang="en">The catalog of all the semantic assets used to enable the exchange of evidence in the Once-Only Technical System</sr:description>
					<sr:homepage>https://sr.oots.tech.ec.europa.eu/sr/home</sr:homepage>
					<sr:publisher>
						<sr:Agent>
							<sr:name xml:lang="en">Directorate-General for Digital Services</sr:name>
						</sr:Agent>
					</sr:publisher>
				</sr:AssetRepository>
			</sr:inCatalog>
		</sr:AssetRepository>
	</sr:inCatalog>
</sr:Asset>

```

</details>


### Error response

When something unexpected occurs during the execution of the Get Metadata method, an error response is returned in XML format with the following fields:

- **code**: The internal error code of the Semantic Repository (with the following format SR:ERR:XXXX)
- **title**: This is the general description of the error code
- **description**: A string describing additional details of the error that might be needed to understand the context in which it occurred
- **timestamp**: the exact time in which the error was thrown by the system.

#### Example

An example of an Error Response of the Semantic Repository can be found in the following XML snippet:

<details>
<summary>Example of an Error Response</summary>

```xml
<errorDto>
    <code>SR:ERR:0004</code>
    <title>Semantic Repository data error</title>
    <description>Semantic Repository data are not present in database. Asset with identifier '/codelists/errorseverity' is not found</description>
    <timestamp>2024-06-27T16:55:49</timestamp>
</errorDto>
```

</details>

#### Error Response Codes

The following table provides the list of possible error codes for this method. The `Description` may contain further information for specific errors:

**Error Response Codes**

| HTTP Status  | Code        | Title                                    | Detail                                                                                |
| ------------ |-------------|------------------------------------------| --------------------------------------------------------------------------------------|
| 500          | SR:ERR:0001 | General error                            | An error occurred during execution (runtime error)
| 406          | SR:ERR:0002 | Bad request error                        | Error for general bad requests due to wrong unaccepted values on input parameters
| 404          | SR:ERR:0004 | Semantic Repository data error	        | Asset with a given identifier (and eventually version) is not found when calling the GET endpoint
| 404          | SR:ERR:0005 | Path not found error                     | The URI pattern is not recognised by the system (wrong endpoint)                
