﻿---
sidebar_position: 2
---

# Search
>**Version 1.1.0** | **Mandatory** | 

## Request

:::info
**Purpose of the Search method:**
This REST API method allows SR users to easily locate and filter semantic assets together with their associated metadata, obtaining the results in the form of XML files. 
The aim of this method is to provide users with a public search method that allows multiple filtering options. Users will be able to look for assets based on their main attributes, which include creator/publisher, last modified date range, asset type, language, status, or media type format. Additionally, the search function allows free text parameter to locate assets based on title and description (including possible translations). The method will also allow results pagination and multiple values per filter.
:::

### Parameter Details
Use the GET protocol on search method using one or more of the available parameters listed in continuation. If nothing is indicated, the default search call will return all available results. The format of the output is RegRep, aligned with other Common Service API methods.

| Parameter                  | Requirement | Description                                                                                                                                                                                                                                                                                                                                                                         |
| ---------------------------| ----------- |-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| **text**                   | OPTIONAL    | Free text affecting all textual fields of an asset (titles and descriptions), including locale translations. The query must match partial values like in %text%.                                                                                                                                                                                                                    |
| **assetRepository**        | OPTIONAL    | sub-repository of the asset. The accepted values are:  "OOTS TDDs assets repository", "OOTS CS assets repository". Users can select multiple values simultaneously, separating them by “,”                                                                                                                                                                                          |
| **assetType**              | OPTIONAL    | Type of the asset. The accepted values are:  "TECHNICAL_SPECIFICATION","SEMANTIC_DATA_SPECIFICATION", "SCHEMA", "CODELIST", "SCHEMATRON", "EXAMPLE", "PROCEDURE", "REQUIREMENT", "EVIDENCE_TYPE". Users can select multiple values simultaneously, separating them by “,”                                                                                                           |
| **status**                 | OPTIONAL    | Status of the asset. The accepted values are: "published", "withdrawn". Users can select multiple values simultaneously, separating them by colons “,”.                                                                                                                                                                                                                             |
| **publisher**              | OPTIONAL    | Free text field targeting agent name for publishers. The query must match partial values like in %text%.                                                                                                                                                                                                                                                                            |
| **creator**                | OPTIONAL    | Free text field targeting agent name for creators. The query must match partial values like in %text%.                                                                                                                                                                                                                                                                              |
| **language**               | OPTIONAL    | Language of the asset metadata. The accepted values are two letter ISO_3166-1_alpha-2 codes or three letter codes from [Publications Office language codelists](https://op.europa.eu/en/web/eu-vocabularies/dataset/-/resource?uri=http://publications.europa.eu/resource/dataset/language) can select multiple values simultaneously, separating them by colons “,”                |
| **modified**               | OPTIONAL    | Last modification date of the asset version. The accepted range formats are: 1) YYYYMMDD-YYYYMMDD representing a complete range with both start and end dates included. 2) YYYYMMDD representing a exact date (all assets modified anytime during that day) 3) <YYYYMMDD any date earlier than this 4) >YYYYMMDD any date later than this.                                          |
| **mediaType**              | OPTIONAL    | Media Type of the asset distribution. The accepted values are: "xlsx", "xml", "xslt", "html", "css", "png", "jpeg", "gif", "json", "md", "docx", "zip". Users can select multiple values simultaneously, separating them by colons “,”                                                                                                                                              |
| **theme**                  | OPTIONAL    | Theme of the asset validation (only schemathrons and examples). The accepted values are: "structure", "content", "cross_message". Users can select multiple values simultaneously, separating them by colons “,”                                                                                                                                                                    |
| **transactionComponent**   | OPTIONAL    | Component of the asset transactions (only schemathrons and examples). The accepted values are: "edm", "eb", "dsd". Users can select multiple values simultaneously, separating them by colons “,”                                                                                                                                                                                   |
| **transactionType**        | OPTIONAL    | Type of the asset transactions (only schemathrons and examples). The accepted values are: "query", "lcm". Users can select multiple values simultaneously, separating them by colons “,”                                                                                                                                                                                            |
| **transactionSubject**     | OPTIONAL    | Subject of the asset transactions (only schemathrons and examples). The accepted values are: "payload", "header". Users can select multiple values simultaneously, separating them by colons “,”                                                                                                                                                                                    |
| **showVersions**           | OPTIONAL    | Allows listing past version of the assets. The accepted values are: "yes","no". The default value if nothing is indicated is “no”. If showVersions=yes then search will return all different versions of matched assets. If showVersions=no or it is not indicated as parameter, then the search will return only unversioned assets or asset versions marked as “currentVersion”   |
| **version**                | OPTIONAL    | Free text affecting “version” field. The query must match partial values like in %version%. No minimum length is required.                      																																													                                                 |
| **issued**                 | OPTIONAL    | The date in which the asset version was published for first time. The accepted range formats are: 1) YYYYMMDD-YYYYMMDD representing a complete range with both start and end dates included. 2) YYYYMMDD representing a exact date (all asset versions issued anytime during that day) 3) <YYYYMMDD any date earlier than this and 4) >YYYYMMDD any date later than this.           |
| **associationType**        | OPTIONAL    | Type of asset association towards other assets in the SR. The accepted values are: "is_part_of", "conforms_to". Users can select multiple values simultaneously, separating them by colons “,”                                                                                                                                                                                      |
| **associatedAsset**        | OPTIONAL    | Associated Asset URI (only complete URIs are considered as valid.                                                                                                                                                                                                                                                                                                                   |
| **associatedAssetVersion** | OPTIONAL    | Free text affecting “version” field of the associated asset. The query must match partial values like in %version%. No minimum length is required.                                                                                                                                                                                                                                  |
| **sortField**              | OPTIONAL    | Field used for sorting. Possible values are "title", "assetType", "publisher", "creator", "modified", "version", "status", "issued"                                                                                                                                                                                                                                                 |
| **order**                  | OPTIONAL    | Sorting order (it works only with sortField attribute). The accepted values are "asc" or "desc"                                                                                                                                                                                                                                                                                     |
| **responseFormat**         | OPTIONAL    | The accepted values are: "regrep" (default), "xml". If "responseFormat=regrep" or nothing is indicated, it will return the XML response in RegRep format. If  "responseFormat=xml", it will return the XML response in SR specific format                                                                                                                                           |
| **resultsPerPage**         | OPTIONAL    | Max. results per page (any integer is accepted). If this parameter is set, then also page is expected.	If ommitted, all results are displayed																																					                                                                                     |
| **page**                   | OPTIONAL    | Page number (if omitted all pages are retuned)																																																																																		 |
## Response

### Positive response

If there are no errors in the filters value, the search method is expected to return a response in XML format. The response will include an XML document containing both the selected filters and the search results, as well as the total number of results and distribution in pages.

#### Example

A sample output is shown in continuation in RegRep format to search the first 25 assets of type CODELIST included in TDD release 1.1.0 and sorted by asset title

https://sr.acc.oots.tech.ec.europa.eu/search?associatedAsset=https://sr.oots.tech.ec.europa.eu/specifications/tdds&associatedAssetVersion=1.1.0&assetType=CODELIST&resultsPerPage=25&page=1&sortField=title&order=ASC

<details>
<summary>Example of a positive Search result</summary>

```xml
<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<query:QueryResponse xmlns:lcm="urn:oasis:names:tc:ebxml-regrep:xsd:lcm:4.0" xmlns:sr="https://sr.oots.tech.ec.europa.eu/model/srdm#" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:rs="urn:oasis:names:tc:ebxml-regrep:xsd:rs:4.0" xmlns:p4s="http://data.europa.eu/p4s" xmlns:query="urn:oasis:names:tc:ebxml-regrep:xsd:query:4.0" xmlns:spi="urn:oasis:names:tc:ebxml-regrep:xsd:spi:4.0" xmlns:ns8="http://www.w3.org/1999/xhtml" xmlns:rim="urn:oasis:names:tc:ebxml-regrep:xsd:rim:4.0" xmlns:ws-addr="http://www.w3.org/2005/08/addressing" startIndex="1" totalResultCount="19" status="urn:oasis:names:tc:ebxml-regrep:ResponseStatusType:Success">
    <rim:RegistryObjectList>
        <rim:RegistryObject>
            <rim:Slot>
                <rim:SlotValue xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="rim:AnyValueType">
                    <sr:Asset>
                        <sr:identifier>https://sr.acc.oots.tech.ec.europa.eu/codelists/agentclassification</sr:identifier>
                        <sr:versionedIdentifier>https://sr.acc.oots.tech.ec.europa.eu/codelists/agentclassification/1.0.0</sr:versionedIdentifier>
                        <sr:title xml:lang="en">AgentClassification</sr:title>
                        <sr:description xml:lang="en">Agent Classification</sr:description>
                        <sr:type>CODELIST</sr:type>
                        <sr:publisher>
                            <sr:Agent>
                                <sr:name xml:lang="en">Directorate-General for Digital Services</sr:name>
                            </sr:Agent>
                        </sr:publisher>
                        <sr:status>PUBLISHED</sr:status>
                        <sr:language>ENG</sr:language>
                        <sr:version>1.0.0</sr:version>
                        <sr:versionNotes xml:lang="en">Release notes of version 1.0.0 can be checked at: https://code.europa.eu/oots/tdd/tdd_chapters/-/commits/1.1.0/OOTS-EDM/codelists/OOTS/AgentClassification-CodeList.gc</sr:versionNotes>
                        <sr:issued>2024-09-08</sr:issued>
                        <sr:modified>2024-09-08</sr:modified>
                        <sr:landingPage>https://code.europa.eu/oots/tdd/tdd_chapters/-/tree/master/OOTS-EDM</sr:landingPage>
                        <sr:distribution>
                            <sr:AssetDistribution>
                                <sr:downloadURL>https://code.europa.eu/oots/tdd/tdd_chapters/-/raw/1.1.0/OOTS-EDM/sch/codelist-include/AGENTCLASSIFICATION-CODELIST_code.sch</sr:downloadURL>
                                <sr:mediaType>XML</sr:mediaType>
                                <sr:title xml:lang="en">Schematron distribution of AgentClassification code codelist</sr:title>
                            </sr:AssetDistribution>
                            <sr:AssetDistribution>
                                <sr:downloadURL>https://code.europa.eu/oots/tdd/tdd_chapters/-/raw/1.1.0/OOTS-EDM/codelists/OOTS/AgentClassification-CodeList.gc</sr:downloadURL>
                                <sr:mediaType>XML</sr:mediaType>
                                <sr:title xml:lang="en">Genericode distribution of AgentClassification codelist</sr:title>
                            </sr:AssetDistribution>
                        </sr:distribution>
                        <sr:isCurrentVersionOf>
                            <sr:AssetRef>
                                <sr:identifier>https://sr.acc.oots.tech.ec.europa.eu/codelists/agentclassification</sr:identifier>
                            </sr:AssetRef>
                        </sr:isCurrentVersionOf>
                        <sr:previousVersion>
                            <sr:VersionedAssetRef>
                                <sr:identifier>https://sr.acc.oots.tech.ec.europa.eu/codelists/agentclassification</sr:identifier>
                                <sr:version>2024-02</sr:version>
                            </sr:VersionedAssetRef>
                        </sr:previousVersion>
                        <sr:isPartOf>
                            <sr:VersionedAssetRef>
                                <sr:identifier>https://sr.acc.oots.tech.ec.europa.eu/specifications/tdds</sr:identifier>
                                <sr:version>1.1.0</sr:version>
                            </sr:VersionedAssetRef>
                        </sr:isPartOf>
                        <sr:inCatalog>
                            <sr:AssetRepository>
                                <sr:title xml:lang="en">OOTS TDDs assets repository</sr:title>
                                <sr:description xml:lang="en">The catalog of the OOTS Technical Design Documents assets</sr:description>
                                <sr:landingPage>https://code.europa.eu/oots/tdd/tdd_chapters/-/tree/master/OOTS-EDM</sr:landingPage>
                                <sr:publisher>
                                    <sr:Agent>
                                        <sr:name xml:lang="en">Directorate-General for Digital Services</sr:name>
                                    </sr:Agent>
                                </sr:publisher>
                                <sr:inCatalog>
                                    <sr:AssetRepository>
                                        <sr:title xml:lang="en">OOTS Semantic Repository</sr:title>
                                        <sr:description xml:lang="en">The catalog of all the semantic assets used to enable the exchange of evidence in the Once-Only Technical System</sr:description>
                                        <sr:homepage>https://sr.oots.tech.ec.europa.eu/sr/home</sr:homepage>
                                        <sr:publisher>
                                            <sr:Agent>
                                                <sr:name xml:lang="en">Directorate-General for Digital Services</sr:name>
                                            </sr:Agent>
                                        </sr:publisher>
                                    </sr:AssetRepository>
                                </sr:inCatalog>
                            </sr:AssetRepository>
                        </sr:inCatalog>
                    </sr:Asset>
                </rim:SlotValue>
            </rim:Slot>
        </rim:RegistryObject>
        <rim:RegistryObject>
            <rim:Slot>
                <rim:SlotValue xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="rim:AnyValueType">
                    <sr:Asset>
                        <sr:identifier>https://sr.acc.oots.tech.ec.europa.eu/codelists/countryidentificationcode</sr:identifier>
                        <sr:versionedIdentifier>https://sr.acc.oots.tech.ec.europa.eu/codelists/countryidentificationcode/1.0.0</sr:versionedIdentifier>
                        <sr:title xml:lang="en">CountryIdentificationCode</sr:title>
                        <sr:description xml:lang="en">Country Identification Code (refactored ISO-3166 alpha-2 subset)</sr:description>
                        <sr:type>CODELIST</sr:type>
                        <sr:publisher>
                            <sr:Agent>
                                <sr:name xml:lang="en">Directorate-General for Digital Services</sr:name>
                            </sr:Agent>
                        </sr:publisher>
                        <sr:status>PUBLISHED</sr:status>
                        <sr:language>ENG</sr:language>
                        <sr:version>1.0.0</sr:version>
                        <sr:versionNotes xml:lang="en">Release notes of version 1.0.0 can be checked at: https://code.europa.eu/oots/tdd/tdd_chapters/-/commits/1.1.0/OOTS-EDM/codelists/OOTS/CountryIdentificationCode-CodeList.gc</sr:versionNotes>
                        <sr:issued>2024-09-08</sr:issued>
                        <sr:modified>2024-09-08</sr:modified>
                        <sr:landingPage>https://code.europa.eu/oots/tdd/tdd_chapters/-/tree/master/OOTS-EDM</sr:landingPage>
                        <sr:distribution>
                            <sr:AssetDistribution>
                                <sr:downloadURL>https://code.europa.eu/oots/tdd/tdd_chapters/-/raw/1.1.0/OOTS-EDM/codelists/OOTS/CountryIdentificationCode-CodeList.gc</sr:downloadURL>
                                <sr:mediaType>XML</sr:mediaType>
                                <sr:title xml:lang="en">Genericode distribution of CountryIdentificationCode codelist</sr:title>
                            </sr:AssetDistribution>
                            <sr:AssetDistribution>
                                <sr:downloadURL>https://code.europa.eu/oots/tdd/tdd_chapters/-/raw/1.1.0/OOTS-EDM/sch/codelist-include/COUNTRYIDENTIFICATIONCODE-CODELIST_code.sch</sr:downloadURL>
                                <sr:mediaType>XML</sr:mediaType>
                                <sr:title xml:lang="en">Schematron distribution of CountryIdentificationCode code codelist</sr:title>
                            </sr:AssetDistribution>
                        </sr:distribution>
                        <sr:isCurrentVersionOf>
                            <sr:AssetRef>
                                <sr:identifier>https://sr.acc.oots.tech.ec.europa.eu/codelists/countryidentificationcode</sr:identifier>
                            </sr:AssetRef>
                        </sr:isCurrentVersionOf>
                        <sr:previousVersion>
                            <sr:VersionedAssetRef>
                                <sr:identifier>https://sr.acc.oots.tech.ec.europa.eu/codelists/countryidentificationcode</sr:identifier>
                                <sr:version>2023-11</sr:version>
                            </sr:VersionedAssetRef>
                        </sr:previousVersion>
                        <sr:isPartOf>
                            <sr:VersionedAssetRef>
                                <sr:identifier>https://sr.acc.oots.tech.ec.europa.eu/specifications/tdds</sr:identifier>
                                <sr:version>1.1.0</sr:version>
                            </sr:VersionedAssetRef>
                        </sr:isPartOf>
                        <sr:inCatalog>
                            <sr:AssetRepository>
                                <sr:title xml:lang="en">OOTS TDDs assets repository</sr:title>
                                <sr:description xml:lang="en">The catalog of the OOTS Technical Design Documents assets</sr:description>
                                <sr:landingPage>https://code.europa.eu/oots/tdd/tdd_chapters/-/tree/master/OOTS-EDM</sr:landingPage>
                                <sr:publisher>
                                    <sr:Agent>
                                        <sr:name xml:lang="en">Directorate-General for Digital Services</sr:name>
                                    </sr:Agent>
                                </sr:publisher>
                                <sr:inCatalog>
                                    <sr:AssetRepository>
                                        <sr:title xml:lang="en">OOTS Semantic Repository</sr:title>
                                        <sr:description xml:lang="en">The catalog of all the semantic assets used to enable the exchange of evidence in the Once-Only Technical System</sr:description>
                                        <sr:homepage>https://sr.oots.tech.ec.europa.eu/sr/home</sr:homepage>
                                        <sr:publisher>
                                            <sr:Agent>
                                                <sr:name xml:lang="en">Directorate-General for Digital Services</sr:name>
                                            </sr:Agent>
                                        </sr:publisher>
                                    </sr:AssetRepository>
                                </sr:inCatalog>
                            </sr:AssetRepository>
                        </sr:inCatalog>
                    </sr:Asset>
                </rim:SlotValue>
            </rim:Slot>
        </rim:RegistryObject>
        <rim:RegistryObject>
            <rim:Slot>
                <rim:SlotValue xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="rim:AnyValueType">
                    <sr:Asset>
                        <sr:identifier>https://sr.acc.oots.tech.ec.europa.eu/codelists/dsderrorcodes</sr:identifier>
                        <sr:versionedIdentifier>https://sr.acc.oots.tech.ec.europa.eu/codelists/dsderrorcodes/1.0.0</sr:versionedIdentifier>
                        <sr:title xml:lang="en">DSDErrorCodes</sr:title>
                        <sr:description xml:lang="en">Data Service Directory Error Response Codes</sr:description>
                        <sr:type>CODELIST</sr:type>
                        <sr:publisher>
                            <sr:Agent>
                                <sr:name xml:lang="en">Directorate-General for Digital Services</sr:name>
                            </sr:Agent>
                        </sr:publisher>
                        <sr:status>PUBLISHED</sr:status>
                        <sr:language>ENG</sr:language>
                        <sr:version>1.0.0</sr:version>
                        <sr:versionNotes xml:lang="en">Release notes of version 1.0.0 can be checked at: https://code.europa.eu/oots/tdd/tdd_chapters/-/commits/1.1.0/OOTS-EDM/codelists/OOTS/DSDErrorCodes-CodeList.gc</sr:versionNotes>
                        <sr:issued>2024-09-08</sr:issued>
                        <sr:modified>2024-09-08</sr:modified>
                        <sr:landingPage>https://code.europa.eu/oots/tdd/tdd_chapters/-/tree/master/OOTS-EDM</sr:landingPage>
                        <sr:distribution>
                            <sr:AssetDistribution>
                                <sr:downloadURL>https://code.europa.eu/oots/tdd/tdd_chapters/-/raw/1.1.0/OOTS-EDM/sch/codelist-include/DSDERRORCODES-CODELIST_name-Type.sch</sr:downloadURL>
                                <sr:mediaType>XML</sr:mediaType>
                                <sr:title xml:lang="en">Schematron distribution of DSDErrorCodes name-Type codelist</sr:title>
                            </sr:AssetDistribution>
                            <sr:AssetDistribution>
                                <sr:downloadURL>https://code.europa.eu/oots/tdd/tdd_chapters/-/raw/1.1.0/OOTS-EDM/codelists/OOTS/DSDErrorCodes-CodeList.gc</sr:downloadURL>
                                <sr:mediaType>XML</sr:mediaType>
                                <sr:title xml:lang="en">Genericode distribution of DSDErrorCodes codelist</sr:title>
                            </sr:AssetDistribution>
                            <sr:AssetDistribution>
                                <sr:downloadURL>https://code.europa.eu/oots/tdd/tdd_chapters/-/raw/1.1.0/OOTS-EDM/sch/codelist-include/DSDERRORCODES-CODELIST_code.sch</sr:downloadURL>
                                <sr:mediaType>XML</sr:mediaType>
                                <sr:title xml:lang="en">Schematron distribution of DSDErrorCodes code codelist</sr:title>
                            </sr:AssetDistribution>
                        </sr:distribution>
                        <sr:isCurrentVersionOf>
                            <sr:AssetRef>
                                <sr:identifier>https://sr.acc.oots.tech.ec.europa.eu/codelists/dsderrorcodes</sr:identifier>
                            </sr:AssetRef>
                        </sr:isCurrentVersionOf>
                        <sr:previousVersion>
                            <sr:VersionedAssetRef>
                                <sr:identifier>https://sr.acc.oots.tech.ec.europa.eu/codelists/dsderrorcodes</sr:identifier>
                                <sr:version>2023-11</sr:version>
                            </sr:VersionedAssetRef>
                        </sr:previousVersion>
                        <sr:isPartOf>
                            <sr:VersionedAssetRef>
                                <sr:identifier>https://sr.acc.oots.tech.ec.europa.eu/specifications/tdds</sr:identifier>
                                <sr:version>1.1.0</sr:version>
                            </sr:VersionedAssetRef>
                        </sr:isPartOf>
                        <sr:inCatalog>
                            <sr:AssetRepository>
                                <sr:title xml:lang="en">OOTS TDDs assets repository</sr:title>
                                <sr:description xml:lang="en">The catalog of the OOTS Technical Design Documents assets</sr:description>
                                <sr:landingPage>https://code.europa.eu/oots/tdd/tdd_chapters/-/tree/master/OOTS-EDM</sr:landingPage>
                                <sr:publisher>
                                    <sr:Agent>
                                        <sr:name xml:lang="en">Directorate-General for Digital Services</sr:name>
                                    </sr:Agent>
                                </sr:publisher>
                                <sr:inCatalog>
                                    <sr:AssetRepository>
                                        <sr:title xml:lang="en">OOTS Semantic Repository</sr:title>
                                        <sr:description xml:lang="en">The catalog of all the semantic assets used to enable the exchange of evidence in the Once-Only Technical System</sr:description>
                                        <sr:homepage>https://sr.oots.tech.ec.europa.eu/sr/home</sr:homepage>
                                        <sr:publisher>
                                            <sr:Agent>
                                                <sr:name xml:lang="en">Directorate-General for Digital Services</sr:name>
                                            </sr:Agent>
                                        </sr:publisher>
                                    </sr:AssetRepository>
                                </sr:inCatalog>
                            </sr:AssetRepository>
                        </sr:inCatalog>
                    </sr:Asset>
                </rim:SlotValue>
            </rim:Slot>
        </rim:RegistryObject>
        <rim:RegistryObject>
            <rim:Slot>
                <rim:SlotValue xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="rim:AnyValueType">
                    <sr:Asset>
                        <sr:identifier>https://sr.acc.oots.tech.ec.europa.eu/codelists/eas</sr:identifier>
                        <sr:versionedIdentifier>https://sr.acc.oots.tech.ec.europa.eu/codelists/eas/2.0.0</sr:versionedIdentifier>
                        <sr:title xml:lang="en">EAS</sr:title>
                        <sr:description xml:lang="en">EAS</sr:description>
                        <sr:type>CODELIST</sr:type>
                        <sr:publisher>
                            <sr:Agent>
                                <sr:name xml:lang="en">Directorate-General for Digital Services</sr:name>
                            </sr:Agent>
                        </sr:publisher>
                        <sr:status>PUBLISHED</sr:status>
                        <sr:language>ENG</sr:language>
                        <sr:version>2.0.0</sr:version>
                        <sr:versionNotes xml:lang="en">Release notes of version 2.0.0 can be checked at: https://code.europa.eu/oots/tdd/tdd_chapters/-/commits/1.1.0/OOTS-EDM/codelists/External/EAS.gc</sr:versionNotes>
                        <sr:issued>2024-09-08</sr:issued>
                        <sr:modified>2024-09-08</sr:modified>
                        <sr:landingPage>https://code.europa.eu/oots/tdd/tdd_chapters/-/tree/master/OOTS-EDM</sr:landingPage>
                        <sr:distribution>
                            <sr:AssetDistribution>
                                <sr:downloadURL>https://code.europa.eu/oots/tdd/tdd_chapters/-/raw/1.1.0/OOTS-EDM/codelists/External/EAS.gc</sr:downloadURL>
                                <sr:mediaType>XML</sr:mediaType>
                                <sr:title xml:lang="en">Genericode distribution of EAS codelist</sr:title>
                            </sr:AssetDistribution>
                        </sr:distribution>
                        <sr:isCurrentVersionOf>
                            <sr:AssetRef>
                                <sr:identifier>https://sr.acc.oots.tech.ec.europa.eu/codelists/eas</sr:identifier>
                            </sr:AssetRef>
                        </sr:isCurrentVersionOf>
                        <sr:previousVersion>
                            <sr:VersionedAssetRef>
                                <sr:identifier>https://sr.acc.oots.tech.ec.europa.eu/codelists/eas</sr:identifier>
                                <sr:version>1.0.0</sr:version>
                            </sr:VersionedAssetRef>
                        </sr:previousVersion>
                        <sr:isPartOf>
                            <sr:VersionedAssetRef>
                                <sr:identifier>https://sr.acc.oots.tech.ec.europa.eu/specifications/tdds</sr:identifier>
                                <sr:version>1.1.0</sr:version>
                            </sr:VersionedAssetRef>
                        </sr:isPartOf>
                        <sr:isSnapshotOf>
                            <sr:ExternalAsset>
                                <sr:identifier>urn:cef.eu:names:identifier:EAS</sr:identifier>
                                <sr:versionedIdentifier>urn:cef.eu:names:identifier:EAS-2024-05-15</sr:versionedIdentifier>
                                <sr:title xml:lang="en">EAS</sr:title>
                                <sr:description xml:lang="en">Electronic Address Scheme codelist</sr:description>
                                <sr:type>CODELIST</sr:type>
                                <sr:version>13.0</sr:version>
                                <sr:landingPage>https://ec.europa.eu/digital-building-blocks/sites/display/DIGITAL/Registry+of+supporting+artefacts+to+implement+EN16931</sr:landingPage>
                            </sr:ExternalAsset>
                        </sr:isSnapshotOf>
                        <sr:inCatalog>
                            <sr:AssetRepository>
                                <sr:title xml:lang="en">OOTS TDDs assets repository</sr:title>
                                <sr:description xml:lang="en">The catalog of the OOTS Technical Design Documents assets</sr:description>
                                <sr:landingPage>https://code.europa.eu/oots/tdd/tdd_chapters/-/tree/master/OOTS-EDM</sr:landingPage>
                                <sr:publisher>
                                    <sr:Agent>
                                        <sr:name xml:lang="en">Directorate-General for Digital Services</sr:name>
                                    </sr:Agent>
                                </sr:publisher>
                                <sr:inCatalog>
                                    <sr:AssetRepository>
                                        <sr:title xml:lang="en">OOTS Semantic Repository</sr:title>
                                        <sr:description xml:lang="en">The catalog of all the semantic assets used to enable the exchange of evidence in the Once-Only Technical System</sr:description>
                                        <sr:homepage>https://sr.oots.tech.ec.europa.eu/sr/home</sr:homepage>
                                        <sr:publisher>
                                            <sr:Agent>
                                                <sr:name xml:lang="en">Directorate-General for Digital Services</sr:name>
                                            </sr:Agent>
                                        </sr:publisher>
                                    </sr:AssetRepository>
                                </sr:inCatalog>
                            </sr:AssetRepository>
                        </sr:inCatalog>
                    </sr:Asset>
                </rim:SlotValue>
            </rim:Slot>
        </rim:RegistryObject>
        <rim:RegistryObject>
            <rim:Slot>
                <rim:SlotValue xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="rim:AnyValueType">
                    <sr:Asset>
                        <sr:identifier>https://sr.acc.oots.tech.ec.europa.eu/codelists/eberrorcodes</sr:identifier>
                        <sr:versionedIdentifier>https://sr.acc.oots.tech.ec.europa.eu/codelists/eberrorcodes/1.0.0</sr:versionedIdentifier>
                        <sr:title xml:lang="en">EBErrorCodes</sr:title>
                        <sr:description xml:lang="en">Evidence Broker Error Response Codes</sr:description>
                        <sr:type>CODELIST</sr:type>
                        <sr:publisher>
                            <sr:Agent>
                                <sr:name xml:lang="en">Directorate-General for Digital Services</sr:name>
                            </sr:Agent>
                        </sr:publisher>
                        <sr:status>PUBLISHED</sr:status>
                        <sr:language>ENG</sr:language>
                        <sr:version>1.0.0</sr:version>
                        <sr:versionNotes xml:lang="en">Release notes of version 1.0.0 can be checked at: https://code.europa.eu/oots/tdd/tdd_chapters/-/commits/1.1.0/OOTS-EDM/codelists/OOTS/EBErrorCodes-CodeList.gc</sr:versionNotes>
                        <sr:issued>2024-09-08</sr:issued>
                        <sr:modified>2024-09-08</sr:modified>
                        <sr:landingPage>https://code.europa.eu/oots/tdd/tdd_chapters/-/tree/master/OOTS-EDM</sr:landingPage>
                        <sr:distribution>
                            <sr:AssetDistribution>
                                <sr:downloadURL>https://code.europa.eu/oots/tdd/tdd_chapters/-/raw/1.1.0/OOTS-EDM/codelists/OOTS/EBErrorCodes-CodeList.gc</sr:downloadURL>
                                <sr:mediaType>XML</sr:mediaType>
                                <sr:title xml:lang="en">Genericode distribution of EBErrorCodes codelist</sr:title>
                            </sr:AssetDistribution>
                            <sr:AssetDistribution>
                                <sr:downloadURL>https://code.europa.eu/oots/tdd/tdd_chapters/-/raw/1.1.0/OOTS-EDM/sch/codelist-include/EBERRORCODES-CODELIST_code.sch</sr:downloadURL>
                                <sr:mediaType>XML</sr:mediaType>
                                <sr:title xml:lang="en">Schematron distribution of EBErrorCodes code codelist</sr:title>
                            </sr:AssetDistribution>
                            <sr:AssetDistribution>
                                <sr:downloadURL>https://code.europa.eu/oots/tdd/tdd_chapters/-/raw/1.1.0/OOTS-EDM/sch/codelist-include/EBERRORCODES-CODELIST_name-Type.sch</sr:downloadURL>
                                <sr:mediaType>XML</sr:mediaType>
                                <sr:title xml:lang="en">Schematron distribution of EBErrorCodes name-Type codelist</sr:title>
                            </sr:AssetDistribution>
                        </sr:distribution>
                        <sr:isCurrentVersionOf>
                            <sr:AssetRef>
                                <sr:identifier>https://sr.acc.oots.tech.ec.europa.eu/codelists/eberrorcodes</sr:identifier>
                            </sr:AssetRef>
                        </sr:isCurrentVersionOf>
                        <sr:previousVersion>
                            <sr:VersionedAssetRef>
                                <sr:identifier>https://sr.acc.oots.tech.ec.europa.eu/codelists/eberrorcodes</sr:identifier>
                                <sr:version>2023-11</sr:version>
                            </sr:VersionedAssetRef>
                        </sr:previousVersion>
                        <sr:isPartOf>
                            <sr:VersionedAssetRef>
                                <sr:identifier>https://sr.acc.oots.tech.ec.europa.eu/specifications/tdds</sr:identifier>
                                <sr:version>1.1.0</sr:version>
                            </sr:VersionedAssetRef>
                        </sr:isPartOf>
                        <sr:inCatalog>
                            <sr:AssetRepository>
                                <sr:title xml:lang="en">OOTS TDDs assets repository</sr:title>
                                <sr:description xml:lang="en">The catalog of the OOTS Technical Design Documents assets</sr:description>
                                <sr:landingPage>https://code.europa.eu/oots/tdd/tdd_chapters/-/tree/master/OOTS-EDM</sr:landingPage>
                                <sr:publisher>
                                    <sr:Agent>
                                        <sr:name xml:lang="en">Directorate-General for Digital Services</sr:name>
                                    </sr:Agent>
                                </sr:publisher>
                                <sr:inCatalog>
                                    <sr:AssetRepository>
                                        <sr:title xml:lang="en">OOTS Semantic Repository</sr:title>
                                        <sr:description xml:lang="en">The catalog of all the semantic assets used to enable the exchange of evidence in the Once-Only Technical System</sr:description>
                                        <sr:homepage>https://sr.oots.tech.ec.europa.eu/sr/home</sr:homepage>
                                        <sr:publisher>
                                            <sr:Agent>
                                                <sr:name xml:lang="en">Directorate-General for Digital Services</sr:name>
                                            </sr:Agent>
                                        </sr:publisher>
                                    </sr:AssetRepository>
                                </sr:inCatalog>
                            </sr:AssetRepository>
                        </sr:inCatalog>
                    </sr:Asset>
                </rim:SlotValue>
            </rim:Slot>
        </rim:RegistryObject>
        <rim:RegistryObject>
            <rim:Slot>
                <rim:SlotValue xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="rim:AnyValueType">
                    <sr:Asset>
                        <sr:identifier>https://sr.acc.oots.tech.ec.europa.eu/codelists/edmerrorcodes</sr:identifier>
                        <sr:versionedIdentifier>https://sr.acc.oots.tech.ec.europa.eu/codelists/edmerrorcodes/1.0.0</sr:versionedIdentifier>
                        <sr:title xml:lang="en">EDMErrorCodes</sr:title>
                        <sr:description xml:lang="en">Exchange Data Model Error Response Codes </sr:description>
                        <sr:type>CODELIST</sr:type>
                        <sr:publisher>
                            <sr:Agent>
                                <sr:name xml:lang="en">Directorate-General for Digital Services</sr:name>
                            </sr:Agent>
                        </sr:publisher>
                        <sr:status>PUBLISHED</sr:status>
                        <sr:language>ENG</sr:language>
                        <sr:version>1.0.0</sr:version>
                        <sr:versionNotes xml:lang="en">Release notes of version 1.0.0 can be checked at: https://code.europa.eu/oots/tdd/tdd_chapters/-/commits/1.1.0/OOTS-EDM/codelists/OOTS/EDMErrorCodes-CodeList.gc</sr:versionNotes>
                        <sr:issued>2024-09-08</sr:issued>
                        <sr:modified>2024-09-08</sr:modified>
                        <sr:landingPage>https://code.europa.eu/oots/tdd/tdd_chapters/-/tree/master/OOTS-EDM</sr:landingPage>
                        <sr:distribution>
                            <sr:AssetDistribution>
                                <sr:downloadURL>https://code.europa.eu/oots/tdd/tdd_chapters/-/raw/1.1.0/OOTS-EDM/codelists/OOTS/EDMErrorCodes-CodeList.gc</sr:downloadURL>
                                <sr:mediaType>XML</sr:mediaType>
                                <sr:title xml:lang="en">Genericode distribution of EDMErrorCodes codelist</sr:title>
                            </sr:AssetDistribution>
                            <sr:AssetDistribution>
                                <sr:downloadURL>https://code.europa.eu/oots/tdd/tdd_chapters/-/raw/1.1.0/OOTS-EDM/sch/codelist-include/EDMERRORCODES-CODELIST_code.sch</sr:downloadURL>
                                <sr:mediaType>XML</sr:mediaType>
                                <sr:title xml:lang="en">Schematron distribution of EDMErrorCodes code codelist</sr:title>
                            </sr:AssetDistribution>
                            <sr:AssetDistribution>
                                <sr:downloadURL>https://code.europa.eu/oots/tdd/tdd_chapters/-/raw/1.1.0/OOTS-EDM/sch/codelist-include/EDMERRORCODES-CODELIST_name-Type.sch</sr:downloadURL>
                                <sr:mediaType>XML</sr:mediaType>
                                <sr:title xml:lang="en">Schematron distribution of EDMErrorCodes name-Type codelist</sr:title>
                            </sr:AssetDistribution>
                        </sr:distribution>
                        <sr:isCurrentVersionOf>
                            <sr:AssetRef>
                                <sr:identifier>https://sr.acc.oots.tech.ec.europa.eu/codelists/edmerrorcodes</sr:identifier>
                            </sr:AssetRef>
                        </sr:isCurrentVersionOf>
                        <sr:previousVersion>
                            <sr:VersionedAssetRef>
                                <sr:identifier>https://sr.acc.oots.tech.ec.europa.eu/codelists/edmerrorcodes</sr:identifier>
                                <sr:version>2023-11</sr:version>
                            </sr:VersionedAssetRef>
                        </sr:previousVersion>
                        <sr:isPartOf>
                            <sr:VersionedAssetRef>
                                <sr:identifier>https://sr.acc.oots.tech.ec.europa.eu/specifications/tdds</sr:identifier>
                                <sr:version>1.1.0</sr:version>
                            </sr:VersionedAssetRef>
                        </sr:isPartOf>
                        <sr:inCatalog>
                            <sr:AssetRepository>
                                <sr:title xml:lang="en">OOTS TDDs assets repository</sr:title>
                                <sr:description xml:lang="en">The catalog of the OOTS Technical Design Documents assets</sr:description>
                                <sr:landingPage>https://code.europa.eu/oots/tdd/tdd_chapters/-/tree/master/OOTS-EDM</sr:landingPage>
                                <sr:publisher>
                                    <sr:Agent>
                                        <sr:name xml:lang="en">Directorate-General for Digital Services</sr:name>
                                    </sr:Agent>
                                </sr:publisher>
                                <sr:inCatalog>
                                    <sr:AssetRepository>
                                        <sr:title xml:lang="en">OOTS Semantic Repository</sr:title>
                                        <sr:description xml:lang="en">The catalog of all the semantic assets used to enable the exchange of evidence in the Once-Only Technical System</sr:description>
                                        <sr:homepage>https://sr.oots.tech.ec.europa.eu/sr/home</sr:homepage>
                                        <sr:publisher>
                                            <sr:Agent>
                                                <sr:name xml:lang="en">Directorate-General for Digital Services</sr:name>
                                            </sr:Agent>
                                        </sr:publisher>
                                    </sr:AssetRepository>
                                </sr:inCatalog>
                            </sr:AssetRepository>
                        </sr:inCatalog>
                    </sr:Asset>
                </rim:SlotValue>
            </rim:Slot>
        </rim:RegistryObject>
        <rim:RegistryObject>
            <rim:Slot>
                <rim:SlotValue xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="rim:AnyValueType">
                    <sr:Asset>
                        <sr:identifier>https://sr.acc.oots.tech.ec.europa.eu/codelists/eea_country</sr:identifier>
                        <sr:versionedIdentifier>https://sr.acc.oots.tech.ec.europa.eu/codelists/eea_country/1.0.0</sr:versionedIdentifier>
                        <sr:title xml:lang="en">EEA_Country</sr:title>
                        <sr:description xml:lang="en">Country Identification Code EEA Subset (refactored ISO-3166 alpha-2 subset of EEA countries)</sr:description>
                        <sr:type>CODELIST</sr:type>
                        <sr:publisher>
                            <sr:Agent>
                                <sr:name xml:lang="en">Directorate-General for Digital Services</sr:name>
                            </sr:Agent>
                        </sr:publisher>
                        <sr:status>PUBLISHED</sr:status>
                        <sr:language>ENG</sr:language>
                        <sr:version>1.0.0</sr:version>
                        <sr:versionNotes xml:lang="en">Release notes of version 1.0.0 can be checked at: https://code.europa.eu/oots/tdd/tdd_chapters/-/commits/1.1.0/OOTS-EDM/codelists/OOTS/EEA_Country-CodeList.gc</sr:versionNotes>
                        <sr:issued>2024-09-08</sr:issued>
                        <sr:modified>2024-09-08</sr:modified>
                        <sr:landingPage>https://code.europa.eu/oots/tdd/tdd_chapters/-/tree/master/OOTS-EDM</sr:landingPage>
                        <sr:distribution>
                            <sr:AssetDistribution>
                                <sr:downloadURL>https://code.europa.eu/oots/tdd/tdd_chapters/-/raw/1.1.0/OOTS-EDM/codelists/OOTS/EEA_Country-CodeList.gc</sr:downloadURL>
                                <sr:mediaType>XML</sr:mediaType>
                                <sr:title xml:lang="en">Genericode distribution of EEA_Country codelist</sr:title>
                            </sr:AssetDistribution>
                            <sr:AssetDistribution>
                                <sr:downloadURL>https://code.europa.eu/oots/tdd/tdd_chapters/-/raw/1.1.0/OOTS-EDM/sch/codelist-include/EEA_COUNTRY-CODELIST_code.sch</sr:downloadURL>
                                <sr:mediaType>XML</sr:mediaType>
                                <sr:title xml:lang="en">Schematron distribution of EEA_Country code codelist</sr:title>
                            </sr:AssetDistribution>
                        </sr:distribution>
                        <sr:isCurrentVersionOf>
                            <sr:AssetRef>
                                <sr:identifier>https://sr.acc.oots.tech.ec.europa.eu/codelists/eea_country</sr:identifier>
                            </sr:AssetRef>
                        </sr:isCurrentVersionOf>
                        <sr:previousVersion>
                            <sr:VersionedAssetRef>
                                <sr:identifier>https://sr.acc.oots.tech.ec.europa.eu/codelists/eea_country</sr:identifier>
                                <sr:version>2023-11</sr:version>
                            </sr:VersionedAssetRef>
                        </sr:previousVersion>
                        <sr:isPartOf>
                            <sr:VersionedAssetRef>
                                <sr:identifier>https://sr.acc.oots.tech.ec.europa.eu/specifications/tdds</sr:identifier>
                                <sr:version>1.1.0</sr:version>
                            </sr:VersionedAssetRef>
                        </sr:isPartOf>
                        <sr:inCatalog>
                            <sr:AssetRepository>
                                <sr:title xml:lang="en">OOTS TDDs assets repository</sr:title>
                                <sr:description xml:lang="en">The catalog of the OOTS Technical Design Documents assets</sr:description>
                                <sr:landingPage>https://code.europa.eu/oots/tdd/tdd_chapters/-/tree/master/OOTS-EDM</sr:landingPage>
                                <sr:publisher>
                                    <sr:Agent>
                                        <sr:name xml:lang="en">Directorate-General for Digital Services</sr:name>
                                    </sr:Agent>
                                </sr:publisher>
                                <sr:inCatalog>
                                    <sr:AssetRepository>
                                        <sr:title xml:lang="en">OOTS Semantic Repository</sr:title>
                                        <sr:description xml:lang="en">The catalog of all the semantic assets used to enable the exchange of evidence in the Once-Only Technical System</sr:description>
                                        <sr:homepage>https://sr.oots.tech.ec.europa.eu/sr/home</sr:homepage>
                                        <sr:publisher>
                                            <sr:Agent>
                                                <sr:name xml:lang="en">Directorate-General for Digital Services</sr:name>
                                            </sr:Agent>
                                        </sr:publisher>
                                    </sr:AssetRepository>
                                </sr:inCatalog>
                            </sr:AssetRepository>
                        </sr:inCatalog>
                    </sr:Asset>
                </rim:SlotValue>
            </rim:Slot>
        </rim:RegistryObject>
        <rim:RegistryObject>
            <rim:Slot>
                <rim:SlotValue xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="rim:AnyValueType">
                    <sr:Asset>
                        <sr:identifier>https://sr.acc.oots.tech.ec.europa.eu/codelists/errorseverity</sr:identifier>
                        <sr:versionedIdentifier>https://sr.acc.oots.tech.ec.europa.eu/codelists/errorseverity/1.0.0</sr:versionedIdentifier>
                        <sr:title xml:lang="en">ErrorSeverity</sr:title>
                        <sr:description xml:lang="en">Error Severity</sr:description>
                        <sr:type>CODELIST</sr:type>
                        <sr:publisher>
                            <sr:Agent>
                                <sr:name xml:lang="en">Directorate-General for Digital Services</sr:name>
                            </sr:Agent>
                        </sr:publisher>
                        <sr:status>PUBLISHED</sr:status>
                        <sr:language>ENG</sr:language>
                        <sr:version>1.0.0</sr:version>
                        <sr:versionNotes xml:lang="en">Release notes of version 1.0.0 can be checked at: https://code.europa.eu/oots/tdd/tdd_chapters/-/commits/1.1.0/OOTS-EDM/codelists/OOTS/ErrorSeverity-CodeList.gc</sr:versionNotes>
                        <sr:issued>2024-09-08</sr:issued>
                        <sr:modified>2024-09-08</sr:modified>
                        <sr:landingPage>https://code.europa.eu/oots/tdd/tdd_chapters/-/tree/master/OOTS-EDM</sr:landingPage>
                        <sr:distribution>
                            <sr:AssetDistribution>
                                <sr:downloadURL>https://code.europa.eu/oots/tdd/tdd_chapters/-/raw/1.1.0/OOTS-EDM/codelists/OOTS/ErrorSeverity-CodeList.gc</sr:downloadURL>
                                <sr:mediaType>XML</sr:mediaType>
                                <sr:title xml:lang="en">Genericode distribution of ErrorSeverity codelist</sr:title>
                            </sr:AssetDistribution>
                            <sr:AssetDistribution>
                                <sr:downloadURL>https://code.europa.eu/oots/tdd/tdd_chapters/-/raw/1.1.0/OOTS-EDM/sch/codelist-include/ERRORSEVERITY-CODELIST_code.sch</sr:downloadURL>
                                <sr:mediaType>XML</sr:mediaType>
                                <sr:title xml:lang="en">Schematron distribution of ErrorSeverity code codelist</sr:title>
                            </sr:AssetDistribution>
                        </sr:distribution>
                        <sr:isCurrentVersionOf>
                            <sr:AssetRef>
                                <sr:identifier>https://sr.acc.oots.tech.ec.europa.eu/codelists/errorseverity</sr:identifier>
                            </sr:AssetRef>
                        </sr:isCurrentVersionOf>
                        <sr:previousVersion>
                            <sr:VersionedAssetRef>
                                <sr:identifier>https://sr.acc.oots.tech.ec.europa.eu/codelists/errorseverity</sr:identifier>
                                <sr:version>2023-11</sr:version>
                            </sr:VersionedAssetRef>
                        </sr:previousVersion>
                        <sr:isPartOf>
                            <sr:VersionedAssetRef>
                                <sr:identifier>https://sr.acc.oots.tech.ec.europa.eu/specifications/tdds</sr:identifier>
                                <sr:version>1.1.0</sr:version>
                            </sr:VersionedAssetRef>
                        </sr:isPartOf>
                        <sr:inCatalog>
                            <sr:AssetRepository>
                                <sr:title xml:lang="en">OOTS TDDs assets repository</sr:title>
                                <sr:description xml:lang="en">The catalog of the OOTS Technical Design Documents assets</sr:description>
                                <sr:landingPage>https://code.europa.eu/oots/tdd/tdd_chapters/-/tree/master/OOTS-EDM</sr:landingPage>
                                <sr:publisher>
                                    <sr:Agent>
                                        <sr:name xml:lang="en">Directorate-General for Digital Services</sr:name>
                                    </sr:Agent>
                                </sr:publisher>
                                <sr:inCatalog>
                                    <sr:AssetRepository>
                                        <sr:title xml:lang="en">OOTS Semantic Repository</sr:title>
                                        <sr:description xml:lang="en">The catalog of all the semantic assets used to enable the exchange of evidence in the Once-Only Technical System</sr:description>
                                        <sr:homepage>https://sr.oots.tech.ec.europa.eu/sr/home</sr:homepage>
                                        <sr:publisher>
                                            <sr:Agent>
                                                <sr:name xml:lang="en">Directorate-General for Digital Services</sr:name>
                                            </sr:Agent>
                                        </sr:publisher>
                                    </sr:AssetRepository>
                                </sr:inCatalog>
                            </sr:AssetRepository>
                        </sr:inCatalog>
                    </sr:Asset>
                </rim:SlotValue>
            </rim:Slot>
        </rim:RegistryObject>
        <rim:RegistryObject>
            <rim:Slot>
                <rim:SlotValue xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="rim:AnyValueType">
                    <sr:Asset>
                        <sr:identifier>https://sr.acc.oots.tech.ec.europa.eu/codelists/identifierschemes</sr:identifier>
                        <sr:versionedIdentifier>https://sr.acc.oots.tech.ec.europa.eu/codelists/identifierschemes/1.0.0</sr:versionedIdentifier>
                        <sr:title xml:lang="en">IdentifierSchemes</sr:title>
                        <sr:description xml:lang="en">eIDAS Legal Person Identifier Schemes</sr:description>
                        <sr:type>CODELIST</sr:type>
                        <sr:publisher>
                            <sr:Agent>
                                <sr:name xml:lang="en">Directorate-General for Digital Services</sr:name>
                            </sr:Agent>
                        </sr:publisher>
                        <sr:status>PUBLISHED</sr:status>
                        <sr:language>ENG</sr:language>
                        <sr:version>1.0.0</sr:version>
                        <sr:versionNotes xml:lang="en">Release notes of version 1.0.0 can be checked at: https://code.europa.eu/oots/tdd/tdd_chapters/-/commits/1.1.0/OOTS-EDM/codelists/OOTS/IdentifierSchemes-CodeList.gc</sr:versionNotes>
                        <sr:issued>2024-09-08</sr:issued>
                        <sr:modified>2024-09-08</sr:modified>
                        <sr:landingPage>https://code.europa.eu/oots/tdd/tdd_chapters/-/tree/master/OOTS-EDM</sr:landingPage>
                        <sr:distribution>
                            <sr:AssetDistribution>
                                <sr:downloadURL>https://code.europa.eu/oots/tdd/tdd_chapters/-/raw/1.1.0/OOTS-EDM/codelists/OOTS/IdentifierSchemes-CodeList.gc</sr:downloadURL>
                                <sr:mediaType>XML</sr:mediaType>
                                <sr:title xml:lang="en">Genericode distribution of IdentifierSchemes codelist</sr:title>
                            </sr:AssetDistribution>
                            <sr:AssetDistribution>
                                <sr:downloadURL>https://code.europa.eu/oots/tdd/tdd_chapters/-/raw/1.1.0/OOTS-EDM/sch/codelist-include/IDENTIFIERSCHEMES-CODELIST_code.sch</sr:downloadURL>
                                <sr:mediaType>XML</sr:mediaType>
                                <sr:title xml:lang="en">Schematron distribution of IdentifierSchemes code codelist</sr:title>
                            </sr:AssetDistribution>
                        </sr:distribution>
                        <sr:isCurrentVersionOf>
                            <sr:AssetRef>
                                <sr:identifier>https://sr.acc.oots.tech.ec.europa.eu/codelists/identifierschemes</sr:identifier>
                            </sr:AssetRef>
                        </sr:isCurrentVersionOf>
                        <sr:previousVersion>
                            <sr:VersionedAssetRef>
                                <sr:identifier>https://sr.acc.oots.tech.ec.europa.eu/codelists/identifierschemes</sr:identifier>
                                <sr:version>2023-11</sr:version>
                            </sr:VersionedAssetRef>
                        </sr:previousVersion>
                        <sr:isPartOf>
                            <sr:VersionedAssetRef>
                                <sr:identifier>https://sr.acc.oots.tech.ec.europa.eu/specifications/tdds</sr:identifier>
                                <sr:version>1.1.0</sr:version>
                            </sr:VersionedAssetRef>
                        </sr:isPartOf>
                        <sr:inCatalog>
                            <sr:AssetRepository>
                                <sr:title xml:lang="en">OOTS TDDs assets repository</sr:title>
                                <sr:description xml:lang="en">The catalog of the OOTS Technical Design Documents assets</sr:description>
                                <sr:landingPage>https://code.europa.eu/oots/tdd/tdd_chapters/-/tree/master/OOTS-EDM</sr:landingPage>
                                <sr:publisher>
                                    <sr:Agent>
                                        <sr:name xml:lang="en">Directorate-General for Digital Services</sr:name>
                                    </sr:Agent>
                                </sr:publisher>
                                <sr:inCatalog>
                                    <sr:AssetRepository>
                                        <sr:title xml:lang="en">OOTS Semantic Repository</sr:title>
                                        <sr:description xml:lang="en">The catalog of all the semantic assets used to enable the exchange of evidence in the Once-Only Technical System</sr:description>
                                        <sr:homepage>https://sr.oots.tech.ec.europa.eu/sr/home</sr:homepage>
                                        <sr:publisher>
                                            <sr:Agent>
                                                <sr:name xml:lang="en">Directorate-General for Digital Services</sr:name>
                                            </sr:Agent>
                                        </sr:publisher>
                                    </sr:AssetRepository>
                                </sr:inCatalog>
                            </sr:AssetRepository>
                        </sr:inCatalog>
                    </sr:Asset>
                </rim:SlotValue>
            </rim:Slot>
        </rim:RegistryObject>
        <rim:RegistryObject>
            <rim:Slot>
                <rim:SlotValue xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="rim:AnyValueType">
                    <sr:Asset>
                        <sr:identifier>https://sr.acc.oots.tech.ec.europa.eu/codelists/jurisdictionlevel</sr:identifier>
                        <sr:versionedIdentifier>https://sr.acc.oots.tech.ec.europa.eu/codelists/jurisdictionlevel/1.0.0</sr:versionedIdentifier>
                        <sr:title xml:lang="en">JurisdictionLevel</sr:title>
                        <sr:description xml:lang="en">Jurisdiction Level</sr:description>
                        <sr:type>CODELIST</sr:type>
                        <sr:publisher>
                            <sr:Agent>
                                <sr:name xml:lang="en">Directorate-General for Digital Services</sr:name>
                            </sr:Agent>
                        </sr:publisher>
                        <sr:status>PUBLISHED</sr:status>
                        <sr:language>ENG</sr:language>
                        <sr:version>1.0.0</sr:version>
                        <sr:versionNotes xml:lang="en">Release notes of version 1.0.0 can be checked at: https://code.europa.eu/oots/tdd/tdd_chapters/-/commits/1.1.0/OOTS-EDM/codelists/OOTS/JurisdictionLevel-CodeList.gc</sr:versionNotes>
                        <sr:issued>2024-09-08</sr:issued>
                        <sr:modified>2024-09-08</sr:modified>
                        <sr:landingPage>https://code.europa.eu/oots/tdd/tdd_chapters/-/tree/master/OOTS-EDM</sr:landingPage>
                        <sr:distribution>
                            <sr:AssetDistribution>
                                <sr:downloadURL>https://code.europa.eu/oots/tdd/tdd_chapters/-/raw/1.1.0/OOTS-EDM/codelists/OOTS/JurisdictionLevel-CodeList.gc</sr:downloadURL>
                                <sr:mediaType>XML</sr:mediaType>
                                <sr:title xml:lang="en">Genericode distribution of JurisdictionLevel codelist</sr:title>
                            </sr:AssetDistribution>
                            <sr:AssetDistribution>
                                <sr:downloadURL>https://code.europa.eu/oots/tdd/tdd_chapters/-/raw/1.1.0/OOTS-EDM/sch/codelist-include/JURISDICTIONLEVEL-CODELIST_code.sch</sr:downloadURL>
                                <sr:mediaType>XML</sr:mediaType>
                                <sr:title xml:lang="en">Schematron distribution of JurisdictionLevel code codelist</sr:title>
                            </sr:AssetDistribution>
                        </sr:distribution>
                        <sr:isCurrentVersionOf>
                            <sr:AssetRef>
                                <sr:identifier>https://sr.acc.oots.tech.ec.europa.eu/codelists/jurisdictionlevel</sr:identifier>
                            </sr:AssetRef>
                        </sr:isCurrentVersionOf>
                        <sr:previousVersion>
                            <sr:VersionedAssetRef>
                                <sr:identifier>https://sr.acc.oots.tech.ec.europa.eu/codelists/jurisdictionlevel</sr:identifier>
                                <sr:version>2023-11</sr:version>
                            </sr:VersionedAssetRef>
                        </sr:previousVersion>
                        <sr:isPartOf>
                            <sr:VersionedAssetRef>
                                <sr:identifier>https://sr.acc.oots.tech.ec.europa.eu/specifications/tdds</sr:identifier>
                                <sr:version>1.1.0</sr:version>
                            </sr:VersionedAssetRef>
                        </sr:isPartOf>
                        <sr:inCatalog>
                            <sr:AssetRepository>
                                <sr:title xml:lang="en">OOTS TDDs assets repository</sr:title>
                                <sr:description xml:lang="en">The catalog of the OOTS Technical Design Documents assets</sr:description>
                                <sr:landingPage>https://code.europa.eu/oots/tdd/tdd_chapters/-/tree/master/OOTS-EDM</sr:landingPage>
                                <sr:publisher>
                                    <sr:Agent>
                                        <sr:name xml:lang="en">Directorate-General for Digital Services</sr:name>
                                    </sr:Agent>
                                </sr:publisher>
                                <sr:inCatalog>
                                    <sr:AssetRepository>
                                        <sr:title xml:lang="en">OOTS Semantic Repository</sr:title>
                                        <sr:description xml:lang="en">The catalog of all the semantic assets used to enable the exchange of evidence in the Once-Only Technical System</sr:description>
                                        <sr:homepage>https://sr.oots.tech.ec.europa.eu/sr/home</sr:homepage>
                                        <sr:publisher>
                                            <sr:Agent>
                                                <sr:name xml:lang="en">Directorate-General for Digital Services</sr:name>
                                            </sr:Agent>
                                        </sr:publisher>
                                    </sr:AssetRepository>
                                </sr:inCatalog>
                            </sr:AssetRepository>
                        </sr:inCatalog>
                    </sr:Asset>
                </rim:SlotValue>
            </rim:Slot>
        </rim:RegistryObject>
        <rim:RegistryObject>
            <rim:Slot>
                <rim:SlotValue xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="rim:AnyValueType">
                    <sr:Asset>
                        <sr:identifier>https://sr.acc.oots.tech.ec.europa.eu/codelists/lau</sr:identifier>
                        <sr:versionedIdentifier>https://sr.acc.oots.tech.ec.europa.eu/codelists/lau/1.0.0</sr:versionedIdentifier>
                        <sr:title xml:lang="en">LAU</sr:title>
                        <sr:description xml:lang="en">Local Administrative Units 2022</sr:description>
                        <sr:type>CODELIST</sr:type>
                        <sr:publisher>
                            <sr:Agent>
                                <sr:name xml:lang="en">Directorate-General for Digital Services</sr:name>
                            </sr:Agent>
                        </sr:publisher>
                        <sr:status>PUBLISHED</sr:status>
                        <sr:language>ENG</sr:language>
                        <sr:version>1.0.0</sr:version>
                        <sr:versionNotes xml:lang="en">Release notes of version 1.0.0 can be checked at: https://code.europa.eu/oots/tdd/tdd_chapters/-/commits/1.1.0/OOTS-EDM/codelists/OOTS/LAU2022-CodeList.gc</sr:versionNotes>
                        <sr:issued>2024-09-08</sr:issued>
                        <sr:modified>2024-09-08</sr:modified>
                        <sr:landingPage>https://code.europa.eu/oots/tdd/tdd_chapters/-/tree/master/OOTS-EDM</sr:landingPage>
                        <sr:distribution>
                            <sr:AssetDistribution>
                                <sr:downloadURL>https://code.europa.eu/oots/tdd/tdd_chapters/-/raw/1.1.0/OOTS-EDM/codelists/OOTS/LAU2022-CodeList.gc</sr:downloadURL>
                                <sr:mediaType>XML</sr:mediaType>
                                <sr:title xml:lang="en">Genericode distribution of LAU codelist</sr:title>
                            </sr:AssetDistribution>
                        </sr:distribution>
                        <sr:isCurrentVersionOf>
                            <sr:AssetRef>
                                <sr:identifier>https://sr.acc.oots.tech.ec.europa.eu/codelists/lau</sr:identifier>
                            </sr:AssetRef>
                        </sr:isCurrentVersionOf>
                        <sr:previousVersion>
                            <sr:VersionedAssetRef>
                                <sr:identifier>https://sr.acc.oots.tech.ec.europa.eu/codelists/lau</sr:identifier>
                                <sr:version>2023-11</sr:version>
                            </sr:VersionedAssetRef>
                        </sr:previousVersion>
                        <sr:isPartOf>
                            <sr:VersionedAssetRef>
                                <sr:identifier>https://sr.acc.oots.tech.ec.europa.eu/specifications/tdds</sr:identifier>
                                <sr:version>1.1.0</sr:version>
                            </sr:VersionedAssetRef>
                        </sr:isPartOf>
                        <sr:inCatalog>
                            <sr:AssetRepository>
                                <sr:title xml:lang="en">OOTS TDDs assets repository</sr:title>
                                <sr:description xml:lang="en">The catalog of the OOTS Technical Design Documents assets</sr:description>
                                <sr:landingPage>https://code.europa.eu/oots/tdd/tdd_chapters/-/tree/master/OOTS-EDM</sr:landingPage>
                                <sr:publisher>
                                    <sr:Agent>
                                        <sr:name xml:lang="en">Directorate-General for Digital Services</sr:name>
                                    </sr:Agent>
                                </sr:publisher>
                                <sr:inCatalog>
                                    <sr:AssetRepository>
                                        <sr:title xml:lang="en">OOTS Semantic Repository</sr:title>
                                        <sr:description xml:lang="en">The catalog of all the semantic assets used to enable the exchange of evidence in the Once-Only Technical System</sr:description>
                                        <sr:homepage>https://sr.oots.tech.ec.europa.eu/sr/home</sr:homepage>
                                        <sr:publisher>
                                            <sr:Agent>
                                                <sr:name xml:lang="en">Directorate-General for Digital Services</sr:name>
                                            </sr:Agent>
                                        </sr:publisher>
                                    </sr:AssetRepository>
                                </sr:inCatalog>
                            </sr:AssetRepository>
                        </sr:inCatalog>
                    </sr:Asset>
                </rim:SlotValue>
            </rim:Slot>
        </rim:RegistryObject>
        <rim:RegistryObject>
            <rim:Slot>
                <rim:SlotValue xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="rim:AnyValueType">
                    <sr:Asset>
                        <sr:identifier>https://sr.acc.oots.tech.ec.europa.eu/codelists/lcmerrorcodes</sr:identifier>
                        <sr:versionedIdentifier>https://sr.acc.oots.tech.ec.europa.eu/codelists/lcmerrorcodes/1.0.0</sr:versionedIdentifier>
                        <sr:title xml:lang="en">LCMErrorCodes</sr:title>
                        <sr:description xml:lang="en">Life Cycle Management Error Response Codes</sr:description>
                        <sr:type>CODELIST</sr:type>
                        <sr:publisher>
                            <sr:Agent>
                                <sr:name xml:lang="en">Directorate-General for Digital Services</sr:name>
                            </sr:Agent>
                        </sr:publisher>
                        <sr:status>PUBLISHED</sr:status>
                        <sr:language>ENG</sr:language>
                        <sr:version>1.0.0</sr:version>
                        <sr:versionNotes xml:lang="en">Release notes of version 1.0.0 can be checked at: https://code.europa.eu/oots/tdd/tdd_chapters/-/commits/1.1.0/OOTS-EDM/codelists/OOTS/LCMErrorCodes-CodeList.gc</sr:versionNotes>
                        <sr:issued>2024-09-08</sr:issued>
                        <sr:modified>2024-09-08</sr:modified>
                        <sr:landingPage>https://code.europa.eu/oots/tdd/tdd_chapters/-/tree/master/OOTS-EDM</sr:landingPage>
                        <sr:distribution>
                            <sr:AssetDistribution>
                                <sr:downloadURL>https://code.europa.eu/oots/tdd/tdd_chapters/-/raw/1.1.0/OOTS-EDM/codelists/OOTS/LCMErrorCodes-CodeList.gc</sr:downloadURL>
                                <sr:mediaType>XML</sr:mediaType>
                                <sr:title xml:lang="en">Genericode distribution of LCMErrorCodes codelist</sr:title>
                            </sr:AssetDistribution>
                            <sr:AssetDistribution>
                                <sr:downloadURL>https://code.europa.eu/oots/tdd/tdd_chapters/-/raw/1.1.0/OOTS-EDM/sch/codelist-include/LCMERRORCODES-CODELIST_code.sch</sr:downloadURL>
                                <sr:mediaType>XML</sr:mediaType>
                                <sr:title xml:lang="en">Schematron distribution of LCMErrorCodes code codelist</sr:title>
                            </sr:AssetDistribution>
                            <sr:AssetDistribution>
                                <sr:downloadURL>https://code.europa.eu/oots/tdd/tdd_chapters/-/raw/1.1.0/OOTS-EDM/sch/codelist-include/LCMERRORCODES-CODELIST_name-Type.sch</sr:downloadURL>
                                <sr:mediaType>XML</sr:mediaType>
                                <sr:title xml:lang="en">Schematron distribution of LCMErrorCodes name-Type codelist</sr:title>
                            </sr:AssetDistribution>
                        </sr:distribution>
                        <sr:isCurrentVersionOf>
                            <sr:AssetRef>
                                <sr:identifier>https://sr.acc.oots.tech.ec.europa.eu/codelists/lcmerrorcodes</sr:identifier>
                            </sr:AssetRef>
                        </sr:isCurrentVersionOf>
                        <sr:previousVersion>
                            <sr:VersionedAssetRef>
                                <sr:identifier>https://sr.acc.oots.tech.ec.europa.eu/codelists/lcmerrorcodes</sr:identifier>
                                <sr:version>2023-11</sr:version>
                            </sr:VersionedAssetRef>
                        </sr:previousVersion>
                        <sr:isPartOf>
                            <sr:VersionedAssetRef>
                                <sr:identifier>https://sr.acc.oots.tech.ec.europa.eu/specifications/tdds</sr:identifier>
                                <sr:version>1.1.0</sr:version>
                            </sr:VersionedAssetRef>
                        </sr:isPartOf>
                        <sr:inCatalog>
                            <sr:AssetRepository>
                                <sr:title xml:lang="en">OOTS TDDs assets repository</sr:title>
                                <sr:description xml:lang="en">The catalog of the OOTS Technical Design Documents assets</sr:description>
                                <sr:landingPage>https://code.europa.eu/oots/tdd/tdd_chapters/-/tree/master/OOTS-EDM</sr:landingPage>
                                <sr:publisher>
                                    <sr:Agent>
                                        <sr:name xml:lang="en">Directorate-General for Digital Services</sr:name>
                                    </sr:Agent>
                                </sr:publisher>
                                <sr:inCatalog>
                                    <sr:AssetRepository>
                                        <sr:title xml:lang="en">OOTS Semantic Repository</sr:title>
                                        <sr:description xml:lang="en">The catalog of all the semantic assets used to enable the exchange of evidence in the Once-Only Technical System</sr:description>
                                        <sr:homepage>https://sr.oots.tech.ec.europa.eu/sr/home</sr:homepage>
                                        <sr:publisher>
                                            <sr:Agent>
                                                <sr:name xml:lang="en">Directorate-General for Digital Services</sr:name>
                                            </sr:Agent>
                                        </sr:publisher>
                                    </sr:AssetRepository>
                                </sr:inCatalog>
                            </sr:AssetRepository>
                        </sr:inCatalog>
                    </sr:Asset>
                </rim:SlotValue>
            </rim:Slot>
        </rim:RegistryObject>
        <rim:RegistryObject>
            <rim:Slot>
                <rim:SlotValue xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="rim:AnyValueType">
                    <sr:Asset>
                        <sr:identifier>https://sr.acc.oots.tech.ec.europa.eu/codelists/languagecode</sr:identifier>
                        <sr:versionedIdentifier>https://sr.acc.oots.tech.ec.europa.eu/codelists/languagecode/1.0.0</sr:versionedIdentifier>
                        <sr:title xml:lang="en">LanguageCode</sr:title>
                        <sr:description xml:lang="en">Language Code (refactored ISO 639-1 alpha-2 subset)</sr:description>
                        <sr:type>CODELIST</sr:type>
                        <sr:publisher>
                            <sr:Agent>
                                <sr:name xml:lang="en">Directorate-General for Digital Services</sr:name>
                            </sr:Agent>
                        </sr:publisher>
                        <sr:status>PUBLISHED</sr:status>
                        <sr:language>ENG</sr:language>
                        <sr:version>1.0.0</sr:version>
                        <sr:versionNotes xml:lang="en">Release notes of version 1.0.0 can be checked at: https://code.europa.eu/oots/tdd/tdd_chapters/-/commits/1.1.0/OOTS-EDM/codelists/OOTS/LanguageCode-CodeList.gc</sr:versionNotes>
                        <sr:issued>2024-09-08</sr:issued>
                        <sr:modified>2024-09-08</sr:modified>
                        <sr:landingPage>https://code.europa.eu/oots/tdd/tdd_chapters/-/tree/master/OOTS-EDM</sr:landingPage>
                        <sr:distribution>
                            <sr:AssetDistribution>
                                <sr:downloadURL>https://code.europa.eu/oots/tdd/tdd_chapters/-/raw/1.1.0/OOTS-EDM/codelists/OOTS/LanguageCode-CodeList.gc</sr:downloadURL>
                                <sr:mediaType>XML</sr:mediaType>
                                <sr:title xml:lang="en">Genericode distribution of LanguageCode codelist</sr:title>
                            </sr:AssetDistribution>
                            <sr:AssetDistribution>
                                <sr:downloadURL>https://code.europa.eu/oots/tdd/tdd_chapters/-/raw/1.1.0/OOTS-EDM/sch/codelist-include/LANGUAGECODE-CODELIST_code.sch</sr:downloadURL>
                                <sr:mediaType>XML</sr:mediaType>
                                <sr:title xml:lang="en">Schematron distribution of LanguageCode code codelist</sr:title>
                            </sr:AssetDistribution>
                        </sr:distribution>
                        <sr:isCurrentVersionOf>
                            <sr:AssetRef>
                                <sr:identifier>https://sr.acc.oots.tech.ec.europa.eu/codelists/languagecode</sr:identifier>
                            </sr:AssetRef>
                        </sr:isCurrentVersionOf>
                        <sr:previousVersion>
                            <sr:VersionedAssetRef>
                                <sr:identifier>https://sr.acc.oots.tech.ec.europa.eu/codelists/languagecode</sr:identifier>
                                <sr:version>2020-04-17</sr:version>
                            </sr:VersionedAssetRef>
                        </sr:previousVersion>
                        <sr:isPartOf>
                            <sr:VersionedAssetRef>
                                <sr:identifier>https://sr.acc.oots.tech.ec.europa.eu/specifications/tdds</sr:identifier>
                                <sr:version>1.1.0</sr:version>
                            </sr:VersionedAssetRef>
                        </sr:isPartOf>
                        <sr:inCatalog>
                            <sr:AssetRepository>
                                <sr:title xml:lang="en">OOTS TDDs assets repository</sr:title>
                                <sr:description xml:lang="en">The catalog of the OOTS Technical Design Documents assets</sr:description>
                                <sr:landingPage>https://code.europa.eu/oots/tdd/tdd_chapters/-/tree/master/OOTS-EDM</sr:landingPage>
                                <sr:publisher>
                                    <sr:Agent>
                                        <sr:name xml:lang="en">Directorate-General for Digital Services</sr:name>
                                    </sr:Agent>
                                </sr:publisher>
                                <sr:inCatalog>
                                    <sr:AssetRepository>
                                        <sr:title xml:lang="en">OOTS Semantic Repository</sr:title>
                                        <sr:description xml:lang="en">The catalog of all the semantic assets used to enable the exchange of evidence in the Once-Only Technical System</sr:description>
                                        <sr:homepage>https://sr.oots.tech.ec.europa.eu/sr/home</sr:homepage>
                                        <sr:publisher>
                                            <sr:Agent>
                                                <sr:name xml:lang="en">Directorate-General for Digital Services</sr:name>
                                            </sr:Agent>
                                        </sr:publisher>
                                    </sr:AssetRepository>
                                </sr:inCatalog>
                            </sr:AssetRepository>
                        </sr:inCatalog>
                    </sr:Asset>
                </rim:SlotValue>
            </rim:Slot>
        </rim:RegistryObject>
        <rim:RegistryObject>
            <rim:Slot>
                <rim:SlotValue xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="rim:AnyValueType">
                    <sr:Asset>
                        <sr:identifier>https://sr.acc.oots.tech.ec.europa.eu/codelists/loa</sr:identifier>
                        <sr:versionedIdentifier>https://sr.acc.oots.tech.ec.europa.eu/codelists/loa/1.0.0</sr:versionedIdentifier>
                        <sr:title xml:lang="en">LoA</sr:title>
                        <sr:description xml:lang="en">eIDAS Levels Of Assurance</sr:description>
                        <sr:type>CODELIST</sr:type>
                        <sr:publisher>
                            <sr:Agent>
                                <sr:name xml:lang="en">Directorate-General for Digital Services</sr:name>
                            </sr:Agent>
                        </sr:publisher>
                        <sr:status>PUBLISHED</sr:status>
                        <sr:language>ENG</sr:language>
                        <sr:version>1.0.0</sr:version>
                        <sr:versionNotes xml:lang="en">Release notes of version 1.0.0 can be checked at: https://code.europa.eu/oots/tdd/tdd_chapters/-/commits/1.1.0/OOTS-EDM/codelists/OOTS/LevelsOfAssurance-CodeList.gc</sr:versionNotes>
                        <sr:issued>2024-09-08</sr:issued>
                        <sr:modified>2024-09-08</sr:modified>
                        <sr:landingPage>https://code.europa.eu/oots/tdd/tdd_chapters/-/tree/master/OOTS-EDM</sr:landingPage>
                        <sr:distribution>
                            <sr:AssetDistribution>
                                <sr:downloadURL>https://code.europa.eu/oots/tdd/tdd_chapters/-/raw/1.1.0/OOTS-EDM/codelists/OOTS/LevelsOfAssurance-CodeList.gc</sr:downloadURL>
                                <sr:mediaType>XML</sr:mediaType>
                                <sr:title xml:lang="en">Genericode distribution of LoA codelist</sr:title>
                            </sr:AssetDistribution>
                            <sr:AssetDistribution>
                                <sr:downloadURL>https://code.europa.eu/oots/tdd/tdd_chapters/-/raw/1.1.0/OOTS-EDM/sch/codelist-include/LEVELSOFASSURANCE-CODELIST_code.sch</sr:downloadURL>
                                <sr:mediaType>XML</sr:mediaType>
                                <sr:title xml:lang="en">Schematron distribution of LoA code codelist</sr:title>
                            </sr:AssetDistribution>
                        </sr:distribution>
                        <sr:isCurrentVersionOf>
                            <sr:AssetRef>
                                <sr:identifier>https://sr.acc.oots.tech.ec.europa.eu/codelists/loa</sr:identifier>
                            </sr:AssetRef>
                        </sr:isCurrentVersionOf>
                        <sr:previousVersion>
                            <sr:VersionedAssetRef>
                                <sr:identifier>https://sr.acc.oots.tech.ec.europa.eu/codelists/loa</sr:identifier>
                                <sr:version>2023-11</sr:version>
                            </sr:VersionedAssetRef>
                        </sr:previousVersion>
                        <sr:isPartOf>
                            <sr:VersionedAssetRef>
                                <sr:identifier>https://sr.acc.oots.tech.ec.europa.eu/specifications/tdds</sr:identifier>
                                <sr:version>1.1.0</sr:version>
                            </sr:VersionedAssetRef>
                        </sr:isPartOf>
                        <sr:inCatalog>
                            <sr:AssetRepository>
                                <sr:title xml:lang="en">OOTS TDDs assets repository</sr:title>
                                <sr:description xml:lang="en">The catalog of the OOTS Technical Design Documents assets</sr:description>
                                <sr:landingPage>https://code.europa.eu/oots/tdd/tdd_chapters/-/tree/master/OOTS-EDM</sr:landingPage>
                                <sr:publisher>
                                    <sr:Agent>
                                        <sr:name xml:lang="en">Directorate-General for Digital Services</sr:name>
                                    </sr:Agent>
                                </sr:publisher>
                                <sr:inCatalog>
                                    <sr:AssetRepository>
                                        <sr:title xml:lang="en">OOTS Semantic Repository</sr:title>
                                        <sr:description xml:lang="en">The catalog of all the semantic assets used to enable the exchange of evidence in the Once-Only Technical System</sr:description>
                                        <sr:homepage>https://sr.oots.tech.ec.europa.eu/sr/home</sr:homepage>
                                        <sr:publisher>
                                            <sr:Agent>
                                                <sr:name xml:lang="en">Directorate-General for Digital Services</sr:name>
                                            </sr:Agent>
                                        </sr:publisher>
                                    </sr:AssetRepository>
                                </sr:inCatalog>
                            </sr:AssetRepository>
                        </sr:inCatalog>
                    </sr:Asset>
                </rim:SlotValue>
            </rim:Slot>
        </rim:RegistryObject>
        <rim:RegistryObject>
            <rim:Slot>
                <rim:SlotValue xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="rim:AnyValueType">
                    <sr:Asset>
                        <sr:identifier>https://sr.acc.oots.tech.ec.europa.eu/codelists/nuts</sr:identifier>
                        <sr:versionedIdentifier>https://sr.acc.oots.tech.ec.europa.eu/codelists/nuts/1.0.0</sr:versionedIdentifier>
                        <sr:title xml:lang="en">NUTS</sr:title>
                        <sr:description xml:lang="en">Nomenclature of Territorial Units for Statistics 2024</sr:description>
                        <sr:type>CODELIST</sr:type>
                        <sr:publisher>
                            <sr:Agent>
                                <sr:name xml:lang="en">Directorate-General for Digital Services</sr:name>
                            </sr:Agent>
                        </sr:publisher>
                        <sr:status>PUBLISHED</sr:status>
                        <sr:language>ENG</sr:language>
                        <sr:version>1.0.0</sr:version>
                        <sr:versionNotes xml:lang="en">Release notes of version 1.0.0 can be checked at: https://code.europa.eu/oots/tdd/tdd_chapters/-/commits/1.1.0/OOTS-EDM/codelists/OOTS/NUTS2024-CodeList.gc</sr:versionNotes>
                        <sr:issued>2024-09-08</sr:issued>
                        <sr:modified>2024-09-08</sr:modified>
                        <sr:landingPage>https://code.europa.eu/oots/tdd/tdd_chapters/-/tree/master/OOTS-EDM</sr:landingPage>
                        <sr:distribution>
                            <sr:AssetDistribution>
                                <sr:downloadURL>https://code.europa.eu/oots/tdd/tdd_chapters/-/raw/1.1.0/OOTS-EDM/codelists/OOTS/NUTS2024-CodeList.gc</sr:downloadURL>
                                <sr:mediaType>XML</sr:mediaType>
                                <sr:title xml:lang="en">Genericode distribution of NUTS codelist</sr:title>
                            </sr:AssetDistribution>
                            <sr:AssetDistribution>
                                <sr:downloadURL>https://code.europa.eu/oots/tdd/tdd_chapters/-/raw/1.1.0/OOTS-EDM/sch/codelist-include/NUTS2024-CODELIST_code.sch</sr:downloadURL>
                                <sr:mediaType>XML</sr:mediaType>
                                <sr:title xml:lang="en">Schematron distribution of NUTS code codelist</sr:title>
                            </sr:AssetDistribution>
                        </sr:distribution>
                        <sr:isCurrentVersionOf>
                            <sr:AssetRef>
                                <sr:identifier>https://sr.acc.oots.tech.ec.europa.eu/codelists/nuts</sr:identifier>
                            </sr:AssetRef>
                        </sr:isCurrentVersionOf>
                        <sr:previousVersion>
                            <sr:VersionedAssetRef>
                                <sr:identifier>https://sr.acc.oots.tech.ec.europa.eu/codelists/nuts</sr:identifier>
                                <sr:version>2023-11</sr:version>
                            </sr:VersionedAssetRef>
                        </sr:previousVersion>
                        <sr:isPartOf>
                            <sr:VersionedAssetRef>
                                <sr:identifier>https://sr.acc.oots.tech.ec.europa.eu/specifications/tdds</sr:identifier>
                                <sr:version>1.1.0</sr:version>
                            </sr:VersionedAssetRef>
                        </sr:isPartOf>
                        <sr:inCatalog>
                            <sr:AssetRepository>
                                <sr:title xml:lang="en">OOTS TDDs assets repository</sr:title>
                                <sr:description xml:lang="en">The catalog of the OOTS Technical Design Documents assets</sr:description>
                                <sr:landingPage>https://code.europa.eu/oots/tdd/tdd_chapters/-/tree/master/OOTS-EDM</sr:landingPage>
                                <sr:publisher>
                                    <sr:Agent>
                                        <sr:name xml:lang="en">Directorate-General for Digital Services</sr:name>
                                    </sr:Agent>
                                </sr:publisher>
                                <sr:inCatalog>
                                    <sr:AssetRepository>
                                        <sr:title xml:lang="en">OOTS Semantic Repository</sr:title>
                                        <sr:description xml:lang="en">The catalog of all the semantic assets used to enable the exchange of evidence in the Once-Only Technical System</sr:description>
                                        <sr:homepage>https://sr.oots.tech.ec.europa.eu/sr/home</sr:homepage>
                                        <sr:publisher>
                                            <sr:Agent>
                                                <sr:name xml:lang="en">Directorate-General for Digital Services</sr:name>
                                            </sr:Agent>
                                        </sr:publisher>
                                    </sr:AssetRepository>
                                </sr:inCatalog>
                            </sr:AssetRepository>
                        </sr:inCatalog>
                    </sr:Asset>
                </rim:SlotValue>
            </rim:Slot>
        </rim:RegistryObject>
        <rim:RegistryObject>
            <rim:Slot>
                <rim:SlotValue xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="rim:AnyValueType">
                    <sr:Asset>
                        <sr:identifier>https://sr.acc.oots.tech.ec.europa.eu/codelists/ootsmediatypes</sr:identifier>
                        <sr:versionedIdentifier>https://sr.acc.oots.tech.ec.europa.eu/codelists/ootsmediatypes/1.0.0</sr:versionedIdentifier>
                        <sr:title xml:lang="en">OOTSMediaTypes</sr:title>
                        <sr:description xml:lang="en">OOTS Binary Object Mime Code</sr:description>
                        <sr:type>CODELIST</sr:type>
                        <sr:publisher>
                            <sr:Agent>
                                <sr:name xml:lang="en">Directorate-General for Digital Services</sr:name>
                            </sr:Agent>
                        </sr:publisher>
                        <sr:status>PUBLISHED</sr:status>
                        <sr:language>ENG</sr:language>
                        <sr:version>1.0.0</sr:version>
                        <sr:versionNotes xml:lang="en">Release notes of version 1.0.0 can be checked at: https://code.europa.eu/oots/tdd/tdd_chapters/-/commits/1.1.0/OOTS-EDM/codelists/OOTS/OOTSMediaTypes-CodeList.gc</sr:versionNotes>
                        <sr:issued>2024-09-08</sr:issued>
                        <sr:modified>2024-09-08</sr:modified>
                        <sr:landingPage>https://code.europa.eu/oots/tdd/tdd_chapters/-/tree/master/OOTS-EDM</sr:landingPage>
                        <sr:distribution>
                            <sr:AssetDistribution>
                                <sr:downloadURL>https://code.europa.eu/oots/tdd/tdd_chapters/-/raw/1.1.0/OOTS-EDM/codelists/OOTS/OOTSMediaTypes-CodeList.gc</sr:downloadURL>
                                <sr:mediaType>XML</sr:mediaType>
                                <sr:title xml:lang="en">Genericode distribution of OOTSMediaTypes codelist</sr:title>
                            </sr:AssetDistribution>
                            <sr:AssetDistribution>
                                <sr:downloadURL>https://code.europa.eu/oots/tdd/tdd_chapters/-/raw/1.1.0/OOTS-EDM/sch/codelist-include/OOTSMEDIATYPES-CODELIST_code.sch</sr:downloadURL>
                                <sr:mediaType>XML</sr:mediaType>
                                <sr:title xml:lang="en">Schematron distribution of OOTSMediaTypes code codelist</sr:title>
                            </sr:AssetDistribution>
                        </sr:distribution>
                        <sr:isCurrentVersionOf>
                            <sr:AssetRef>
                                <sr:identifier>https://sr.acc.oots.tech.ec.europa.eu/codelists/ootsmediatypes</sr:identifier>
                            </sr:AssetRef>
                        </sr:isCurrentVersionOf>
                        <sr:previousVersion>
                            <sr:VersionedAssetRef>
                                <sr:identifier>https://sr.acc.oots.tech.ec.europa.eu/codelists/ootsmediatypes</sr:identifier>
                                <sr:version>2023-11</sr:version>
                            </sr:VersionedAssetRef>
                        </sr:previousVersion>
                        <sr:isPartOf>
                            <sr:VersionedAssetRef>
                                <sr:identifier>https://sr.acc.oots.tech.ec.europa.eu/specifications/tdds</sr:identifier>
                                <sr:version>1.1.0</sr:version>
                            </sr:VersionedAssetRef>
                        </sr:isPartOf>
                        <sr:inCatalog>
                            <sr:AssetRepository>
                                <sr:title xml:lang="en">OOTS TDDs assets repository</sr:title>
                                <sr:description xml:lang="en">The catalog of the OOTS Technical Design Documents assets</sr:description>
                                <sr:landingPage>https://code.europa.eu/oots/tdd/tdd_chapters/-/tree/master/OOTS-EDM</sr:landingPage>
                                <sr:publisher>
                                    <sr:Agent>
                                        <sr:name xml:lang="en">Directorate-General for Digital Services</sr:name>
                                    </sr:Agent>
                                </sr:publisher>
                                <sr:inCatalog>
                                    <sr:AssetRepository>
                                        <sr:title xml:lang="en">OOTS Semantic Repository</sr:title>
                                        <sr:description xml:lang="en">The catalog of all the semantic assets used to enable the exchange of evidence in the Once-Only Technical System</sr:description>
                                        <sr:homepage>https://sr.oots.tech.ec.europa.eu/sr/home</sr:homepage>
                                        <sr:publisher>
                                            <sr:Agent>
                                                <sr:name xml:lang="en">Directorate-General for Digital Services</sr:name>
                                            </sr:Agent>
                                        </sr:publisher>
                                    </sr:AssetRepository>
                                </sr:inCatalog>
                            </sr:AssetRepository>
                        </sr:inCatalog>
                    </sr:Asset>
                </rim:SlotValue>
            </rim:Slot>
        </rim:RegistryObject>
        <rim:RegistryObject>
            <rim:Slot>
                <rim:SlotValue xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="rim:AnyValueType">
                    <sr:Asset>
                        <sr:identifier>https://sr.acc.oots.tech.ec.europa.eu/codelists/procedures</sr:identifier>
                        <sr:versionedIdentifier>https://sr.acc.oots.tech.ec.europa.eu/codelists/procedures/1.0.0</sr:versionedIdentifier>
                        <sr:title xml:lang="en">Procedures</sr:title>
                        <sr:description xml:lang="en">Procedures</sr:description>
                        <sr:type>CODELIST</sr:type>
                        <sr:publisher>
                            <sr:Agent>
                                <sr:name xml:lang="en">Directorate-General for Digital Services</sr:name>
                            </sr:Agent>
                        </sr:publisher>
                        <sr:status>PUBLISHED</sr:status>
                        <sr:language>ENG</sr:language>
                        <sr:version>1.0.0</sr:version>
                        <sr:versionNotes xml:lang="en">Release notes of version 1.0.0 can be checked at: https://code.europa.eu/oots/tdd/tdd_chapters/-/commits/1.1.0/OOTS-EDM/codelists/OOTS/Procedures-CodeList.gc</sr:versionNotes>
                        <sr:issued>2024-09-08</sr:issued>
                        <sr:modified>2024-09-08</sr:modified>
                        <sr:landingPage>https://code.europa.eu/oots/tdd/tdd_chapters/-/tree/master/OOTS-EDM</sr:landingPage>
                        <sr:distribution>
                            <sr:AssetDistribution>
                                <sr:downloadURL>https://code.europa.eu/oots/tdd/tdd_chapters/-/raw/1.1.0/OOTS-EDM/codelists/OOTS/Procedures-CodeList.gc</sr:downloadURL>
                                <sr:mediaType>XML</sr:mediaType>
                                <sr:title xml:lang="en">Genericode distribution of Procedures codelist</sr:title>
                            </sr:AssetDistribution>
                            <sr:AssetDistribution>
                                <sr:downloadURL>https://code.europa.eu/oots/tdd/tdd_chapters/-/raw/1.1.0/OOTS-EDM/sch/codelist-include/PROCEDURES-CODELIST_code.sch</sr:downloadURL>
                                <sr:mediaType>XML</sr:mediaType>
                                <sr:title xml:lang="en">Schematron distribution of Procedures code codelist</sr:title>
                            </sr:AssetDistribution>
                        </sr:distribution>
                        <sr:isCurrentVersionOf>
                            <sr:AssetRef>
                                <sr:identifier>https://sr.acc.oots.tech.ec.europa.eu/codelists/procedures</sr:identifier>
                            </sr:AssetRef>
                        </sr:isCurrentVersionOf>
                        <sr:previousVersion>
                            <sr:VersionedAssetRef>
                                <sr:identifier>https://sr.acc.oots.tech.ec.europa.eu/codelists/procedures</sr:identifier>
                                <sr:version>2024-02</sr:version>
                            </sr:VersionedAssetRef>
                        </sr:previousVersion>
                        <sr:isPartOf>
                            <sr:VersionedAssetRef>
                                <sr:identifier>https://sr.acc.oots.tech.ec.europa.eu/specifications/tdds</sr:identifier>
                                <sr:version>1.1.0</sr:version>
                            </sr:VersionedAssetRef>
                        </sr:isPartOf>
                        <sr:inCatalog>
                            <sr:AssetRepository>
                                <sr:title xml:lang="en">OOTS TDDs assets repository</sr:title>
                                <sr:description xml:lang="en">The catalog of the OOTS Technical Design Documents assets</sr:description>
                                <sr:landingPage>https://code.europa.eu/oots/tdd/tdd_chapters/-/tree/master/OOTS-EDM</sr:landingPage>
                                <sr:publisher>
                                    <sr:Agent>
                                        <sr:name xml:lang="en">Directorate-General for Digital Services</sr:name>
                                    </sr:Agent>
                                </sr:publisher>
                                <sr:inCatalog>
                                    <sr:AssetRepository>
                                        <sr:title xml:lang="en">OOTS Semantic Repository</sr:title>
                                        <sr:description xml:lang="en">The catalog of all the semantic assets used to enable the exchange of evidence in the Once-Only Technical System</sr:description>
                                        <sr:homepage>https://sr.oots.tech.ec.europa.eu/sr/home</sr:homepage>
                                        <sr:publisher>
                                            <sr:Agent>
                                                <sr:name xml:lang="en">Directorate-General for Digital Services</sr:name>
                                            </sr:Agent>
                                        </sr:publisher>
                                    </sr:AssetRepository>
                                </sr:inCatalog>
                            </sr:AssetRepository>
                        </sr:inCatalog>
                    </sr:Asset>
                </rim:SlotValue>
            </rim:Slot>
        </rim:RegistryObject>
        <rim:RegistryObject>
            <rim:Slot>
                <rim:SlotValue xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="rim:AnyValueType">
                    <sr:Asset>
                        <sr:identifier>https://sr.acc.oots.tech.ec.europa.eu/codelists/srsearcherrorcodes</sr:identifier>
                        <sr:versionedIdentifier>https://sr.acc.oots.tech.ec.europa.eu/codelists/srsearcherrorcodes/1.0.1</sr:versionedIdentifier>
                        <sr:title xml:lang="en">SRSearchErrorCodes</sr:title>
                        <sr:description xml:lang="en">Semantic repository Search API Error Response Codes</sr:description>
                        <sr:type>CODELIST</sr:type>
                        <sr:publisher>
                            <sr:Agent>
                                <sr:name xml:lang="en">Directorate-General for Digital Services</sr:name>
                            </sr:Agent>
                        </sr:publisher>
                        <sr:status>PUBLISHED</sr:status>
                        <sr:language>ENG</sr:language>
                        <sr:version>1.0.1</sr:version>
                        <sr:versionNotes xml:lang="en">Release notes of version 1.0.1 can be checked at: https://code.europa.eu/oots/tdd/tdd_chapters/-/commits/1.1.0/OOTS-EDM/codelists/OOTS/SRSearchErrorCodes-CodeList.gc</sr:versionNotes>
                        <sr:issued>2024-09-08</sr:issued>
                        <sr:modified>2024-09-08</sr:modified>
                        <sr:landingPage>https://code.europa.eu/oots/tdd/tdd_chapters/-/tree/master/OOTS-EDM</sr:landingPage>
                        <sr:distribution>
                            <sr:AssetDistribution>
                                <sr:downloadURL>https://code.europa.eu/oots/tdd/tdd_chapters/-/raw/1.1.0/OOTS-EDM/sch/codelist-include/SRSEARCHERRORCODES-CODELIST_name-Type.sch</sr:downloadURL>
                                <sr:mediaType>XML</sr:mediaType>
                                <sr:title xml:lang="en">Schematron distribution of SRSearchErrorCodes name-Type codelist</sr:title>
                            </sr:AssetDistribution>
                            <sr:AssetDistribution>
                                <sr:downloadURL>https://code.europa.eu/oots/tdd/tdd_chapters/-/raw/1.1.0/OOTS-EDM/codelists/OOTS/SRSearchErrorCodes-CodeList.gc</sr:downloadURL>
                                <sr:mediaType>XML</sr:mediaType>
                                <sr:title xml:lang="en">Genericode distribution of SRSearchErrorCodes codelist</sr:title>
                            </sr:AssetDistribution>
                            <sr:AssetDistribution>
                                <sr:downloadURL>https://code.europa.eu/oots/tdd/tdd_chapters/-/raw/1.1.0/OOTS-EDM/sch/codelist-include/SRSEARCHERRORCODES-CODELIST_code.sch</sr:downloadURL>
                                <sr:mediaType>XML</sr:mediaType>
                                <sr:title xml:lang="en">Schematron distribution of SRSearchErrorCodes code codelist</sr:title>
                            </sr:AssetDistribution>
                        </sr:distribution>
                        <sr:isCurrentVersionOf>
                            <sr:AssetRef>
                                <sr:identifier>https://sr.acc.oots.tech.ec.europa.eu/codelists/srsearcherrorcodes</sr:identifier>
                            </sr:AssetRef>
                        </sr:isCurrentVersionOf>
                        <sr:previousVersion>
                            <sr:VersionedAssetRef>
                                <sr:identifier>https://sr.acc.oots.tech.ec.europa.eu/codelists/srsearcherrorcodes</sr:identifier>
                                <sr:version>1.0.0</sr:version>
                            </sr:VersionedAssetRef>
                        </sr:previousVersion>
                        <sr:isPartOf>
                            <sr:VersionedAssetRef>
                                <sr:identifier>https://sr.acc.oots.tech.ec.europa.eu/specifications/tdds</sr:identifier>
                                <sr:version>1.1.0</sr:version>
                            </sr:VersionedAssetRef>
                        </sr:isPartOf>
                        <sr:inCatalog>
                            <sr:AssetRepository>
                                <sr:title xml:lang="en">OOTS TDDs assets repository</sr:title>
                                <sr:description xml:lang="en">The catalog of the OOTS Technical Design Documents assets</sr:description>
                                <sr:landingPage>https://code.europa.eu/oots/tdd/tdd_chapters/-/tree/master/OOTS-EDM</sr:landingPage>
                                <sr:publisher>
                                    <sr:Agent>
                                        <sr:name xml:lang="en">Directorate-General for Digital Services</sr:name>
                                    </sr:Agent>
                                </sr:publisher>
                                <sr:inCatalog>
                                    <sr:AssetRepository>
                                        <sr:title xml:lang="en">OOTS Semantic Repository</sr:title>
                                        <sr:description xml:lang="en">The catalog of all the semantic assets used to enable the exchange of evidence in the Once-Only Technical System</sr:description>
                                        <sr:homepage>https://sr.oots.tech.ec.europa.eu/sr/home</sr:homepage>
                                        <sr:publisher>
                                            <sr:Agent>
                                                <sr:name xml:lang="en">Directorate-General for Digital Services</sr:name>
                                            </sr:Agent>
                                        </sr:publisher>
                                    </sr:AssetRepository>
                                </sr:inCatalog>
                            </sr:AssetRepository>
                        </sr:inCatalog>
                    </sr:Asset>
                </rim:SlotValue>
            </rim:Slot>
        </rim:RegistryObject>
        <rim:RegistryObject>
            <rim:Slot>
                <rim:SlotValue xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="rim:AnyValueType">
                    <sr:Asset>
                        <sr:identifier>https://sr.acc.oots.tech.ec.europa.eu/codelists/uidsuppressioncountry</sr:identifier>
                        <sr:versionedIdentifier>https://sr.acc.oots.tech.ec.europa.eu/codelists/uidsuppressioncountry/1.0.0</sr:versionedIdentifier>
                        <sr:title xml:lang="en">UIDSuppressionCountry</sr:title>
                        <sr:description xml:lang="en">eIDAS Unique Person Identifier Suppression Country</sr:description>
                        <sr:type>CODELIST</sr:type>
                        <sr:publisher>
                            <sr:Agent>
                                <sr:name xml:lang="en">Directorate-General for Digital Services</sr:name>
                            </sr:Agent>
                        </sr:publisher>
                        <sr:status>PUBLISHED</sr:status>
                        <sr:language>ENG</sr:language>
                        <sr:version>1.0.0</sr:version>
                        <sr:versionNotes xml:lang="en">Release notes of version 1.0.0 can be checked at: https://code.europa.eu/oots/tdd/tdd_chapters/-/commits/1.1.0/OOTS-EDM/codelists/OOTS/UIDSuppressionCountry-CodeList.gc</sr:versionNotes>
                        <sr:issued>2024-09-08</sr:issued>
                        <sr:modified>2024-09-08</sr:modified>
                        <sr:landingPage>https://code.europa.eu/oots/tdd/tdd_chapters/-/tree/master/OOTS-EDM</sr:landingPage>
                        <sr:distribution>
                            <sr:AssetDistribution>
                                <sr:downloadURL>https://code.europa.eu/oots/tdd/tdd_chapters/-/raw/1.1.0/OOTS-EDM/codelists/OOTS/UIDSuppressionCountry-CodeList.gc</sr:downloadURL>
                                <sr:mediaType>XML</sr:mediaType>
                                <sr:title xml:lang="en">Genericode distribution of UIDSuppressionCountry codelist</sr:title>
                            </sr:AssetDistribution>
                            <sr:AssetDistribution>
                                <sr:downloadURL>https://code.europa.eu/oots/tdd/tdd_chapters/-/raw/1.1.0/OOTS-EDM/sch/codelist-include/UIDSUPPRESSIONCOUNTRY-CODELIST_code.sch</sr:downloadURL>
                                <sr:mediaType>XML</sr:mediaType>
                                <sr:title xml:lang="en">Schematron distribution of UIDSuppressionCountry code codelist</sr:title>
                            </sr:AssetDistribution>
                        </sr:distribution>
                        <sr:isCurrentVersionOf>
                            <sr:AssetRef>
                                <sr:identifier>https://sr.acc.oots.tech.ec.europa.eu/codelists/uidsuppressioncountry</sr:identifier>
                            </sr:AssetRef>
                        </sr:isCurrentVersionOf>
                        <sr:isPartOf>
                            <sr:VersionedAssetRef>
                                <sr:identifier>https://sr.acc.oots.tech.ec.europa.eu/specifications/tdds</sr:identifier>
                                <sr:version>1.1.0</sr:version>
                            </sr:VersionedAssetRef>
                        </sr:isPartOf>
                        <sr:inCatalog>
                            <sr:AssetRepository>
                                <sr:title xml:lang="en">OOTS TDDs assets repository</sr:title>
                                <sr:description xml:lang="en">The catalog of the OOTS Technical Design Documents assets</sr:description>
                                <sr:landingPage>https://code.europa.eu/oots/tdd/tdd_chapters/-/tree/master/OOTS-EDM</sr:landingPage>
                                <sr:publisher>
                                    <sr:Agent>
                                        <sr:name xml:lang="en">Directorate-General for Digital Services</sr:name>
                                    </sr:Agent>
                                </sr:publisher>
                                <sr:inCatalog>
                                    <sr:AssetRepository>
                                        <sr:title xml:lang="en">OOTS Semantic Repository</sr:title>
                                        <sr:description xml:lang="en">The catalog of all the semantic assets used to enable the exchange of evidence in the Once-Only Technical System</sr:description>
                                        <sr:homepage>https://sr.oots.tech.ec.europa.eu/sr/home</sr:homepage>
                                        <sr:publisher>
                                            <sr:Agent>
                                                <sr:name xml:lang="en">Directorate-General for Digital Services</sr:name>
                                            </sr:Agent>
                                        </sr:publisher>
                                    </sr:AssetRepository>
                                </sr:inCatalog>
                            </sr:AssetRepository>
                        </sr:inCatalog>
                    </sr:Asset>
                </rim:SlotValue>
            </rim:Slot>
        </rim:RegistryObject>
    </rim:RegistryObjectList>
</query:QueryResponse>
```
</details>

### Error response

When something unexpected occurs during the execution of the Get Metadata method, an error response is returned in XML format with the following fields:

- **code**: The internal error code of the Semantic Repository (with the following format SR:ERR:XXXX)
- **message**: The title of the error message
- **detail**: The detailed description of the error
- **severity**: Warning, Error, Info

#### Example

An example of an Error Response of the Semantic Repository search can be found in the following XML snippet:

<details>
<summary>Example of an Error Response</summary>

```xml
<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<errorDto>
    <code>SR:ERR:0009</code>
    <title>Bad request error</title>
    <description>Incorrect provided value for requested parameters. The Response format isn't valid.</description>
    <timestamp>2024-11-20T15:29:08</timestamp>
</errorDto>
```

</details>

#### Error Response Codes

The following table provides the list of possible error codes for this method. The `Description` may contain further information for specific errors:

**Error Response Codes**

| HTTP Status  | Code        | Title                                    | Detail                                                                                |
| ------------ |-------------|------------------------------------------| --------------------------------------------------------------------------------------|
| 406          | SR:ERR:0009 | Incorrect value for requested parameter  | Incorrect provided value for requested parameter
| 406          | SR:ERR:0010 | Incorrect parameter                      | The query parameters do not follow the query specification
| 406          | SR:ERR:0011 | Incorrect date range                     | The end date of the range must be always equal or greater than the start date
| 500          | SR:ERR:0001 | Internal server error                    | An error occurred during search execution.                 
