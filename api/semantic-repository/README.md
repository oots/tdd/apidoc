﻿---
hide_table_of_contents: true
---

# Semantic Repository
>**Version 1.1.0** | **Mandatory** | **Assets Versioned in Subsections**

The Semantic Repository (SR) is one of the Common Services of the OOTS high-level architecture. Its mission is to store and provide access to the semantic assets used to support the exchange of documents and data through the OOTS. These assets refer to agreed-upon semantic specifications that are linked to the Evidence Broker and the Data Service Directory Common Services and are composed of definitions of names, data types and data elements associated with specific evidence types to ensure mutual understanding and language-independent interpretation for evidence providers, evidence requesters and users, when exchanging evidence through the OOTS.

Article 7.7 of the  [Implementing Act](https://eur-lex.europa.eu/legal-content/EN/TXT/HTML/?uri=PI_COM:C(2022)5628)  stipulates that "the Commission shall publicly make available the Semantic Repository on a dedicated Commission website." This allows users to easily access the metadata of a particular asset, including the asset itself, the provider of the asset, and other metadata fields outlined in Article 13.1 (Article 7.1).

Semantic assets and their associated metadata are accessible via HTTP protocols and can be identified using URIs (Uniform Resource Identifiers) as defined in RFC 3986. The compositional structure of URIs allows for the creation of additional URIs and the breakdown of constituent parts as required. Furthermore, URIs are used as canonical identifiers of the asset.

Each semantic asset saved within the Semantic Repository is assigned with a Unique Resource Identifier (URI) upon creation. An asset URI provides an endpoint for obtaining the data in a machine-readable format.

The current version of the Semantic Repository grants access to metadata via HTTP GET method (using the asset URI or versioned identifier wich is formed by the pattern URI/version) for 9 asset types, namely **semantic data specifications, schemas, technical specifications, codelists, schematrons, examples, procedures, requirements, and evidence types.**

Apart from the asset metadata, the SR API also offers the option to redirect to the asset distribution access URL (the page where the asset is stored).
