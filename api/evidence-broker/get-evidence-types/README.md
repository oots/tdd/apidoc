---
sidebar_position: 1
---

# Get Evidence Types
>**Version 1.1.0** | **Mandatory** | **Assets Versioned in Subsections**

## Request
>**Version 1.1.0** | **Mandatory**

:::info
**Purpose of the Get Evidence Types for Requirement Query:**
The second query of the Evidence Broker (Get Evidence Types for Requirement) determines the context of the Evidence Provider in order to identify the Evidence Types in the evidence providing Member State. The query parameters should therefore reflect the country and jurisdiction of the Evidence Provider.

The query is based on a requirement that is known to the Procedure Portal or that is determined via the Evidence Broker's first query (Get List of Requirements Query). It is important to note that all query results reflect the view of the Evidence Provider, including the information associated with the requirement (reference framework, procedure, jurisdiction).
:::

The URL pattern for parameterised query invocation is defined as follows in the OASIS RegRep REST binding:

```
«server base url»/rest/search?queryId={the query id}(&{param-name}={param-value})*
```

The query interface consists of a simple predefined parameterised query detailed below. In addition, the RegRep standard defines a set of canonical queries and query parameters that can be used. As the Data Service Directory is not a complete implementation of a RegRep server, it is NOT REQUIRED to support these canonical queries and query parameters. Clients, therefore, SHOULD only use the queries and query parameters specified by this specification. When the canonical queries or parameters are used, the Common Service implementation MAY return an error.

### Parameter Details

This query returns the list of evidence types that prove a specific requirement. It must contain the Identifier of the requirement, that can be extracted using the ["Get List of Requirements"](../get-requirements/README.md) query if not known. The supported parameters and query filters, are listed in the following table. The optional parameter values are retrieved from the [codelists](https://code.europa.eu/oots/tdd/tdd_chapters/-/tree/master/OOTS-EDM/codelists) published in the semantic repository:

| Parameter                 | Requirement | Description                                                                                                                                                                                                                                                                                                                                                                             |
| ------------------------- | ----------- |-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| **queryId**               | MANDATORY   | **urn:fdc:oots:eb:ebxml-regrep:queries:evidence-types-by-requirement-and-jurisdiction**                                                                                                                                                                                                                                                                                                 |
| **requirement-id**        | MANDATORY   | The id of the requirement we request the evidence types for. The URL must be encoded according to [RFC3986](https://www.rfc-editor.org/rfc/rfc3986) to ensure the URL is valid. The requirement is either known to the Procedure Portal or is determined via the Evidence Broker's first query (Get List of Requirements Query).                                                        |
| **country-code**          | OPTIONAL    | The two-letter [ISO 3166-1 alpha-2 country code (EEA_Country subset)](https://code.europa.eu/oots/tdd/tdd_chapters/-/blob/master/OOTS-EDM/codelists/OOTS/EEA_Country-CodeList.gc) that is expected to provide the evidence. Thus, the country code should relate to the country of the Evidence Provider to determine the Evidence Type in the country where the Evidence is requested. |
| **jurisdiction-admin-l2** | OPTIONAL    | The level two administration level code for the jurisdiction of the evidence type, expressed using [NUTS code](https://code.europa.eu/oots/tdd/tdd_chapters/-/blob/master/OOTS-EDM/codelists/OOTS/NUTS2024-CodeList.gc?ref_type=heads). It MUST be combined with `country-code`                                                                                                         |
| **jurisdiction-admin-l3** | OPTIONAL    | The level three administration level code for the jurisdiction of the evidence type, expressed using [LAU code](https://code.europa.eu/oots/tdd/tdd_chapters/-/blob/master/OOTS-EDM/codelists/OOTS/LAU2022-CodeList.gc). It MUST be combined with `country-code`                                                                                                                        |

### Example Flows

The Evidence Requester needs to fetch the evidence types that are associated to a requirement and country. To do this, it executes the following HTTP REST Call to the EB with the included values _https://sr.oots.tech.ec.europa.eu/requirements/315cfd75-6605-49c4-b0fe-799833b41099_ for **requirement-id** and _DE_ for **country-code**. The parameter values have to be looked up in the [codelists](https://code.europa.eu/oots/tdd/tdd_chapters/-/tree/master/OOTS-EDM/codelists) published in the semantic repository. The URL value of the requirement-ID must be encoded according to RFC3986 to ensure the URL is valid.

> _«server base url»_/rest/search?queryId=urn:fdc:oots:eb:ebxml-regrep:queries:evidence-types-by-requirement-and-jurisdiction&**requirement-id**=https%3A%2F%2Fsr.oots.tech.ec.europa.eu%2Frequirements%2F315cfd75-6605-49c4-b0fe-799833b41099&**country-code**=DE

The EB receives the request and checks whether for **requirement-id** = _https://sr.oots.tech.ec.europa.eu/requirements/315cfd75-6605-49c4-b0fe-799833b41099_ and **country-code** = _DE_ exist. Two potential responses can be given thus the following flows must distinguished:

#### Positive Flow

An `EvidenceType` exists. The EB will provide a [Query Response](#query-response)

The following figure illustrates the positive flow resulting in a `query:QueryResponse`

![Positive Flow of the EB](./eb_getfrq.png)

#### Negative Flow

The request to the EB cannot be responded and an exception response is returned. The EB will provide a [Error Response](#error-response).

The following figure illustrates the negative flow resulting in a `query:Exception`

![Negative Flow of the EB](./eb_getfrq_exception.png)

## Response

The Query Response of the EB for Evidence Types that prove a specific requirement returns a RegRep QueryResponse document which MUST either contain an `Exception` or `RegistryObjectList` element with zero or more `RegistryObject`s. Each `RegistryObject` in the result MUST include one `Slot` element with a `SlotValue` of type `rim:AnyValueType` and a single `Requirement` child element, following the OOTS Application Profile of the EB. The OOTS application profile of the EB describes how the [SDG-Generic-Metadata Profile (SDG-syntax)](https://code.europa.eu/oots/tdd/tdd_chapters/-/tree/master/OOTS-EDM/xsd/sdg) is profiled in [ebRIM](http://docs.oasis-open.org/regrep/regrep-core/v4.0/os/regrep-core-rim-v4.0-os.html) in order to compose a valid QueryResponse. It therefore contains a mapping to the underlying [SDG-syntax](https://code.europa.eu/oots/tdd/tdd_chapters/-/blob/master/OOTS-EDM/xsd/sdg/SDG-GenericMetadataProfile-v1.0.0.xsd) elements and necessary parameters to compose a QueryResponse. The namespace of the [SDG-syntax](https://code.europa.eu/oots/tdd/tdd_chapters/-/blob/master/OOTS-EDM/xsd/sdg/SDG-GenericMetadataProfile-v1.0.0.xsd) is http://data.europa.eu/p4s.

### Query Response
>**Version 1.0.0** | **Mandatory**

#### Data Model

The following data model illustrates the RegRep QueryResponse returned by the EB when requesting Evidence Types that prove a specific requirement. It shows the case of a successful response, therefore the `RegistryObjectList` element is present and the `Exception` element is not present.

![Query Response Diagram](./get-evidence-types.png)

#### Example

The query response contains the list of evidence types that fulfil the specific requirement of the query, filtered by the jurisdiction level code. The following example shows a response using the SDG Application Profile XML Representation:

<details>
<summary>Example of a successful Query Response</summary>

```xml
<?xml version="1.0" encoding="UTF-8"?>
<query:QueryResponse
  xmlns:lcm="urn:oasis:names:tc:ebxml-regrep:xsd:lcm:4.0"
  xmlns:query="urn:oasis:names:tc:ebxml-regrep:xsd:query:4.0"
  xmlns:rim="urn:oasis:names:tc:ebxml-regrep:xsd:rim:4.0"
  xmlns:rs="urn:oasis:names:tc:ebxml-regrep:xsd:rs:4.0"
  xmlns:sdg="http://data.europa.eu/p4s"
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  status="urn:oasis:names:tc:ebxml-regrep:ResponseStatusType:Success">

  <!-- Validate with sch/EB-EVI-C and sch/EB-EVI-S-->

  <rim:Slot name="SpecificationIdentifier">
    <rim:SlotValue xsi:type="rim:StringValueType">
      <!-- MUST be a valid version of the Common Service, e.g. "oots-cs:v1.1".  -->
      <rim:Value>oots-cs:v1.1</rim:Value>
    </rim:SlotValue>
  </rim:Slot>

  <rim:RegistryObjectList>
    <!-- One registry object per dataset -->
    <rim:RegistryObject id="urn:uuid:7b8a8ec6-71b9-4864-b147-f65debc14653">
      <rim:Slot name="Requirement">
        <rim:SlotValue xsi:type="rim:AnyValueType">
          <sdg:Requirement>
            <sdg:Identifier>https://sr.oots.tech.ec.europa.eu/requirements/315cfd75-6605-49c4-b0fe-799833b41099</sdg:Identifier>
            <sdg:Name lang="EN">Proof of Birth</sdg:Name>
            <sdg:Name lang="DE">Geburtsnachweis</sdg:Name>

            <!-- List of Reference Frameworks that implement the Requirement-->
            <sdg:ReferenceFramework>

              <!-- Link to the Procedure as implemented in Germany, uniquely identified by the Identifier and Jurisdiction. The name of the procedure, as declared in the SDG Regulation; Is possibly extended or replaced with national reference frameworks when the procedure is implemented by a Member State regulation -->
              <sdg:Identifier>118fd444-6443-42be-a084-c9fbfd1f674d</sdg:Identifier>
              <sdg:Title lang="EN">Procedure #1 - Requesting proof of registration of birth</sdg:Title>
              <sdg:Title lang="DE">Verfahren #1 - Beantragung des Nachweises über die Eintragung in das Geburtenregister</sdg:Title>
              <sdg:Description lang="EN"> Procedure #1 "Requesting proof of registration of birth" belongs to the life event "Birth" of Annex II of the Regulation (EU) 2018/1724
                of the European Parliament and of the Council of 2 October 2018 establishing a single digital gateway to provide access to information, to procedures and to assistance and problem-solving services and amending Regulation (EU) No 1024/2012
              </sdg:Description>
              <sdg:Description lang="DE"> Das Verfahren #1 "Beantragung des Nachweises über die Eintragung in das Geburtenregister" gehört zum Lebensereignis "Geburt" des Anhangs II der Verordnung (EU) 2018/1724 des Europäischen Parlaments und des Rates vom 2. Oktober 2018 über die Einrichtung eines einheitlichen digitalen Zugangstors zu Informationen, Verfahren, Hilfsund Problemlösungsdiensten und zur Änderung der Verordnung (EU) Nr. 1024/2012
              </sdg:Description>

              <!-- The Identifier of the SDGR Procedure which this procedure relates to encoded according to Procedures-CodeList.gc. In this case R1 relates to "Requesting a birth certificate" -->
              <sdg:RelatedTo>
                <sdg:Identifier>R1</sdg:Identifier>
              </sdg:RelatedTo>

              <!-- EEA Country - Mandatory - ISO code. Requirement is connected to the implementation of Procedure 1 in Germany. Regional Codes (AdminUnitLevel 2 and 3) not applicable for DE in this Procedure (Common Reference Framework for the country)  -->
              <sdg:Jurisdiction>
                <sdg:AdminUnitLevel1>DE</sdg:AdminUnitLevel1>
              </sdg:Jurisdiction>

            </sdg:ReferenceFramework>

            <!-- List of Evidence Types that proof the Requirement in Germany, indicated by the Jurisdiction - DE-->
            <!-- Multiple List of Evidence Types might be part of the result - depending on the query parameters -->
            <sdg:EvidenceTypeList>
              <sdg:Identifier>970918e4-1b27-41f0-be20-0a7c5fe4059f</sdg:Identifier>
              <sdg:Name lang="EN">List of Birth Certificates</sdg:Name>
              <sdg:Name lang="EN">Liste der Geburtsurkunden</sdg:Name>

              <!-- Example structure of an evidence type having national coverage area / jurisdiction = DE -->
              <sdg:EvidenceType>
                <!-- EvidenceTypeClassification Information - Provided by the Evidence Broker on the basis of the Semantic Repository. Used for linking with the Semantic Repository and Data Service Directory -->
                <sdg:EvidenceTypeClassification>https://sr.oots.tech.ec.europa.eu/evidencetypeclassifications/DE/ca8afed6-2dc0-422a-a931-d21c3d8d370e</sdg:EvidenceTypeClassification>
                <sdg:Title lang="EN">Certificate of Birth</sdg:Title>
                <sdg:Title lang="DE">Geburtsurkunde</sdg:Title>
                <sdg:Description lang="EN">An official certificate of birth of a person - with first name, surname, sex, date and place of birth, which is obtained from the birth register of the place of birth.</sdg:Description>
                <sdg:Description lang="DE">Eine amtliche Bescheinigung über die Geburt einer Person – mit Vorname, Familienname, Geschlecht, Datum und Ort der Geburt, welche aus dem Geburtsregister des Geburtsortes erstellt wird.</sdg:Description>
                <sdg:Jurisdiction>
                  <!-- EEA Country - Mandatory - ISO code -->
                  <sdg:AdminUnitLevel1>DE</sdg:AdminUnitLevel1>
                  <!-- Regional Code not applicable for DE (Common Evidence for the country), NUTS Code and LAU Code might be added -->
                </sdg:Jurisdiction>
              </sdg:EvidenceType>
            </sdg:EvidenceTypeList>

          </sdg:Requirement>
        </rim:SlotValue>
      </rim:Slot>
    </rim:RegistryObject>
  </rim:RegistryObjectList>
</query:QueryResponse>
```

</details>

### Error Response
>**Version 1.1.0** | **Mandatory**

When an error occurs during the execution of the query, the Common Service returns an exception as defined in the Common Service Query Interface Specification. The exception has the following properties that are profiled for each expected error of the query.

- **xsi:type**: The type of the error, selectable from a predefined set of error classes of the Query Interface of the Common Service.
- **severity**: The severity of the error, selectable from a predefined set of error classes of the Query Interface of the Common Service.
- **message**: A string describing the error in Human Readable form.
- **code**: A code for the error, specified by the Common Service Technical Design documents.
- **detail**: Is used to describe technical details of the error that might be needed to identify and debug the error.

#### Data Model

The Error Response of the EB is syntactically expressed inside an ebRS QueryResponse using the ebRS RegistryExceptionType as shown in data model below.  It shows the case of an unsuccessful response, therefore the RegistryObjectList element is not present and the Exception element is present.

![Error Response Data Model](../../data-services-directory/find-data-services/QueryErrorResponse.png)

#### Example

An example of an Error Response of the Evidence Broker due to a bad query parameters is shown in the following XML snippet:

<details>
<summary>Example of an Error Response</summary>

```xml
<?xml version="1.0" encoding="UTF-8"?>
<query:QueryResponse xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                     xmlns:rs="urn:oasis:names:tc:ebxml-regrep:xsd:rs:4.0"
                     xmlns:rim="urn:oasis:names:tc:ebxml-regrep:xsd:rim:4.0"
                     xmlns:query="urn:oasis:names:tc:ebxml-regrep:xsd:query:4.0"
                     status="urn:oasis:names:tc:ebxml-regrep:ResponseStatusType:Failure">

  <rim:Slot name="SpecificationIdentifier">
    <rim:SlotValue xsi:type="rim:StringValueType">
      <!-- MUST be a valid version of the Common Service, e.g. "oots-cs:v1.1".  -->
      <rim:Value>oots-cs:v1.1</rim:Value>
    </rim:SlotValue>
  </rim:Slot>

  <!-- Valid parameter combinations are defined by the EBErrorResponseCodes -->
  <!-- The Codelists EBErrorCodes provides the values for xsi:type, message and code - - -->
  <!-- The ErrorSeverity provides allowed values for severity. The default value is "Error".  -->
  <!-- The detail may contain further freely defined information about the error -->

  <rs:Exception xsi:type="rs:InvalidRequestExceptionType"
                severity="urn:oasis:names:tc:ebxml-regrep:ErrorSeverityType:Error"
                message="The query parameters do not follow the query specification"
                detail="country-code"
                code="EB:ERR:0003">

  </rs:Exception>
</query:QueryResponse>

```

</details>

### Error Response Codes

The following table provides the list of EBErrorResponseCodes for the exceptions defined in the Error Response. The `Detail` may contain further information for specific errors:

**EB Error Response Codes**

| #   | Title (not used in the exception)        | Type                           | Code        | Message                                                                                 |
| --- |------------------------------------------| ------------------------------ |-------------|-----------------------------------------------------------------------------------------|
| 1   | Resultset is empty                       | rs:ObjectNotFoundExceptionType | EB:ERR:0001 | The result set is empty                                                                 |
| 2   | Requirement not found                    | rs:ObjectNotFoundExceptionType | EB:ERR:0002 | The requirement requested, represented by the requirement id, does not exist            |
| 3   | Bad Query Parameters                     | rs:InvalidRequestExceptionType | EB:ERR:0003 | The query parameters do not follow the query specification                              |
| 4   | Unknown Jurisdiction Level Code          | rs:InvalidRequestExceptionType | EB:ERR:0004 | The jurisdiction level code query parameter is invalid or unknown                       |
| 5   | Unknown procedure                        | rs:InvalidRequestExceptionType | EB:ERR:0005 | The value of the procedure-id query parameter is invalid or unknown                     |
| 6   | Unknown procedure implementation country | rs:InvalidRequestExceptionType | EB:ERR:0006 | The value of the procedure implementation country query parameter is invalid or unknown |
| 7   | Unknown Query                            | rs:InvalidRequestExceptionType | EB:ERR:0007 | The requested Query does not exist                                                      |

### XSD Schema Documentation

The [full schema documentation](pathname:///oxygen/SDG-GenericMetadataProfile-v1.1.0.html) contains the complete list of xsd schemes used within the OOTS. The following xsd schemes are used by this query:

- [SDG-GenericMetadataProfile-v1.1.0.xsd](pathname:///oxygen/SDG-GenericMetadataProfile-v1_1_0_xsd.html)
- [query.xsd](pathname:///oxygen/query_xsd.html)
- [rim.xsd](pathname:///oxygen/rim_xsd.html)
- [rs.xsd](pathname:///oxygen/rs_xsd.html)
- [xml.xsd](pathname:///oxygen/xml_xsd.html)

The XSD schemes can be found in the [OOTS semantic repository](https://code.europa.eu/oots/tdd/tdd_chapters/-/tree/master/OOTS-EDM/xsd)
