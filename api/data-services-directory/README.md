---
hide_table_of_contents: true
---

# Data Service Directory
>**Version 1.1.0** | **Mandatory** | **Assets Versioned in Subsections**

The query interface specification for the Data Service Directory is based on the OASIS ebXML RegRep V4 standard. This standard has multiple protocol bindings that can be used to execute queries. Since the DSD queries have only simple, single-value parameters, the REST binding is used to implement the DSD query interface. This implies that the query transaction is executed as an HTTP GET request with the URL representing the query to execute and the HTTP response carrying the query response as an XML document. This section further profiles the [REST binding as specified in the OASIS RegRep standard](http://docs.oasis-open.org/regrep/regrep-core/v4.0/os/regrep-core-rs-v4.0-os.html#__RefHeading__32747_422331532) for use by the DSD.

The interface only supports a subset of the REST binding. In particular, support for the canonical query parameters defined in section 12 of the OASIS RegRep 4 Registry Services specification is NOT REQUIRED.
