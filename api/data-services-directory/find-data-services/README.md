---
hide_table_of_contents: false
---

# Find Data Services of Evidence Providers
>**Version 1.1.0** | **Mandatory** | **Assets Versioned in Subsections**

## Request
>**Version 1.1.0** | **Mandatory** |

This query allows the Evidence Requester to find all the Data Services of the Evidence Providers that can provide the required type of Evidence Type and are based in a specific geographic area.

The URL pattern for parameterised query invocation is defined as follows in the OASIS RegRep REST binding:

```
«server base url»/rest/search?queryId={the query id}(&{param-name}={param-value})*
```

The query interface consists of a simple predefined parameterised query detailed below. In addition, the RegRep standard defines a set of canonical queries and query parameters that can be used. As the Data Service Directory is not a complete implementation of a RegRep server, it is NOT REQUIRED to support these canonical queries and query parameters. Clients, therefore, SHOULD only use the queries and query parameters specified by this specification. When the canonical queries or parameters are used, the Common Service implementation MAY return an error.

### Description

This query allows the Evidence Requester to find all the Data Services of the Evidence Providers that can provide the required type of Evidence Type and are based in a specific geographic area.

### Parameter Details

This query allows the Evidence Requester to find all the Data Services of the Evidence Providers that can provide the required type of Evidence Type and are based in a specific geographic area. The parameter values for **evidence-type-classification** and for **country-code** must be retrieved from the Evidence Broker. The **values country-code, jurisdiction-admin-l2** and **jurisdiction-admin-l3** are based upon the [codelists](https://code.europa.eu/oots/tdd/tdd_chapters/-/tree/master/OOTS-EDM/codelists) published in the semantic repository. The  values for  **jurisdiction-context-id**, **evidence-type-id**, **jurisdiction-admin-l2** and **jurisdiction-admin-l3**, **EvidenceProviderClassification** must be only used when responding to a DSD exception DSD:ERR:0005 (see section  4.2.4 Example Flows and  4.4.3. Query Error Response of the DSD requesting additional User Provided Attributes (DSD:ERR:005)). The parameter **supported-profile-version** can be used to filter DSD result sets for specific data exchange profiles and versions that are supported by an Access Service in the `ConformsTo` statement.

| Parameter                                                                                                                       | Requirement | Description                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   | Value provided by                                                                                                                                                                  |
|---------------------------------------------------------------------------------------------------------------------------------| ----------- |---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| **queryId**                                                                                                                     | MANDATORY   | This parameter MUST have value **urn:fdc:oots:dsd:ebxml-regrep:queries:dataservices-by-evidencetype-and-jurisdiction**.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       | fixed value                                                                                                                                                                        | 
| **evidence-type-classification**                                                                                                | MANDATORY   | The Evidence Type classification code. The URL must be encoded according to [RFC3986](https://www.rfc-editor.org/rfc/rfc3986) to ensure the URL is valid.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     | Value provided by Evidence Broker                                                                                                                                                  |
| **country-code**                                                                                                                | MANDATORY   | 	Two-letter [ISO 3166-1 alpha-2 country code (EEA_country subset)](https://code.europa.eu/oots/tdd/tdd_chapters/-/blob/master/OOTS-EDM/codelists/OOTS/EEA_Country-CodeList.gc). Used to filter only the Data Service Evidence Types that are provided by a specific member state for an Evidence Type.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        | Value provided by Evidence Broker                                                                                                                                                  |
| **jurisdiction-admin-l2**                                                                                                       | OPTIONAL    | The level two administration code for a specific jurisdiction context expressed using 3 to 5 letter [NUTS code](https://code.europa.eu/oots/tdd/tdd_chapters/-/blob/master/OOTS-EDM/codelists/OOTS/NUTS2024-CodeList.gc?ref_type=heads). This user selected parameter and value shall be only used after receiving DSD:ERR:0005 which requests for an additional parameter and provides the corresponding jurisdiction context.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               | Value provided by User                                                                                                                                                             |
| **jurisdiction-admin-l3**                                                                                                       | OPTIONAL    | The level three administration code for a specific jurisdiction context expressed using [LAU Code](https://code.europa.eu/oots/tdd/tdd_chapters/-/blob/master/OOTS-EDM/codelists/OOTS/LAU2022-CodeList.gc). This user selected parameter and value shall be only used after receiving DSD:ERR:0005 which requests for an additional parameter and provides the corresponding jurisdiction context.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            | Value provided by User                                                                                                                                                             |
| **jurisdiction-context-id**                                                                                                     | OPTIONAL    | Used to provide the jurisdiction context id used for the query. The parameter MUST be used when responding to a DSD exception DSD:ERR:0005 received, by providing the Jurisdiction Context Id found in the exception.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         |
| **evidence-type-id**                                                                                                            | OPTIONAL    | Used to provide the selected Data Service Evidence Type from a set of multiple. The parameter MUST be used when responding to a DSD exception DSD:ERR:0005 received, by providing the Identifier of the selected DataServiceEvidenceType found in the exception.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              | Value provided by Exception DSD:ERR:005                                                                                                                                            |
| The parameter for **EvidenceProviderClassification** must be the value of **sdg:EvidenceProviderClassification/sdg:Identifier** | OPTIONAL    | Used to provide the user selected value for EvidenceProviderClassification from a set of multiple. The user selected parameter and value MUST be used when responding to a DSD exception DSD:ERR:0005 received, by providing the Identifier of the selected DataServiceEvidenceType found in the exception                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    | Parameter provided by Exception DSD:ERR:005 / Value provided by User                                                                                                               |
| **specification**                                                                                                               | OPTIONAL    | The parameter can be used to filter DSD result sets for specific data exchange profiles and versions that are supported by an Access Service in the `ConformsTo` statement. In the OOTS, Access Services can for example support the Evidence Exchange Data Model (EDM) in version 1.0 (`oots-edm:v1.0`)  or in version 1.1. (`oots-edm:v1.1`)  or in both versions. Thus, the parameter **specification** helps users to find an Access Service that ConformTo a particular profile or version.<br/><br/>**EUDI Wallet Extension**<br/>The DSD may be used to support other scenarios than OOTS. The analysis of synergies with EUDI Wallet has shown that two new services need to be registered to the DSD that can be assigned to Access Services. In that case, Access Services offer data exchange profiles in the EUDI Wallet domain that are different from the OOTS Evidence Exchange Data Model.<br/>Two new Identifiers from EUDI Wallet PoC complement the existing specifications from OOTS and may be supported by Access Services:<br/>- `eudi-evs:1.0`   ← Evidence Type Verification Service<br/>- `eudi-pubeea-is:1.0`   ← Evidence Type Pub-EEA Wallet Issuing Service<br/><br/>_Additional notes:_<br/>- If the parameter is not used all results are returned.<br/>- If `oots` is specified, all results for the OOTS are returned. If `eudi` is specified, all results for the EUDI are returned<br/>- If `eudi-evs`  is specified, all versions relating to the Evidence Type Verification Service are returned. Likewise for `eudi-pubeea-is`  or `oots-edm`<br/>- If `oots-edm:v1`  is specified, all minor versions of 1 will be returned => `oots-edm:v1.0` and  `oots-edm:v1.1`. Likewise for `eudi-pubeea-is:1` and `eudi-evs:1` | Value provided by User/Possible Values:<br/><br/>**OOTS**<br/>- `oots-edm:v1.0`<br/>- `oots-edm:v1.1`<br/><br/>**EUDI Wallet PoC**<br/>- `eudi-evs:1.0`<br/>- `eudi-pubeea-is:1.0` |

### Example Flows

The Evidence Requester needs to fetch the Evidence Providers that can provide an evidence type with the included values _https://sr.oots.tech.ec.europa.eu/evidencetypeclassifications/DE/ca8afed6-2dc0-422a-a931-d21c3d8d370e_ (pointing to a Certificate of Birth) for **evidence-type-classification** and _DE_ for **country-code**, as it was extracted from the Evidence Broker. To do this, it encodes the URL for evidence-type classification and executes the following HTTP REST Call to the DSD:

> _«server base url»_/rest/search?queryId=urn:fdc:oots:dsd:ebxml-regrep:queries:dataservices-by-evidencetype-and-jurisdiction&**evidence-type-classification**=https%3A%2F%2Fsr.oots.tech.ec.europa.eu%2Fevidencetypeclassifications%2FDE%2Fca8afed6-2dc0-422a-a931-d21c3d8d370e&**country-code**=DE

The DSD receives the requests and checks whether the specific evidenceType _Certificate of Birth_ for country _DE_ has a `DataServiceEvidenceType`. Three potential responses can be given thus the following flows must distinguished:

#### Positive Flow

A `DataServiceEvidenceType` exists. The DSD will provide a [Query Response](#query-response)

The following figure illustrates the positive flow resulting in a `query:QueryResponse`

![Positive Flow of the DSD](./dsd_fdsep_base.png)

#### Negative Flow

The request to the DSD cannot be responded and an exception response is returned. The DSD will provide a [Error Response](#error-response) whereas the exception response contains an `error code` other than _DSD:ERR:0005_.

The following figure illustrates the negative flow resulting in a `query:Exception`

![Negative Flow of the DSD](./dsd_fdsep_exception.png)

#### Conditional Flow (_DSD:ERR:0005_)

The request to the DSD cannot be responded but a `DataServiceEvidenceType` exists that contains `EvidenceProviderJurisdictionDetermination` **(Parameter: jurisdiction-context-id)** and/or an `EvidenceProviderClassification` **(Parameter = value of sdg:EvidenceProviderClassification/sdg:Identifier)** scheme. The DSD will provide an [Error Response requesting additional User Provided Attributes (DSD:ERR:005)](#error-response-requesting-additional-user-provided-attributes-dsderr005) whereas the exception response is returned with `error code` _DSD:ERR:0005_. The `error code` _DSD:ERR:0005_ points to a follow-up query with the additional user provided attributes required to answer the query successfully.

In our example, both exists and thus the DSD must return an exception response with `error code` _DSD:ERR:0005_ requesting information on both the **jurisdiction-context-id** and the the value of sdg:EvidenceProviderClassification/sdg:Identifier **b07146b6-1468-41a7-a10f-ee2a61429dbc**. The values for these parameters are user provided and must be based upon the [codelists](https://code.europa.eu/oots/tdd/tdd_chapters/-/tree/master/OOTS-EDM/codelists) published in the semantic repository. The Evidence Requester will then request the following information from the user:

- `EvidenceProviderJurisdictionDetermination` **(Parameter: jurisdiction-context-id)**: In this example the Place Of Birth, using the [LAU Code](https://code.europa.eu/oots/tdd/tdd_chapters/-/blob/master/OOTS-EDM/codelists/OOTS/LAU2022-CodeList.gc) for country _DE_ is requested. For LAU codes the additional parameter **jurisdiction-admin-l3** MUST be used.
- `EvidenceProviderClassification` **(Parameter: b07146b6-1468-41a7-a10f-ee2a61429dbc)**: In this example the specific EvidenceProviderClassification scheme is identified by the UUID **b07146b6-1468-41a7-a10f-ee2a61429dbc** and it requests principle residence information of the user based on a [codelists](https://code.europa.eu/oots/tdd/tdd_chapters/-/tree/master/OOTS-EDM/codelists) published in the semantic repository distinguishing between _ApplicantHasNeverBeenResidentInGermany_ and _ApplicantHasBeenResidentInGermany_.

With the information given by the user (**jurisdiction-context-id** = _02000000_; **b07146b6-1468-41a7-a10f-ee2a61429dbc** = _ApplicantHasBeenResidentInGermany_), the Evidence requester then create a new HTTP REST Call to the DSD:

> _«server base url»_/rest/search?queryId=urn:fdc:oots:dsd:ebxml-regrep:queries:dataservices-by-evidencetype-and-jurisdiction&**evidence-type-classification**=https%3A%2F%2Fsr.oots.tech.ec.europa.eu%2Fevidencetypeclassifications%2FDE%2Fca8afed6-2dc0-422a-a931-d21c3d8d370e&**country-code**=DE&**evidence-type-id**=2af27699-f131-4411-8fdb-9e8cd4e8bded&**jurisdiction-context-id**=5ce148b9-5578-4049-aecf-af7bb55714b5&**jurisdiction-admin-l3**=02000000&**b07146b6-1468-41a7-a10f-ee2a61429dbc**=ApplicantHasBeenResidentInGermany

where the value of **evidence-type-id** _2af27699-f131-4411-8fdb-9e8cd4e8bded_, the value **jurisdiction-context-id** _5ce148b9-5578-4049-aecf-af7bb55714b5_ and the parameter **b07146b6-1468-41a7-a10f-ee2a61429dbc** (pointing to sdg:EvidenceProviderClassification/sdg:Identifier) are retrieved from the Exception and the LAU code _02000000_ for **jurisdiction-admin-l3** and the code _ApplicantHasBeenResidentInGermany_ for **EvidenceProviderClassification** are values provided by the user.

The following figure illustrates the conditional flow resulting in a `query:Exception` with `error code` _DSD:ERR:0005_ (first response) and a `query:QueryResponse` (second response)

![Conditional Flow of the DSD](./dsd_fdsep_user_atts.png)

## Response

The Query Response of the DSD returns a RegRep QueryResponse document which
MUST either contain an `Exception` or `RegistryObjectList` element
with zero or more `RegistryObjects`. Each `RegistryObject` in the result MUST
include one `Slot` element with a `SlotValue` of type _rim:AnyValueType_ and a single
`sdg:DataServiceEvidenceType` child element, following the OOTS Application Profile of the DSD. The
OOTS application profile of the DSD describes how the [SDG-Generic-Metadata Profile (SDG-syntax)](https://code.europa.eu/oots/tdd/tdd_chapters/-/tree/master/OOTS-EDM/xsd/sdg)
is profiled in [ebRIM](http://docs.oasis-open.org/regrep/regrep-core/v4.0/os/regrep-core-rim-v4.0-os.html)
in order to compose a valid QueryResponse. It, therefore, contains a mapping to the underlying
[SDG-syntax](https://code.europa.eu/oots/tdd/tdd_chapters/-/blob/master/OOTS-EDM/xsd/sdg/SDG-GenericMetadataProfile-v1.0.0.xsd)
elements and necessary parameters to compose a `QueryResponse`. The namespace of the [SDG-syntax](https://code.europa.eu/oots/tdd/tdd_chapters/-/blob/master/OOTS-EDM/xsd/sdg/SDG-GenericMetadataProfile-v1.0.0.xsd) is http://data.europa.eu/p4s.

### Query Response
>**Version 1.0.0** | **Mandatory** |

#### Data Model

The following data model illustrates the RegRep successful QueryResponse returned by the DSD:

![Query Response diagram](./QueryResponse.png)

#### Example

An example of a successful Query Response of the DSD providing a collection of Data Services of Service Providers for a specific Evidence Type is shown below:

<details>
<summary>Example of a successful Query Response</summary>

```xml
<?xml version="1.0" encoding="UTF-8"?>
<query:QueryResponse
  xmlns:query="urn:oasis:names:tc:ebxml-regrep:xsd:query:4.0"
  xmlns:rim="urn:oasis:names:tc:ebxml-regrep:xsd:rim:4.0"
  xmlns:rs="urn:oasis:names:tc:ebxml-regrep:xsd:rs:4.0"
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xmlns:sdg="http://data.europa.eu/p4s"
  status="urn:oasis:names:tc:ebxml-regrep:ResponseStatusType:Success">

  <rim:Slot name="SpecificationIdentifier">
    <rim:SlotValue xsi:type="rim:StringValueType">
      <!-- MUST be a valid version of the Common Service, e.g. "oots-cs:v1.1".  -->
      <rim:Value>oots-cs:v1.1</rim:Value>
    </rim:SlotValue>
  </rim:Slot>

  <rim:RegistryObjectList>
    <!-- One registry object per dataset -->
    <rim:RegistryObject id="urn:uuid:45c2b94b-5d2b-484b-b6ab-eb542da191cc">
      <rim:Slot name="DataServiceEvidenceType">
        <rim:SlotValue xsi:type="rim:AnyValueType">
          <sdg:DataServiceEvidenceType>

            <!-- - - Evidence Type Metadata - - -->

            <!-- The Data Service assigned Unique Identifier of the Evidence Type. Must be used in the Evidence Exchange Request -->
            <sdg:Identifier>2af27699-f131-4411-8fdb-9e8cd4e8bded</sdg:Identifier>

            <!-- Classification Information - Used for linking with the Semantic Repository and Evidence Broker -->
            <sdg:EvidenceTypeClassification>https://sr.oots.tech.ec.europa.eu/evidencetypeclassifications/DE/ca8afed6-2dc0-422a-a931-d21c3d8d370e</sdg:EvidenceTypeClassification>
            <sdg:Title lang="EN">Certificate of Birth</sdg:Title>
            <sdg:Title lang="DE">Geburtsurkunde</sdg:Title>
            <sdg:Description lang="EN">An official certificate of birth of a person - with first name, surname, sex, date and place of birth, which is obtained from the birth register of the place of birth.</sdg:Description>
            <sdg:Description lang="DE">Eine amtliche Bescheinigung über die Geburt einer Person – mit Vorname, Familienname, Geschlecht, Datum und Ort der Geburt, welche aus dem Geburtsregister des Geburtsortes erstellt wird.</sdg:Description>

            <!-- Distribution Information - Multiple Distributions per Data Service Evidence Type -->
            <!-- XML Distribution, conforming to the common data model on Birth Certificate -->
            <!-- Preferred XML Distribution can be referenced in the Evidence Request -->
            <sdg:DistributedAs>
              <sdg:Format>application/xml</sdg:Format>
              <sdg:ConformsTo>https://sr.oots.tech.ec.europa.eu/datamodels/certificate-of-birth-1.0.0</sdg:ConformsTo>
            </sdg:DistributedAs>
            <!-- XML Distribution, conforming to the common data model on Birth Certificate, Linking to the subset "Age of Majority" only -->
            <sdg:DistributedAs>
              <sdg:Format>application/xml</sdg:Format>
              <sdg:ConformsTo>https://sr.oots.tech.ec.europa.eu/datamodels/certificate-of-birth-1.0.0</sdg:ConformsTo>
              <sdg:Transformation>https://sr.oots.tech.ec.europa.eu/datamodels/certificate-of-birth-1.0.0/age-of-majority</sdg:Transformation>
            </sdg:DistributedAs>
            <!-- PDF Distribution. PDF is unstructured data so there is no conformance to a data model -->
            <sdg:DistributedAs>
              <sdg:Format>application/pdf</sdg:Format>
            </sdg:DistributedAs>

            <!-- - - Evidence Provider and Data Service Metadata - - -->

            <!-- Access Service represents the Data Service serving the piece of Evidence on behalf of an Evidence Provider -->
            <!-- Multiple Access Services possible, one per Evidence Provider. In this example the Access Service (e.g. an Intermediary Platform) is referenced for the Evidence Provider (Publishers) -->
            <sdg:AccessService>
              <!-- The identifier of the Access Service, using ebcore Party ID Type. Used in eDelivery Evidence Exchange for PMode Mapping - Corner 3 (C3). C3 is an Access Point that receives messages on behalf of C4. The scheme ID should point to a valid Electronic Address Scheme (EAS) from the codelist EAS.gc -->
              <sdg:Identifier schemeID="urn:cef.eu:names:identifier:EAS:0060">8889909098</sdg:Identifier>
              <!-- The registered version(s) of the eDelivery profile and Exchange Data Model used by the access service (e.g. oots-edm:v1.0, oots-edm:v1.1) -->
              <sdg:Name lang="EN">German Intermediary Platform I</sdg:Name>
              <sdg:ConformsTo>oots-edm:v1.0</sdg:ConformsTo>

              <!-- The Evidence Provider 1 "Civil Registration Office Berlin I" -->
              <!-- The Evidence Provider Information of this Access Service - Corner 4 (C4). C4 is the final recipient party, which can be selected to receive the Evidence Request Query. The scheme ID should point to a valid Electronic Address Scheme (EAS) from the codelist EAS.gc  -->
              <sdg:Publisher>
                <sdg:Identifier schemeID="urn:cef.eu:names:identifier:EAS:9930">DE73524311</sdg:Identifier>
                <sdg:Name lang="EN">Civil Registration Office Berlin I</sdg:Name>
                <sdg:Description lang="EN">As a foreign registry office of the Federal Republic of Germany, Registry Office I in Berlin is responsible in particular for Certifications of births of Germans abroad who have never had a residence in Germany. </sdg:Description>
                <sdg:Address>
                  <sdg:FullAddress>Schönstedtstraße 5, 13357 Berlin, Germany</sdg:FullAddress>
                  <sdg:LocatorDesignator>5</sdg:LocatorDesignator>
                  <sdg:PostCode>13357</sdg:PostCode>
                  <sdg:PostCityName>Berlin</sdg:PostCityName>
                  <sdg:Thoroughfare>Schönstedtstraße</sdg:Thoroughfare>
                  <!-- Country Code -->
                  <sdg:AdminUnitLevel1>DE</sdg:AdminUnitLevel1>
                  <!-- NUTS Code -->
                  <sdg:AdminUnitLevel2>DE3</sdg:AdminUnitLevel2>
                </sdg:Address>

                <!-- The Publisher is only able to provide information for LAU 11000000, Berlin, City   -->
                <sdg:Jurisdiction>
                  <!-- EEA Country Code -->
                  <sdg:AdminUnitLevel1>DE</sdg:AdminUnitLevel1>
                  <!-- LAU Code -->
                  <sdg:AdminUnitLevel3>11000000</sdg:AdminUnitLevel3>
                </sdg:Jurisdiction>

                <!-- An Example of an Information Concept that can be used to distinguish the capabilities of the Evidence Provider. In this example the capability of the publisher is to provide information even if the applicant has never resided in the country is identified. -->
                <sdg:ClassificationConcept>
                  <!-- The Unique Identifier of this Classification Concept defined by the element <sdg:EvidenceProviderClassification> -->
                  <sdg:Identifier>b07146b6-1468-41a7-a10f-ee2a61429dbc</sdg:Identifier>
                  <sdg:Type>Codelist</sdg:Type>
                  <!-- Definition of the value expression. Must be published in the Semantic Repository. In this example the URL points to the codelist "countryresidence" with two values "ApplicantHasBeenResidentInGermany" and "ApplicantHasNeverBeenResidentInGermany"  -->
                  <sdg:ValueExpression>https://sr.oots.tech.ec.europa.eu/ep-classification/de/countryresidence</sdg:ValueExpression>
                  <sdg:Description lang="EN">Rules of responsibility for the processing of personal status cases of German citizens without residence information
                  </sdg:Description>
                  <sdg:Description lang="DE">Zuständigkeitsregelungen für die Bearbeitung von personenstandsrechtlichen Vorgängen deutscher Staatsangehöriger ohne Wohnsitzinformation</sdg:Description>
                  <!--  The string values indicate that the Publisher is able to provide information even if the applicant has never resided in Germany. Next to this it can provide information if the applicant has been resided in Germany or more precisely in Berlin as indicated through the LAU code of the Jurisdiction of the Publisher  -->
                  <sdg:SupportedValue>
                    <sdg:StringValue>ApplicantHasNeverBeenResidentInGermany</sdg:StringValue>
                    <sdg:StringValue>ApplicantHasBeenResidentInGermany</sdg:StringValue>
                  </sdg:SupportedValue>
                </sdg:ClassificationConcept>
              </sdg:Publisher>
            </sdg:AccessService>

            <!-- Level Of Assurance Required for the Evidence Type -->
            <sdg:AuthenticationLevelOfAssurance>High</sdg:AuthenticationLevelOfAssurance>
            <!-- Free text to include any other important information -->
            <sdg:Note lang="EN">Payment: Free of charge</sdg:Note>

            <!-- Determination of the Jurisdiction Mapping to the User's attributes. LAU is required as additional user input parameter for the Query in order to identify the Place of Birth as Jurisdiction Context -->
            <sdg:EvidenceProviderJurisdictionDetermination>
              <!-- The Unique Identifier of this EvidenceProviderJurisdictionDetermination -->
              <sdg:JurisdictionContextId>5ce148b9-5578-4049-aecf-af7bb55714b5</sdg:JurisdictionContextId>
              <sdg:JurisdictionContext lang="EN">Place Of Birth</sdg:JurisdictionContext>
              <sdg:JurisdictionContext lang="DE">Geburtsort</sdg:JurisdictionContext>
              <!-- Codelist defining the jurisdiction levels, published in the semantic repository - JurisdictionLevel-CodeList.gc -->
              <sdg:JurisdictionLevel>LAU</sdg:JurisdictionLevel>
            </sdg:EvidenceProviderJurisdictionDetermination>

            <!-- Determination of the EvidenceProviderClassification to the User's attributes. In this example the applicant must indicate if he has ever resided in the country.  -->
            <sdg:EvidenceProviderClassification>
              <!-- The Unique Identifier of this EvidenceProviderClassification reused by the element <sdg:ClassificationConcept> of Publishers -->
              <sdg:Identifier>b07146b6-1468-41a7-a10f-ee2a61429dbc</sdg:Identifier>
              <sdg:Type>Codelist</sdg:Type>
              <!-- Definition of the value expression. Must be published in the Semantic Repository. The codelist contains the values ApplicantHasNeverBeenResidentInGermany and ApplicantHasBeenResidentInGermany -->
              <!-- During the query process the user has to select one of the two values as additional input parameter for the query in order to identify the correct publisher / evidence provider -->
              <!-- The selected value will be mapped against the Classification Values of Publishers -->
              <sdg:ValueExpression>https://sr.oots.tech.ec.europa.eu/ep-classification/de/countryresidence</sdg:ValueExpression>
              <sdg:Description lang="EN">Rules of responsibility for the processing of personal status cases of German citizens without residence information
              </sdg:Description>
              <sdg:Description lang="DE">Zuständigkeitsregelungen für die Bearbeitung von personenstandsrechtlichen Vorgängen deutscher Staatsangehöriger ohne Wohnsitzinformation</sdg:Description>
            </sdg:EvidenceProviderClassification>
          </sdg:DataServiceEvidenceType>
        </rim:SlotValue>
      </rim:Slot>
    </rim:RegistryObject>
  </rim:RegistryObjectList>
</query:QueryResponse>
```

</details>

### Error Response
>**Version 1.0.0** | **Mandatory** |

When an error occurs during the execution of the query, the Common Service returns an exception as defined in the Common Service Query Interface Specification. The exception has the following properties that are profiled for each expected error of the query.

- **xsi:type**: The type of the error, selectable from a predefined set of error classes of the Query Interface of the Common Service.
- **severity**: The severity of the error, selectable from a predefined set of error classes of the Query Interface of the Common Service.
- **message**: A string describing the error in Human Readable form.
- **code**: A code for the error, specified by the Common Service Technical Design documents.
- **detail**: Is used to describe technical details of the error that might be needed to identify and debug the error.

#### Data Model

The Error Response of the DSD is syntactically expressed inside an ebRS QueryResponse using the ebRS RegistryExceptionType as shown in data model below:

![Error Response Data Model](QueryErrorResponse.png)

#### Example

An example of the Error Response of the DSD due to bad query parameters is shown in the following XML snippet:

<details>
<summary>Example of the Error Response</summary>

```xml
<?xml version="1.0" encoding="UTF-8"?>
<query:QueryResponse xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                     xmlns:rs="urn:oasis:names:tc:ebxml-regrep:xsd:rs:4.0"
                     xmlns:rim="urn:oasis:names:tc:ebxml-regrep:xsd:rim:4.0"
                     xmlns:query="urn:oasis:names:tc:ebxml-regrep:xsd:query:4.0"
                     xmlns:xlink="http://www.w3.org/1999/xlink"
                     status="urn:oasis:names:tc:ebxml-regrep:ResponseStatusType:Failure">

  <rim:Slot name="SpecificationIdentifier">
    <rim:SlotValue xsi:type="rim:StringValueType">
      <!-- MUST be a valid version of the Common Service, e.g. "oots-cs:v1.1".  -->
      <rim:Value>oots-cs:v1.1</rim:Value>
    </rim:SlotValue>
  </rim:Slot>

  <!-- Valid parameter combinations are defined by the DSDErrorResponseCodes -->
  <!-- The Codelists DSDErrorCodes provides the values for xsi:type, message and code - - -->
  <!-- The ErrorSeverity provides allowed values for severity. The default value is "Error". In case of code DSD:ERR:0005 the severity "AdditionalInput" may point to a DSD request that requires additional discovery metadata of the user in order to be answered successfully. -->
  <!-- The detail may contain further freely defined information about the error -->
  <rs:Exception xsi:type="rs:InvalidRequestExceptionType"
                severity="urn:oasis:names:tc:ebxml-regrep:ErrorSeverityType:Error"
                message="Bad query parameters"
                detail="The query parameters do not follow the query specification"
                code="DSD:ERR:0003">

  </rs:Exception>
</query:QueryResponse>
```

</details>

### Error Response Codes

The following table provides the list of QueryErrorResponseCodes for the exceptions defined in the Error Response. The `Detail` may contain further information for specific errors.

**DSDErrorResponseCodes**

| #   | Title (not used by the exception) | Type                           | Code         | Message                                                                     |
| --- |-----------------------------------| ------------------------------ |--------------|-----------------------------------------------------------------------------|
| 1   | Data Services Not Found           | rs:ObjectNotFoundExceptionType | DSD:ERR:0001 | No Data Services were found based on the given parameters                   |
| 2   | Evidence Type Not Found           | rs:ObjectNotFoundExceptionType | DSD:ERR:0002 | The Evidence Type requested cannot be found                                 |
| 3   | Bad Query Parameters              | rs:InvalidRequestExceptionType | DSD:ERR:0003 | The query parameters do not follow the query specification                  |
| 4   | Unknown Query                     | rs:InvalidRequestExceptionType | DSD:ERR:0004 | The requested Query does not exist                                          |
| 5   | Additional Parameters Required    | rs:ObjectNotFoundExceptionType | DSD:ERR:0005 | The query requires the included extra attributes to be provided by the user |
| 6   | Incorrect Parameter Value         | rs:InvalidRequestExceptionType | DSD:ERR:0006 | Incorrect provided value for requested parameters                           |

### Error Response requesting additional User Provided Attributes (DSD:ERR:005)

#### Data Model

When a `DataServiceEvidenceType` contains required Evidence Provider Discovery Metadata, the DSD will respond initially with an exception containing the required Discovery Metadata. The exception used is the `DSD:ERR:0005` with type `rs:ObjectNotFoundExceptionType` and uses an extension of the Error Response for the DSD Model. The following diagram summarizes the extension of the data model for the `DSD:ERR:0005` exception:

![Error Response requesting additional User Provided Attributes Data Model](./QueryErrorResponseDSDERR005.png)

#### Example of a Error Response with Jurisdiction Determination

The following example shows an exception sent back to the Evidence Requester containing a `JurisdictionDetermination` slot, stating that the user should provide his place of birth using LAU codes:

<details>
<summary>Example of an Error Response with Jurisdiction Determination</summary>

```xml
<?xml version="1.0" encoding="UTF-8" ?>
<query:QueryResponse xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                     xmlns:rs="urn:oasis:names:tc:ebxml-regrep:xsd:rs:4.0"
                     xmlns:rim="urn:oasis:names:tc:ebxml-regrep:xsd:rim:4.0"
                     xmlns:query="urn:oasis:names:tc:ebxml-regrep:xsd:query:4.0" xmlns:sdg="http://data.europa.eu/p4s"
                     status="urn:oasis:names:tc:ebxml-regrep:ResponseStatusType:Failure">

  <rim:Slot name="SpecificationIdentifier">
    <rim:SlotValue xsi:type="rim:StringValueType">
      <!-- MUST be a valid version of the Common Service, e.g. "oots-cs:v1.1".  -->
      <rim:Value>oots-cs:v1.1</rim:Value>
    </rim:SlotValue>
  </rim:Slot>

  <!-- Valid parameter combinations are defined by the DSDErrorResponseCodes -->
  <!-- The Codelists DSDErrorCodes provides the values for xsi:type, message and code - - -->
  <!-- The ErrorSeverity provides allowed values for severity. In this example (code DSD:ERR:0005) the severity "AdditionalInput" points to a DSD request that requires additional discovery metadata of the user in order to be answered successfully. -->
  <!-- The detail may contain further freely defined information about the error -->

  <rs:Exception xsi:type="rs:ObjectNotFoundExceptionType"
                severity="urn:sr.oots.tech.ec.europa.eu:codes:ErrorSeverity:DSDErrorResponse:AdditionalInput"
                message="The query requires the included extra attributes to be provided by the user."
                detail="JurisdictionDetermination"
                code="DSD:ERR:0005">

    <rim:Slot name="DataServiceEvidenceType">
      <rim:SlotValue xsi:type="rim:AnyValueType">
        <sdg:DataServiceEvidenceType>
          <!-- The Data Service assigned Unique Identifier of the Evidence Type. Must be provided in the second query as additional parameter -->
          <sdg:Identifier>2af27699-f131-4411-8fdb-9e8cd4e8bded</sdg:Identifier>

          <!-- Classification Information - Used for linking with the Semantic Repository and Evidence Broker -->
          <sdg:EvidenceTypeClassification>https://sr.oots.tech.ec.europa.eu/evidencetypeclassifications/DE/ca8afed6-2dc0-422a-a931-d21c3d8d370e</sdg:EvidenceTypeClassification>
          <sdg:Title lang="EN">Certificate of Birth</sdg:Title>
          <sdg:Title lang="DE">Geburtsurkunde</sdg:Title>
          <sdg:Description lang="EN">An official certificate of birth of a person - with first name, surname, sex, date and place of birth, which is obtained from the birth register of the place of birth.</sdg:Description>
          <sdg:Description lang="DE">Eine amtliche Bescheinigung über die Geburt einer Person – mit Vorname, Familienname, Geschlecht, Datum und Ort der Geburt, welche aus dem Geburtsregister des Geburtsortes erstellt wird.</sdg:Description>

          <!-- Distribution Information - Multiple Distributions per Data Service Evidence Type -->
          <!-- XML Distribution, conforming to the common data model on Birth Certificate -->
          <!-- Preferred XML Distribution can be referenced in the Evidence Request -->
          <sdg:DistributedAs>
            <sdg:Format>application/xml</sdg:Format>
            <sdg:ConformsTo>https://sr.oots.tech.ec.europa.eu/datamodels/certificate-of-birth-1.0.0</sdg:ConformsTo>
          </sdg:DistributedAs>
          <!-- XML Distribution, conforming to the common data model on Birth Certificate, Linking to the subset "Age of Majority" only -->
          <sdg:DistributedAs>
            <sdg:Format>application/xml</sdg:Format>
            <sdg:ConformsTo>https://sr.oots.tech.ec.europa.eu/datamodels/certificate-of-birth-1.0.0</sdg:ConformsTo>
            <sdg:Transformation>https://sr.oots.tech.ec.europa.eu/datamodels/certificate-of-birth-1.0.0/age-of-majority</sdg:Transformation>
          </sdg:DistributedAs>
          <!-- PDF Distribution. PDF is unstructured data so there is no conformance to a data model -->
          <sdg:DistributedAs>
            <sdg:Format>application/pdf</sdg:Format>
          </sdg:DistributedAs>

          <!-- Level Of Assurance Required for the Evidence Type -->
          <sdg:AuthenticationLevelOfAssurance>High</sdg:AuthenticationLevelOfAssurance>
          <!-- Free text to include any other important information -->
          <sdg:Note lang="EN">Payment: Free of charge</sdg:Note>
        </sdg:DataServiceEvidenceType>
      </rim:SlotValue>
    </rim:Slot>

    <!-- Jurisdiction Mapping Requests -->
    <rim:Slot name="JurisdictionDetermination">
      <rim:SlotValue xsi:type="rim:AnyValueType">
        <!-- Determination of the Jurisdiction Mapping to the User's attributes. LAU is required as additional user input parameter for the Query in order to identify the Place of Birth as Jurisdiction Context -->
        <sdg:EvidenceProviderJurisdictionDetermination>
          <!-- The Unique Identifier of this EvidenceProviderJurisdictionDetermination -->
          <sdg:JurisdictionContextId>5ce148b9-5578-4049-aecf-af7bb55714b5</sdg:JurisdictionContextId>
          <sdg:JurisdictionContext lang="EN">Place Of Birth</sdg:JurisdictionContext>
          <sdg:JurisdictionContext lang="DE">Geburtsort</sdg:JurisdictionContext>
          <!-- Codelist defining the jurisdiction levels, published in the semantic repository - JurisdictionLevel-CodeList.gc -->
          <sdg:JurisdictionLevel>LAU</sdg:JurisdictionLevel>
        </sdg:EvidenceProviderJurisdictionDetermination>
      </rim:SlotValue>
    </rim:Slot>
  </rs:Exception>
</query:QueryResponse>
```
</details>

#### Example of an Error Response with Classification Concepts requested by the user

The following example shows an exception sent back to the Evidence Requester containing a `UserRequestedClassificationConcepts` slot, stating that the user should provide information if he has ever resided in the country using a string value:

<details>
<summary>Example of an Error Response with Classification Concepts requested by the user</summary>

```xml
<?xml version="1.0" encoding="UTF-8" ?>
<query:QueryResponse xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                     xmlns:rs="urn:oasis:names:tc:ebxml-regrep:xsd:rs:4.0"
                     xmlns:rim="urn:oasis:names:tc:ebxml-regrep:xsd:rim:4.0"
                     xmlns:query="urn:oasis:names:tc:ebxml-regrep:xsd:query:4.0" xmlns:sdg="http://data.europa.eu/p4s"
                     status="urn:oasis:names:tc:ebxml-regrep:ResponseStatusType:Failure">

  <rim:Slot name="SpecificationIdentifier">
    <rim:SlotValue xsi:type="rim:StringValueType">
      <!-- MUST be a valid version of the Common Service, e.g. "oots-cs:v1.1".  -->
      <rim:Value>oots-cs:v1.1</rim:Value>
    </rim:SlotValue>
  </rim:Slot>

  <!-- Valid parameter combinations are defined by the DSDErrorResponseCodes -->
  <!-- The Codelists DSDErrorCodes provides the values for xsi:type, message and code - - -->
  <!-- The ErrorSeverity provides allowed values for severity. In this example (code DSD:ERR:0005) the severity "AdditionalInput" points to a DSD request that requires additional discovery metadata of the user in order to be answered successfully. -->
  <!-- The detail may contain further freely defined information about the error -->

  <rs:Exception xsi:type="rs:ObjectNotFoundExceptionType"
                severity="urn:sr.oots.tech.ec.europa.eu:codes:ErrorSeverity:DSDErrorResponse:AdditionalInput"
                message="The query requires the included extra attributes to be provided by the user."
                detail="UserRequestedClassificationConcepts"
                code="DSD:ERR:0005">

    <rim:Slot name="DataServiceEvidenceType">
      <rim:SlotValue xsi:type="rim:AnyValueType">
        <sdg:DataServiceEvidenceType>
          <!-- The Data Service assigned Unique Identifier of the Evidence Type. Must be provided in the second query as additional parameter -->
          <sdg:Identifier>2af27699-f131-4411-8fdb-9e8cd4e8bded</sdg:Identifier>

          <!-- Classification Information - Used for linking with the Semantic Repository and Evidence Broker -->
          <sdg:EvidenceTypeClassification>https://sr.oots.tech.ec.europa.eu/evidencetypeclassifications/DE/ca8afed6-2dc0-422a-a931-d21c3d8d370e</sdg:EvidenceTypeClassification>
          <sdg:Title lang="EN">Certificate of Birth</sdg:Title>
          <sdg:Title lang="DE">Geburtsurkunde</sdg:Title>
          <sdg:Description lang="EN">An official certificate of birth of a person - with first name, surname, sex, date and place of birth, which is obtained from the birth register of the place of birth.</sdg:Description>
          <sdg:Description lang="DE">Eine amtliche Bescheinigung über die Geburt einer Person – mit Vorname, Familienname, Geschlecht, Datum und Ort der Geburt, welche aus dem Geburtsregister des Geburtsortes erstellt wird.</sdg:Description>

          <!-- Distribution Information - Multiple Distributions per Data Service Evidence Type -->
          <!-- XML Distribution, conforming to the common data model on Birth Certificate -->
          <!-- Preferred XML Distribution can be referenced in the Evidence Request -->
          <sdg:DistributedAs>
            <sdg:Format>application/xml</sdg:Format>
            <sdg:ConformsTo>https://sr.oots.tech.ec.europa.eu/datamodels/certificate-of-birth-1.0.0</sdg:ConformsTo>
          </sdg:DistributedAs>
          <!-- XML Distribution, conforming to the common data model on Birth Certificate, Linking to the subset "Age of Majority" only -->
          <sdg:DistributedAs>
            <sdg:Format>application/xml</sdg:Format>
            <sdg:ConformsTo>https://sr.oots.tech.ec.europa.eu/datamodels/certificate-of-birth-1.0.0</sdg:ConformsTo>
            <sdg:Transformation>https://sr.oots.tech.ec.europa.eu/datamodels/certificate-of-birth-1.0.0/age-of-majority</sdg:Transformation>
          </sdg:DistributedAs>
          <!-- PDF Distribution. PDF is unstructured data so there is no conformance to a data model -->
          <sdg:DistributedAs>
            <sdg:Format>application/pdf</sdg:Format>
          </sdg:DistributedAs>

          <!-- Level Of Assurance Required for the Evidence Type -->
          <sdg:AuthenticationLevelOfAssurance>High</sdg:AuthenticationLevelOfAssurance>
          <!-- Free text to include any other important information -->
          <sdg:Note lang="EN">Payment: Free of charge</sdg:Note>
        </sdg:DataServiceEvidenceType>
      </rim:SlotValue>
    </rim:Slot>

    <!-- User Requested Classification Concepts  -->
    <!-- Determination of the EvidenceProviderClassification to the User's attributes. In this example the applicant must indicate if he has ever resided in the country.  -->
    <rim:Slot name="UserRequestedClassificationConcepts">
      <rim:SlotValue xsi:type="rim:CollectionValueType"
                     collectionType="urn:oasis:names:tc:ebxml-regrep:CollectionType:Set">
        <rim:Element xsi:type="rim:AnyValueType">
          <sdg:EvidenceProviderClassification>
            <!-- The Unique Identifier of this EvidenceProviderClassification reused by the element <sdg:ClassificationConcept> of Publishers -->
            <sdg:Identifier>b07146b6-1468-41a7-a10f-ee2a61429dbc</sdg:Identifier>
            <sdg:Type>Codelist</sdg:Type>
            <!-- Definition of the value expression. Must be published in the Semantic Repository. The codelist contains the values ApplicantHasNeverBeenResidentInGermany and ApplicantHasBeenResidentInGermany -->
            <!-- During the query process the user has to select one of the two values as additional input parameter for the query in order to identify the correct publisher / evidence provider -->
            <!-- The selected value will be mapped against the ClassificationValue of Publishers -->
            <sdg:ValueExpression>https://sr.oots.tech.ec.europa.eu/ep-classification/de/countryresidence</sdg:ValueExpression>
            <sdg:Description lang="EN">Rules of responsibility for the processing of personal status cases of German citizens without residence information
            </sdg:Description>
            <sdg:Description lang="DE">Zuständigkeitsregelungen für die Bearbeitung von personenstandsrechtlichen Vorgängen deutscher Staatsangehöriger ohne Wohnsitzinformation</sdg:Description>
          </sdg:EvidenceProviderClassification>
        </rim:Element>
      </rim:SlotValue>
    </rim:Slot>
  </rs:Exception>
</query:QueryResponse>
```

</details>

### XSD Schema Documentation

The [full schema documentation](pathname:///oxygen/SDG-GenericMetadataProfile-v1.1.0.html) contains the complete list of xsd schemes used within the OOTS. The following xsd schemes are used by this query:

- [SDG-GenericMetadataProfile-v1.1.0.xsd](pathname:///oxygen/SDG-GenericMetadataProfile-v1_1_0_xsd.html)
- [query.xsd](pathname:///oxygen/query_xsd.html)
- [rim.xsd](pathname:///oxygen/rim_xsd.html)
- [rs.xsd](pathname:///oxygen/rs_xsd.html)
- [xml.xsd](pathname:///oxygen/xml_xsd.html)

The XSD schemes can be found in the [OOTS semantic repository](https://code.europa.eu/oots/tdd/tdd_chapters/-/tree/master/OOTS-EDM/xsd)
