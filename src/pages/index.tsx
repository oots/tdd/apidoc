import React from "react";
import { clsx } from "clsx";
import Layout from "@theme/Layout";
import useBaseUrl from "@docusaurus/useBaseUrl";
import Link from "@docusaurus/Link";
import styles from "./index.module.css";

function HomepageHeader() {
  return (
    <header className={styles.hero}>
      <div className="container">
        <div className="row">
          <div className="col col--7">
            <h1 className={styles.heroTitle}>APIs</h1>
            <div>
              <p>
                To support the exchange of evidences between Data Services and
                Online Procedure Portals, the Once-Only Technical System uses
                Once-Only supporting services. These are referred to as the
                Common Services. The Common Services do not process data about
                citizens or businesses. Instead they contain and serve
                operational data parameters that support the operation of the
                Once-Only technical system. There are three Common Services:
              </p>
            </div>
            <div>
              <div className={clsx("flex", "margin-bottom--md")}>
                <img
                  src={useBaseUrl("/img/ico-evidence-broker.svg")}
                  alt=""
                  aria-hidden
                />
                <span className="margin-left--sm">Evidence Broker (EB)</span>
              </div>
              <div className={clsx("flex", "margin-bottom--md")}>
                <img
                  src={useBaseUrl("/img/ico-data-service-directory.svg")}
                  alt=""
                  aria-hidden
                />
                <span className="margin-left--sm">
                  Data Service Directory (DSD)
                </span>
              </div>
              <div className={clsx("flex", "margin-bottom--md")}>
                <img
                  src={useBaseUrl("/img/ico-semantic-repository.svg")}
                  alt=""
                  aria-hidden
                />
                <span className="margin-left--sm">
                  Semantic Repository (SR)
                </span>
              </div>
            </div>
          </div>
          <div className={clsx("col col--5", styles.heroImageWrapper)}>
            <img
              src={useBaseUrl("/img/homepage-img-1.png")}
              className={styles.heroImage}
              role="presentation"
              alt=""
            />
          </div>
        </div>
        <div className="row">
          <div className="col col--6">
            <p>
              The Common Services of the OOTS provide machine-to-machine REST
              APIs for querying. This section specifies the normative common
              part of these APIs using the OASIS Regrep 4.0 standard. Services
              implementing these interfaces MUST adhere to the following
              interface specifications. For look-ups, all common services shall
              implement the REST APIs introduced in this section. Currently,
              this section is limited to the Evidence Broker and Data Service
              Directory. More details are provided in the technical design
              documents available from the chapter 3 of the{" "}
              <a href="https://ec.europa.eu/digital-building-blocks/sites/display/OOTS/OOTSHUB+Home">
                OOTS hub site
              </a>
              .
            </p>
          </div>

          <div className="col col--6">
            <p>
              The Once-Only Technical System follows a hybrid deployment model.
              The Commission will provide a production implementation of the
              Common Services that Member States can use as a service. Other
              Member States may implement these services themselves. Each of
              these services will provide a life-cycle management interface to
              maintain the data it serves as this data is subject to change over
              time. That interface uses eDelivery instead of REST and is not
              covered here.
            </p>
          </div>
        </div>
      </div>
    </header>
  );
}

function HomepageFeatures() {
  return (
    <main className={styles.developerResources}>
      <div className="container">
        <div className="row">
          <div className="col col--6 col--4">
            <div className={styles.otherResources}>
              <h2 className={styles.otherResourcesTitle}>
                Access the Common Services
              </h2>
            </div>
          </div>
        </div>
        <div className="row">
          <div className="col col--4 margin-top--lg flex">
            <div className={styles.tile}>
              <div className={styles.tileTitleContainer}>
                <img
                  className={styles.tileIcon}
                  src={useBaseUrl("/img/ico-evidence-broker-card.svg")}
                  alt="Evidence Broker icon"
                />
                <h4 className={styles.tileTitle}>
                  <strong>Evidence Broker</strong>
                </h4>
              </div>
              <div className={styles.tileContent}>
                The Evidence Broker is an authoritative system that maps
                specific data sets as evidence types that prove specific
                requirements.
              </div>
              <Link
                href="/evidence-broker"
                className={clsx(styles.tileLink, styles.tileLinkCta)}
              >
                Go to Evidence Broker
              </Link>
            </div>
          </div>
          <div className="col col--4 margin-top--lg flex">
            <div className={styles.tile}>
              <div className={styles.tileTitleContainer}>
                <img
                  className={styles.tileIcon}
                  src={useBaseUrl("/img/ico-data-service-directory-card.svg")}
                  alt="Data Service Directory icon"
                />
                <h4 className={styles.tileTitle}>
                  <strong>Data Service Directory</strong>
                </h4>
              </div>
              <div className={styles.tileContent}>
                The Data Service Directory is a common service that acts as a
                catalogue of evidence types that can be provided upon request.
              </div>
              <Link
                href="/data-services-directory"
                className={clsx(styles.tileLink, styles.tileLinkCta)}
              >
                Go to Data Service Directory
              </Link>
            </div>
          </div>
          <div className="col col--4 margin-top--lg flex">
            <div className={styles.tile}>
              <div className={styles.tileTitleContainer}>
                <img
                  className={styles.tileIcon}
                  src={useBaseUrl("/img/ico-semantic-repository-card.svg")}
                  alt="Semantic Repository icon"
                />
                <h4 className={styles.tileTitle}>
                  <strong>Semantic Repository</strong>
                </h4>
              </div>
              <div className={styles.tileContent}>
                The Semantic Repository is a common service that acts as a data
                portal for the technical system.
              </div>
              <Link
                href="/semantic-repository"
                className={clsx(styles.tileLink, styles.tileLinkCta)}
              >
                Go to Semantic Repository
              </Link>
            </div>
          </div>
        </div>
      </div>
    </main>
  );
}

export default function Home() {
  return (
    <Layout description="OOTS developers documentation">
      <HomepageHeader />
      <HomepageFeatures />
    </Layout>
  );
}
