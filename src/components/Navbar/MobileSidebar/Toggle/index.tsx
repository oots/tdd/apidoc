import React from "react";
import { useNavbarMobileSidebar } from "@docusaurus/theme-common/internal";
import clsx from "clsx";
import styles from "./styles.module.css";

export default function MobileSidebarToggle(): JSX.Element {
  const mobileSidebar = useNavbarMobileSidebar();

  return (
    <button
      onClick={mobileSidebar.toggle}
      onKeyDown={mobileSidebar.toggle}
      aria-label="Navigation bar toggle"
      className={clsx(
        "navbar__toggle clean-btn",
        styles.toggle,
        mobileSidebar.shown && styles.toggleRotation,
      )}
      type="button"
      tabIndex={0}
    >
      Developers hub materials
    </button>
  );
}
