import React, { type ReactNode } from "react";
import clsx from "clsx";
import { useThemeConfig } from "@docusaurus/theme-common";
import {
  splitNavbarItems,
  useNavbarMobileSidebar,
} from "@docusaurus/theme-common/internal";
import NavbarItem, { type Props as NavbarItemConfig } from "@theme/NavbarItem";
import NavbarColorModeToggle from "@theme/Navbar/ColorModeToggle";
import SearchBar from "@theme/SearchBar";
import NavbarMobileSidebarToggle from "@theme/Navbar/MobileSidebar/Toggle";
import { useLocation } from "@docusaurus/router";
import useBaseUrl from "@docusaurus/useBaseUrl";
import styles from "./styles.module.css";

// Not exported by provider at the time of using this.
interface MobileSidebarContextValue {
  /**
   * Mobile sidebar should be disabled in case it's empty, i.e. no secondary
   * menu + no navbar items). If disabled, the toggle button should not be
   * displayed at all.
   */
  disabled: boolean;
  /**
   * Signals whether the actual sidebar should be displayed (contrary to
   * `disabled` which is about the toggle button). Sidebar should not visible
   * until user interaction to avoid SSR rendering.
   */
  shouldRender: boolean;
  /** The displayed state. Can be toggled with the `toggle` callback. */
  shown: boolean;
  /** Toggle the `shown` attribute. */
  toggle: () => void;
}

function useNavbarItems() {
  // TODO temporary casting until ThemeConfig type is improved
  return useThemeConfig().navbar.items as NavbarItemConfig[];
}

function NavbarItems({ items }: { items: NavbarItemConfig[] }): JSX.Element {
  return (
    <>
      {items.map((item, i) => (
        <NavbarItem {...item} key={i} />
      ))}
    </>
  );
}

function NavbarContentLayout({
  mobileSidebar,
  isHomepage,
  left,
  right,
}: {
  mobileSidebar: MobileSidebarContextValue;
  isHomepage: boolean;
  left: ReactNode;
  right: ReactNode;
}) {
  return (
    <div className={`navbar__inner ${styles.submenu}`}>
      <div
        className={clsx(
          "container",
          styles.submenuContainer,
          mobileSidebar.shown && styles.borderBottom,
        )}
      >
        <div className="navbar__items">{left}</div>
        {!isHomepage && (
          <div className="navbar__items navbar__items--right">{right}</div>
        )}
      </div>
    </div>
  );
}

function removeTrailingSlash(str: string) {
  return str.replace(/\/+$/, "");
}

export default function NavbarContent() {
  const mobileSidebar = useNavbarMobileSidebar();
  const items = useNavbarItems();
  const location = useLocation();
  const [leftItems, rightItems] = splitNavbarItems(items);
  const autoAddSearchBar = !items.some((item) => item.type === "search");

  const isHomepage =
    removeTrailingSlash(location.pathname) ===
    removeTrailingSlash(useBaseUrl("/home"));

  return (
    <NavbarContentLayout
      mobileSidebar={mobileSidebar}
      isHomepage={isHomepage}
      left={
        // TODO stop hardcoding items?
        <>
          {!mobileSidebar.disabled && <NavbarMobileSidebarToggle />}
          <NavbarItems items={leftItems} />
        </>
      }
      right={
        // TODO stop hardcoding items?
        // Ask the user to add the respective navbar items => more flexible
        <>
          <NavbarItems items={rightItems} />
          <NavbarColorModeToggle className={styles.colorModeToggle} />
          {autoAddSearchBar && !isHomepage && (
            <div className={styles.search}>
              <SearchBar />
            </div>
          )}
        </>
      }
    />
  );
}
