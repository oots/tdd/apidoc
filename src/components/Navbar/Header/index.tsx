import React, { useState, useEffect, useCallback } from "react";
import clsx from "clsx";
import useBaseUrl from "@docusaurus/useBaseUrl";
import styles from "./styles.module.css";

export default function Header() {
  const [isOpen, setIsOpen] = useState<boolean>(false);

  const handleClickOnEmptyLink = useCallback((event: React.MouseEvent) => {
    event.preventDefault();
  }, []);

  const toggleMenu = useCallback(
    (event: React.MouseEvent) => {
      event.preventDefault();
      setIsOpen(!isOpen);
    },
    [isOpen],
  );

  const escFunction = useCallback((event: KeyboardEvent) => {
    if (event.key === "Escape") {
      setIsOpen(false);
    }
  }, []);

  useEffect(() => {
    document.addEventListener("keydown", escFunction, false);

    return () => {
      document.removeEventListener("keydown", escFunction, false);
    };
  }, []);

  return (
    <div className="container">
      <header className={styles.header}>
        <div className={styles.wrapper}>
          <div>
            <a href="https://ec.europa.eu/info/index_en">
              <img
                src={useBaseUrl("/img/logo-european-commission.svg")}
                alt="European Commission"
                className={styles.ecLogo}
              />
            </a>
            <a
              href="https://ec.europa.eu/digital-building-blocks/sites/display/OOTS/OOTSHUB+Home"
              className={styles.ootsLogoLink}
            >
              <img
                src={useBaseUrl("/img/logo-once-only-hub.svg")}
                alt="Once Only Hub"
                className={styles.ootsLogo}
              />
            </a>
          </div>
          <nav
            aria-label="main"
            className={clsx(
              {
                [`${styles.openNav}`]: isOpen,
              },
              styles.navbar,
            )}
          >
            <ul className={styles.firstLevel}>
              <li className={styles.oneCol}>
                <a
                  href="#"
                  aria-haspopup="true"
                  onClick={handleClickOnEmptyLink}
                >
                  About
                </a>
                <div className={styles.secondLevel}>
                  <ul>
                    <li>
                      <a href="https://ec.europa.eu/digital-building-blocks/sites/display/OOTS/About+OOTS">
                        <span className={styles.secondLevelTitle}>
                          About OOTS
                        </span>
                        <span className={styles.secondLevelText}>
                          Learn about scope & benefits of the OOTS
                        </span>
                      </a>
                    </li>
                    <li>
                      <a href="https://ec.europa.eu/digital-building-blocks/sites/display/OOTS/Implementing+the+OOTS">
                        <span className={styles.secondLevelTitle}>
                          Implementing the OOTS
                        </span>
                        <span className={styles.secondLevelText}>
                          Discover how EU Member States are Implementing the
                          OOTS
                        </span>
                      </a>
                    </li>
                    <li>
                      <a href="https://ec.europa.eu/digital-building-blocks/sites/display/OOTS/News+and+Interviews">
                        <span className={styles.secondLevelTitle}>
                          News & Interviews
                        </span>
                        <span className={styles.secondLevelText}>
                          Latest news and interviews about OOTS
                        </span>
                      </a>
                    </li>
                    <li>
                      <a href="https://ec.europa.eu/digital-building-blocks/sites/display/OOTS/User+Journey+-+Step+1+Authentication">
                        <span className={styles.secondLevelTitle}>
                          User journey
                        </span>
                        <span className={styles.secondLevelText}>
                          Discover the 7-step OOTS user journey
                        </span>
                      </a>
                    </li>
                    <li>
                      <a href="https://ec.europa.eu/digital-building-blocks/sites/display/OOTS/Architecture">
                        <span className={styles.secondLevelTitle}>
                          Architecture
                        </span>
                        <span className={styles.secondLevelText}>
                          Explore the architectural design of the OOTS
                        </span>
                      </a>
                    </li>
                    <li>
                      <a href="https://ec.europa.eu/digital-building-blocks/sites/display/OOTS/Communication+materials">
                        <span className={styles.secondLevelTitle}>
                          Communication Materials
                        </span>
                        <span className={styles.secondLevelText}>
                          Access materials related to the OOTS
                        </span>
                      </a>
                    </li>
                  </ul>
                </div>
              </li>
              <li className={styles.oneCol}>
                <a
                  href="#"
                  aria-haspopup="true"
                  onClick={handleClickOnEmptyLink}
                >
                  Events
                </a>
                <div className={styles.secondLevel}>
                  <ul>
                    <li>
                      <a href="https://ec.europa.eu/digital-building-blocks/sites/display/OOTS/Events+calendar">
                        <span className={styles.secondLevelTitle}>
                          Events Calendar
                        </span>
                        <span className={styles.secondLevelText}>
                          Register to our events
                        </span>
                      </a>
                    </li>
                    <li>
                      <a href="https://ec.europa.eu/digital-building-blocks/sites/display/OOTS/Projectathons">
                        <span className={styles.secondLevelTitle}>
                          Projectathons
                        </span>
                        <span className={styles.secondLevelText}>
                          Live events to test our Once-Only Technical System
                          implementation
                        </span>
                      </a>
                    </li>
                  </ul>
                </div>
              </li>
              <li className={styles.oneCol}>
                <a
                  href="#"
                  aria-haspopup="true"
                  onClick={handleClickOnEmptyLink}
                >
                  Common Services
                </a>
                <div className={styles.secondLevel}>
                  <ul>
                    <li>
                      <a href="https://ec.europa.eu/digital-building-blocks/sites/display/OOTS/Common+and+Supporting+services">
                        <span className={styles.secondLevelTitle}>
                          Common Services explained
                        </span>
                        <span className={styles.secondLevelText}>
                          Learn about Common Services and access relevant tools
                        </span>
                      </a>
                    </li>
                    <li>
                      <a href="https://ec.europa.eu/digital-building-blocks/sites/display/OOTS/common+services+-+how-to">
                        <span className={styles.secondLevelTitle}>
                          How-to guide
                        </span>
                        <span className={styles.secondLevelText}>
                          Learn how to input data in the Common Services
                        </span>
                      </a>
                    </li>
                    <li>
                      <a href="https://oots.pages.code.europa.eu/evidence-explorer/ee-app/#/home">
                        <span className={styles.secondLevelTitle}>
                          Evidence Explorer
                        </span>
                        <span className={styles.secondLevelText}>
                          Explore data already published in the Common Services
                        </span>
                      </a>
                    </li>
                  </ul>
                </div>
              </li>
              <li className={styles.oneCol}>
                <a
                  href="#"
                  aria-haspopup="true"
                  onClick={handleClickOnEmptyLink}
                >
                  For Implementers
                </a>
                <div className={styles.secondLevel}>
                  <ul>
                    <li>
                      <a href="https://ec.europa.eu/digital-building-blocks/sites/display/OOTS/Onboarding+Playbook">
                        <span className={styles.secondLevelTitle}>
                          Onboarding Playbook
                        </span>
                        <span className={styles.secondLevelText}>
                          Everything you need to know to integrate with the
                          system
                        </span>
                      </a>
                    </li>
                    <li>
                      <a href="https://ec.europa.eu/digital-building-blocks/sites/display/OOTS/Onboarding+Toolkit">
                        <span className={styles.secondLevelTitle}>
                          Onboarding Toolkit
                        </span>
                        <span className={styles.secondLevelText}>
                          Tools and procedures to start operating the system
                        </span>
                      </a>
                    </li>
                    <li>
                      <a href="https://ec.europa.eu/digital-building-blocks/sites/display/OOTS/Welcome+to+the+OOTS+UX+guidelines">
                        <span className={styles.secondLevelTitle}>
                          UX Recommendations
                        </span>
                        <span className={styles.secondLevelText}>
                          Recommendations to help you design the OOTS experience
                        </span>
                      </a>
                    </li>
                    <li>
                      <a href="https://ec.europa.eu/digital-building-blocks/sites/display/OOTS/Technical+Design+Documents">
                        <span className={styles.secondLevelTitle}>
                          Technical design documents
                        </span>
                        <span className={styles.secondLevelText}>
                          Get started with your implementation by reading the
                          documentation
                        </span>
                      </a>
                    </li>
                    <li>
                      <a href="https://oots.pages.code.europa.eu/tdd/apidoc/">
                        <span className={styles.secondLevelTitle}>
                          API specifications
                        </span>
                        <span className={styles.secondLevelText}>
                          Explore the API specifications of the common services
                        </span>
                      </a>
                    </li>
                    <li>
                      <a href="https://ec.europa.eu/digital-building-blocks/sites/display/OOTS/Testing+Services">
                        <span className={styles.secondLevelTitle}>
                          Testing Services
                        </span>
                        <span className={styles.secondLevelText}>
                          Access the test platform and other tools for testing
                        </span>
                      </a>
                    </li>
                    <li>
                      <a href="https://ec.europa.eu/digital-building-blocks/sites/display/OOTS/Catalogue+of+reusable+services">
                        <span className={styles.secondLevelTitle}>
                          Catalogue of Reusable Services
                        </span>
                        <span className={styles.secondLevelText}>
                          Services and solutions to support the implementation
                          of the OOTS
                        </span>
                      </a>
                    </li>
                  </ul>
                </div>
              </li>

              <li>
                <a
                  href="https://ec.europa.eu/digital-building-blocks/sites/display/OOTS/Service+Desk"
                  className={styles.btn}
                  rel="noreferrer"
                >
                  <span className={styles.linkExternal}>Service Desk</span>
                  <span className={styles.question} />
                </a>
              </li>
            </ul>
            <a href="#" className={styles.hamburgerMenu} onClick={toggleMenu}>
              Menu
            </a>
          </nav>
        </div>
      </header>
    </div>
  );
}
