import React from "react";
import clsx from "clsx";
import styles from "./styles.module.css";

type ButtonProps = JSX.IntrinsicElements["button"] & {
  variant?: "primary" | "secondary" | "text";
};

function Button(props: ButtonProps) {
  const { className, variant, ...otherProps } = props;
  const buttonVariant = variant ?? "secondary";

  return (
    <button
      className={clsx(className, styles.button, styles[buttonVariant])}
      {...otherProps}
    />
  );
}

export default Button;
