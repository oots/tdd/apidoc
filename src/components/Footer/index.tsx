import React from "react";
import clsx from "clsx";
import Link from "@docusaurus/Link";
import useBaseUrl from "@docusaurus/useBaseUrl";
import styles from "./styles.module.css";

function Footer() {
  return (
    <footer className={styles.Footer}>
      <div className="container">
        <div className={clsx("row", styles.newsflashRow)}>
          <div className={clsx("col", styles.newsflash)}>
            <div className={styles.newsIllustrationWrapper}>
              <img
                src={useBaseUrl("/img/footer-illustration.svg")}
                className={styles.newsIllustration}
                alt="Subscribe newsflash illustration"
              />
            </div>
            <div>
              <h2 className={styles.newsflashHeading}>
                Subscribe to our newsletter
              </h2>
              <p
                className={clsx(
                  styles.newsflashParagraph,
                  styles.newsflashCopy,
                  "flex",
                )}
              >
                Receive the latest news, invitations to our events and stay
                up-to-date with key Once-Only Technical System developments.
                <a
                  className={styles.newsflashButton}
                  target="_blank"
                  href="https://ec.europa.eu/eusurvey/runner/OOTS_Newsletter_Registration"
                  rel="noreferrer"
                >
                  <span className={styles.ButtonText}>Subscribe</span>
                  <span className={styles.newsflashButtonIcon} />
                </a>
              </p>
            </div>
          </div>
        </div>
        <div className="row">
          <div className="col col--4">
            <div className="margin-bottom--sm">
              <Link href="https://ec.europa.eu/info/index_en">
                <img
                  src={useBaseUrl("/img/logo-european-commission.svg")}
                  className={styles.ecLogo}
                  alt="European Commission Logo"
                />
              </Link>
              <Link href="https://ec.europa.eu/digital-building-blocks/sites/display/OOTS/OOTSHUB+Home">
                <img
                  src={useBaseUrl("/img/logo-once-only-hub.svg")}
                  className={styles.ootsLogo}
                  alt="Once Only Hub Logo"
                />
              </Link>
            </div>
            <p>
              As of the end of 2023, the Once-Only Technical System will
              streamline cross-border online procedures by allowing citizens and
              businesses to supply the same data to public authorities only
              once.
            </p>
            <ul className={styles.SocialMedia}>
              <li>
                <Link
                  href="https://twitter.com/EU_OnceOnly"
                  target="_blank"
                  rel="noreferrer"
                >
                  <img
                    src={useBaseUrl("/img/ico-twitter.svg")}
                    alt="OOTS on Twitter"
                  />
                </Link>
              </li>
            </ul>
          </div>
          <div className="col col--8">
            <div className="row">
              <div className="col col--2">
                <h2>About the programme</h2>
                <nav aria-label="secondary" className={styles.NavFooter}>
                  <ul>
                    <li>
                      <a href="https://ec.europa.eu/digital-building-blocks/sites/display/OOTS/Implementing+the+OOTS">
                        Implementing the OOTS
                      </a>
                    </li>
                    <li>
                      <Link href="https://eur-lex.europa.eu/legal-content/EN/TXT/?uri=uriserv%3AOJ.L_.2022.231.01.0001.01.ENG&toc=OJ%3AL%3A2022%3A231%3ATOC">
                        Implementing regulation
                      </Link>
                    </li>
                    <li>
                      <Link href="https://digital-strategy.ec.europa.eu/en/activities/digital-programme">
                        Digital Europe Programme
                      </Link>
                    </li>
                  </ul>
                </nav>
              </div>
              <div className="col col--3">
                <h2>Once-Only Technical System</h2>
                <nav aria-label="secondary" className={styles.NavFooter}>
                  <ul>
                    <li>
                      <a href="https://ec.europa.eu/digital-building-blocks/sites/display/OOTS/About+OOTS">
                        About OOTS
                      </a>
                    </li>
                    <li>
                      <a href="https://ec.europa.eu/digital-building-blocks/sites/display/OOTS/User+Journey+-+Step+1+Authentication">
                        User journey
                      </a>
                    </li>
                    <li>
                      <a href="https://ec.europa.eu/digital-building-blocks/sites/display/OOTS/Architecture">
                        Architecture
                      </a>
                    </li>
                    <li>
                      <a href="https://ec.europa.eu/digital-building-blocks/sites/display/OOTS/Common+and+Supporting+services">
                        Common and supporting services
                      </a>
                    </li>
                    <li>
                      <a href="https://ec.europa.eu/digital-building-blocks/sites/display/OOTS/Projectathons">
                        Projectathons
                      </a>
                    </li>
                    <li>
                      <a href="https://ec.europa.eu/digital-building-blocks/sites/display/OOTS/once-only+forum">
                        Forums
                      </a>
                    </li>
                    <li>
                      <a href="https://ec.europa.eu/digital-building-blocks/sites/display/OOTS/Newsroom">
                        News
                      </a>
                    </li>
                    <li>
                      <a href="https://ec.europa.eu/digital-building-blocks/sites/display/OOTS/Events+calendar">
                        Events
                      </a>
                    </li>
                    <li>
                      <a href="https://ec.europa.eu/digital-building-blocks/sites/display/OOTS/Communication+materials">
                        Communication materials
                      </a>
                    </li>
                  </ul>
                </nav>
              </div>
              <div className="col col--2">
                <h2>Get started</h2>
                <nav aria-label="secondary" className={styles.NavFooter}>
                  <ul>
                    <li>
                      <a href="https://ec.europa.eu/digital-building-blocks/sites/display/OOTS/Technical+Design+Documents">
                        Technical Design documents
                      </a>
                    </li>
                    <li>
                      <a href="https://oots.pages.code.europa.eu/tdd/apidoc/">
                        API specifications
                      </a>
                    </li>
                  </ul>
                </nav>
              </div>
              <div className="col col--3">
                <h2>Related websites</h2>
                <nav aria-label="secondary" className={styles.NavFooter}>
                  <ul>
                    <li>
                      <Link href="https://ec.europa.eu/digital-building-blocks/sites/display/DIGITAL/Digital+Homepage">
                        Digital Europe: eIDAS enablers
                      </Link>
                    </li>
                    <li>
                      <Link href="https://europa.eu/youreurope/">
                        Your Europe
                      </Link>
                    </li>
                    <li>
                      <Link href="https://webgate.ec.europa.eu/fpfis/wikis/spaces/viewspace.action?key=SDGCOORDGROUP">
                        Single Digital Gateway Coordination Group
                      </Link>
                    </li>
                  </ul>
                </nav>
              </div>
              <div className="col col--2">
                <h2>Support</h2>
                <nav aria-label="secondary" className={styles.NavFooter}>
                  <ul>
                    <li>
                      <a href="mailto:EC-OOTS-SUPPORT@ec.europa.eu">
                        Contact us
                      </a>
                    </li>
                    <li>
                      <a href="https://ec.europa.eu/digital-building-blocks/sites/display/OOTS/Service+Desk">
                        Service Desk
                      </a>
                    </li>
                    <li>
                      <a href="https://ec.europa.eu/digital-building-blocks/sites/display/OOTS/Service+Desk+FAQ">
                        FAQ
                      </a>
                    </li>
                  </ul>
                </nav>
              </div>
            </div>
            <div className="row">
              <div className="col col--12">
                <nav
                  aria-label="secondary"
                  className={styles.footerMainColophon}
                >
                  <ul>
                    <li>
                      <a href="https://ec.europa.eu/digital-building-blocks/sites/display/OOTS/Privacy+Statement">
                        Privacy Statements
                      </a>
                    </li>
                    <li>
                      <a href="https://ec.europa.eu/digital-building-blocks/sites/display/OOTS/Master+Service+Arrangement">
                        Master Service Arrangement
                      </a>
                    </li>
                    <li>
                      <Link href="https://ec.europa.eu/info/legal-notice_en">
                        Legal notice
                      </Link>
                    </li>
                    <li>
                      <Link href="https://ec.europa.eu/info/cookies_en">
                        Cookies
                      </Link>
                    </li>
                  </ul>
                </nav>
              </div>
            </div>
          </div>
        </div>
      </div>
    </footer>
  );
}

export default React.memo(Footer);
