import clsx from "clsx";
import React from "react";
import Link from "@docusaurus/Link";
import styles from "./styles.module.css";

interface ExternalLinkProps {
  className?: string;
  href: string;
  children: React.ReactNode;
  inverted?: boolean;
  buttonLike?: boolean;
  size?: "normal" | "big";
}

function ExternalLink(props: ExternalLinkProps) {
  const {
    className,
    href,
    children,
    buttonLike = false,
    inverted = false,
    size = "normal",
  } = props;

  if (buttonLike) {
    return (
      <Link
        className={clsx(className, styles.button, {
          [`${styles.inverted}`]: inverted,
          [`${styles.big}`]: size === "big",
        })}
        href={href}
        target="_blank"
        rel="noreferrer"
      >
        <span className={styles["external-link"]}>{children}</span>
        <span className={styles["sr-only"]}>(opens in a new tab)</span>
      </Link>
    );
  }

  return (
    <Link
      className={clsx(className, styles["external-link"])}
      href={href}
      target="_blank"
      rel="noreferrer"
    >
      {children}
      <span className={styles["sr-only"]}>(opens in a new tab)</span>
    </Link>
  );
}

export default ExternalLink;
