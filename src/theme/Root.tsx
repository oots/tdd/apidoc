import React from "react";
import Head from "@docusaurus/Head";
// Preload fonts
// @ts-ignore
import regularFont from "@site/static/fonts/SourceSansPro-Regular.ttf";
// @ts-ignore
import boldFont from "@site/static/fonts/SourceSansPro-SemiBold.ttf";

export default function Root({ children }) {
  return (
    <>
      <Head>
        <link
          rel="preload"
          href={regularFont}
          as="font"
          type="font/ttf"
          crossOrigin="anonymous"
        />
        <link
          rel="preload"
          href={boldFont}
          as="font"
          type="font/ttf"
          crossOrigin="anonymous"
        />
      </Head>
      {children}
    </>
  );
}
